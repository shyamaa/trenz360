import React, { Component } from 'react';
import { Text, View, TouchableOpacity, Dimensions, Image, Platform, Linking,Modal,Share,ScrollView } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import backArrow from '../../imgAssests/backArrow.png';
import Swiper from 'react-native-deck-swiper';
import ScrollableTabView, { ScrollableTabBar } from 'react-native-scrollable-tab-view';
import { RFValue } from "react-native-responsive-fontsize";
import Hyperlink from 'react-native-hyperlink';

import cross from '../../imgAssests/cross.png';
import rightArrow from '../../imgAssests/right-arrow-round-red.png'


const paddingValue = Platform.isPad ? true : false;
import OverlayLabel from '../../components/OverlayLabel';
import PostNotificationCard from './PostNotificationCard';
import SocialButton from '../../components/SocialButton';
import {facebookPost} from '../../components/PostUtils'
import { Auth } from 'aws-amplify';
import { networkList } from '../../store/actions/BuzzAction';
import Loader from '../../components/Loader';

const { width, height } = Dimensions.get('window')


const TAG = "RecentPost => \n";

let TwitterParameters = '';


const recentPostData=
    [
        {
            "title":"Jason Lewis for Congress – US Senate – Campaign found in zip code 55044 & 55024",
            "message1":"In the current demo system",
            "message2":"I am excited to have Jason Lewis running for Senate.Are you watching his posts on Twitter?",
            "post1":"In the current demo system",
            "post2":"What a great turn out yesterday at the March for Life. I love that Jason Lewis is an advocate for life. https://www.facebook.com/LewisForMN/photos/a.10152982397618472/10157190807993472/?type=3&theater",
        },
        {
            "title":"Senator Mark Morgan, MN Senate District 56 – Campaign found in zip code 55044 & 55024",
            "message1":"Did you see Mark Morgan is having a meet and greet tomorrow?  Are you going?  Lizzy and I are going I think he would be good in the Senate",
            "message2":"Hey are you going to vote tomorrow?  I am voting for Morgan for Senate.",
            "post1":"I am really happy to see Morgan show up for the MCCL pro-life rally today.",
            "post2":"Mark Morgan’s team is going door knocking tomorrow.  DM me if any of you want to join us. Picture 2",
        }
       

    ]


export default class PostNotifications extends Component {

    constructor() {
        super();
        this.state = {
            isLoading: false,
            cards: [1],
            post:recentPostData,
            swiperVisiable: false,
            setTitle:null,
            swipeImg:require('../../imgAssests/picture1.png'),
            isModalShareVisiable:false,
            postTitle:'',
            postDescription:'',
            postURL: 'https://www.facebook.com/LewisForMN/photos/a.10152982397618472/10157190807993472/?type=3&theater',
            membersArray:[],
            messageList:[],
            FacebookShareURL: 'https://aboutreact.com',
            
        }
    }

    async componentDidMount() {

       this.getNetwokList()
    }

    async getNetwokList(){
        const userSession =  await Auth.currentSession();
        let token = userSession.idToken.jwtToken;
        let list = await networkList(token);
        console.log("network listtt",list)
        this.setState({membersArray:list,isLoading:false})
    }


    // select the type of sharing.

    sharePost = () => {
    
        switch (this.state.setTitle) {
            case 'Facebook':
                this.postOnFacebook();
                break;
            case 'Instagram':
                this.onShare();
                break;
            case 'Twitter':
                this.postOnTwitter();
                break;
            case 'Snapchat':
                this.onShare();
                break;
            default: break;
        }
    }


    // sharing post via facebook.
   
    postOnFacebook = () => {

        let {FacebookShareURL, postDescription } = this.state;
         
        let url = 'https://www.facebook.com/sharer/sharer.php' + facebookPost(FacebookShareURL,postDescription);
       
        Linking.openURL(url).then((data) => {
            console.log('Facebook Opened');
        }).catch(() => {
            console.log('Something went wrong');
        });
        
   }

      // sharing post via twitter.

    async postOnTwitter(){

        let {postTitle, postDescription } = this.state;

          if (postTitle != undefined) {
            if (TwitterParameters.includes('?') == false) {
                TwitterParameters =
                    TwitterParameters + '?url=' + encodeURI(postTitle);
            } else {
                TwitterParameters =
                    TwitterParameters + '&url=' + encodeURI(postTitle);
            }
        }
        if (postDescription != undefined) {
            if (TwitterParameters.includes('?') == false) {
                TwitterParameters =
                    TwitterParameters + '?text=' + encodeURI(postDescription);
            } else {
                TwitterParameters =
                    TwitterParameters + '&text=' + encodeURI(postDescription);
            }
        }
        

        let url = 'https://twitter.com/intent/tweet' + TwitterParameters;

        Linking.openURL(url)
            .then(data => {
                console('Twitter Opened');
            })
            .catch(() => {
                console.log("error")
            });
    };

   
     // sharing post via share dialog popup.

    onShare = () => {

        let shareOptions = {
            title: 'SwipeRed',
            message:this.state.postDescription,
            url:this.state.postTitle,
        };
        
        Share.share(shareOptions);
    }



    // swipe card component

    renderCard = () => {

        let {messageList} = this.state;
        
        return (

            <View style={styles.cardContainer}>
                
            <View style={{ flex: 1, backgroundColor: 'white', alignItems: 'center', padding: 0, borderRadius: 25, }}>
                <Image source={this.state.swipeImg}
                    style={{ height:height*0.25, width: width - 30, borderTopRightRadius: 20, borderTopLeftRadius: 20 }} />
                <ScrollView style={{flex:1}} persistentScrollbar={true}>   
                <Text style={{ color: '#2F3979', fontWeight: '800', fontSize:RFValue(18), textAlign: 'center', marginTop: 10, width: 300, }}>
                {messageList.title} </Text>
                
                <Hyperlink linkDefault={ true } linkStyle={ {textDecorationLine:'underline' } }>
                <Text style={{
                    color: 'grey', flexWrap: 'wrap', flex: 1,
                    fontSize:RFValue(14), textAlign: 'center', top:12, width: 300, 
                }}>{messageList.message}</Text>
                </Hyperlink>
                </ScrollView> 

            </View>


            {/* swipe button view */}


            <View style={styles.swipeBtnView}>

                <TouchableOpacity style={{ bottom: 20 }} onPress={()=>this.swiper.swipeLeft()}>
                    <Image source={cross} style={styles.imgStyle} />
                </TouchableOpacity>
                <TouchableOpacity style={{ bottom: 20 }} onPress={()=>this.swiper.swipeRight()}>
                    <Image source={rightArrow} style={styles.imgStyle} />
                </TouchableOpacity>

            </View>

        </View>




        )

    }

    // select type of sharing method

    onSelect = (title) => {
        this.setState({ setTitle: title })


    }
    
    // swipe with right direction

    onSwipeRight=()=>{
        
        this.setState({isModalShareVisiable:true,swiperVisiable:false},
           // this.refs.modal1.open()
        )
    }

    // sending messages of their specific types

    onSendingMessage = (type) => {
        let msg = "hi how are you"

        if (msg) {
            let url = `fb-messenger://share?link=${msg}`
            Linking.openURL(url).then((data) => {
                console.log('facebook Opened');
            }).catch(() => {
                alert('Make sure facebook messenger installed on your device');
            });
        } else {
            alert('Please insert message to send');
        }
    }

    // line separotor for flatlist

    renderSeparator = () => {
        return (
            <View
                style={{
                    height: 0.5,
                    width: "100%",
                    alignSelf: 'center',
                    backgroundColor: "grey",
                }}
            />
        );
    };


    // render card view for post & message


    renderItemCard=()=>{
        return(
        this.state.membersArray.map((data)=>{
            return(
                <PostNotificationCard tabLabel={data.name.substring(0,10)+"..."} onpress={this.childHandler.bind(this)} networkId={data.networkId} />
            )
        })
      )  
    }

    // getting response from child class

    childHandler(dataFromChild,data) {
        let self = this
        self.setState({ swiperVisiable: true ,messageIndex:dataFromChild,messageList:data,postTitle:data.title,postDescription:data.message})
        
    }

    // returning jsx component 

    render() {
        const { swiperVisiable,isLoading } = this.state;

        return (
            <View style={{ flex: 1 }}>

                {/* header view container */}

                <LinearGradient colors={['#085d87', '#27c7bb']} style={{ height: 130, backgroundColor: '#000' }}>
                    <View style={{
                        paddingVertical: 5, paddingHorizontal: paddingValue ? 120 : 20,
                        paddingTop:50, paddingBottom:50,
                        flexDirection: 'row', top: 20
                    }}>
                        <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                            <Image source={backArrow} style={{ height: 25, width: 25, resizeMode: 'contain' }} />
                        </TouchableOpacity>
                        <Text style={{ fontSize: 18, color: 'white', fontWeight: '500', left: 10 }}>Post to share</Text>
                    </View>
                </LinearGradient>

                {/* main view container */}

                

                <ScrollableTabView
                    initialPage={0}
                    tabBarInactiveTextColor={'grey'}
                    tabBarUnderlineStyle={{ backgroundColor: '#BB281C', width: 100, height: 2 }}
                    tabBarActiveTextColor={'#BB281C'}
                    renderTabBar={() => <ScrollableTabBar />}>

                   {this.renderItemCard()}

                   

                </ScrollableTabView>

                {isLoading && <Loader/>}


                {/* swiping card component */}

                {swiperVisiable &&
                    <Swiper
                        ref={swiper => {
                            this.swiper = swiper
                        }}
                        verticalSwipe={false}
                        cards={this.state.cards}
                        renderCard={this.renderCard}
                        stackSize={3}
                        stackSeparation={15}
                        animateOverlayLabelsOpacity
                        onSwipedRight={() => this.onSwipeRight()}
                        onSwipedLeft={() => this.setState({ swiperVisiable: false })}
                        animateCardOpacity
                        showSecondCard={false}
                        onTapCardDeadZone={1}
                        backgroundColor={'#00000070'}
                        overlayLabels={{
                            left: {
                                title: 'Close',
                                element: <OverlayLabel label="left" color="grey" direction={"left"} isClose={true} />,
                                style: {
                                    label: {
                                        backgroundColor: 'black',
                                        borderColor: 'black',
                                        color: 'white',
                                        borderWidth: 1
                                    },
                                    wrapper: {
                                        flexDirection: 'column',
                                        alignItems: 'center',
                                        justifyContent: 'center',
                                        marginTop: 30,
                                        marginLeft: -30
                                    }
                                }
                            },
                            right: {
                                title: 'Forward',
                                element: <OverlayLabel label="right" color="grey" direction={"right"} />,
                                style: {
                                    label: {
                                        backgroundColor: 'black',
                                        borderColor: 'black',
                                        color: 'white',
                                        borderWidth: 1
                                    },
                                    wrapper: {
                                        flexDirection: 'column',
                                        alignItems: 'center',
                                        justifyContent: 'center',
                                        marginTop: 30,
                                        marginLeft: 30
                                    }
                                }
                            },
                        }}
                        containerStyle={{
                            justifyContent: 'center', backgroundColor: '#00000070'
                        }} />
                }

                {/* modal for user contact lists */}

                <Modal visible={this.state.isModalShareVisiable} backdrop={false} transparent={true} style={{flex:1,backdropColor:'#999999',backdropOpacity:0.5}} >

                   <View style={styles.modalContainer}>
                   
                    <View style={styles.mainViewContainer}>

                    <Text onPress={() => this.setState({isModalShareVisiable:false})} style={{ padding: 20, color: 'black' }}>X</Text>
                    
                     <View style={{ alignItems: 'center', padding:0 }}>
                            <Text style={styles.infoTextStyle}>Where would you like to {'\n'} share this post ? </Text>
                            <View style={{ flex: 1, padding: 30, justifyContent: 'space-between' }}>

                               <SocialButton title={'Facebook'} bgColor={'blue'} setTitle={this.state.setTitle}
                                    onpress={() => this.onSelect('Facebook')}
                                    isCheck={true} titleColor={'#FFF'} />
                                <View style={{ height: 10 }} />
                                <SocialButton title={'Instagram'} bgColor={'red'} onpress={() => this.onSelect('Instagram')}
                                    setTitle={this.state.setTitle}
                                    titleColor={'#FFF'} />
                                <View style={{ height: 10 }} />
                                <SocialButton title={'Snapchat'} bgColor={'yellow'} setTitle={this.state.setTitle}
                                    onpress={() => this.onSelect('Snapchat')} titleColor={'#000'} />
                                <View style={{ height: 10 }} />
                                <SocialButton title={'Twitter'} bgColor={'#0077be'} setTitle={this.state.setTitle}
                                    onpress={() => this.onSelect('Twitter')} titleColor={'#FFF'} />
                                <View style={{ height: 20 }} />    
                                    <TouchableOpacity
                                    disabled={this.state.setTitle !== null ? false : true}
                                    style={[styles.shareBtnStyle,{backgroundColor: this.state.setTitle !== null ? 'blue' : '#D3D3D3'}]}
                                    onPress={() => this.sharePost()} >
                                    <Text style={styles.shareBtnTextStyles}>Share</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                       


                    </View>
                
                  </View>
               
                </Modal>





            </View>
        );
    }

}

const styles = {
    container: {
        flex: 1,
        backgroundColor: 'red',
        alignItems: 'center',
        justifyContent: 'center'
    },
    modal: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#00000070',
        width: width - 20,
        height: height,
        borderRadius: 20,
        top: height / 12
    },
    modalView: {
        backgroundColor: 'white', height: height, width: width - 20, borderRadius: 20
    },
    cardContainer: {
        width: width - 30,
        height: height - 250,
        backgroundColor: '#FFF',
        alignSelf: 'center',
        shadowColor: '#000',
        borderRadius: 50,
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.5,
        shadowRadius: 2,
        elevation: 2,
        top: 50
    },
   
    swipeBtnView:{

        backgroundColor: '#2F3979', borderBottomEndRadius: 10,
        borderBottomStartRadius: 10, height: 80, alignItems: 'center', flexDirection: 'row', justifyContent: 'space-evenly'

    },
    imgStyle: {
        width: 120,
        height: 120, resizeMode: 'contain'
    },
    titleStyle: {
        top: 10,
        fontWeight: '500',
    },
    modalContainer: {
        justifyContent: 'center', alignItems: 'center',
        height: height, width: width, borderRadius: 20,
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.5,
        shadowRadius: 2,
        elevation: 2,
        backgroundColor:  '#00000070',
     
       
        
    },
    mainViewContainer: {
        backgroundColor: 'white', 
        height:450 , width: width - 50,
        borderRadius: 20
    },
    infoTextStyle:{
        color: '#004772', fontWeight: '500',
        fontSize: 18, textAlign: 'center'
    },
    shareBtnStyle:{
        width: width - 70, height: 50, borderRadius: 10, justifyContent: 'center'
    },
    shareBtnTextStyles:{
        color: '#FFF',
        fontWeight: '500', fontSize: 14, textAlign: 'center'
    }

}
