import React from 'react';
import { Text, View, ScrollView, Image, TextInput, FlatList, Picker, StyleSheet, TouchableOpacity,AsyncStorage } from 'react-native';
import CheckBox from 'react-native-check-box';
import CustomButton from '../../components/CustomButton';
import RNPickerSelect from 'react-native-picker-select';
import axios from 'axios'
import { TextField } from 'react-native-material-textfield';
import { getOr } from 'lodash/fp'
import { validEmail, isEmpty } from '../../utils/Validations';
import { showInfoAlert } from '../../utils/AlertPopUp';
import { EMAIL_EMPTY_MESSAGE, EMAIL_VALIDATION_MESSAGE } from '../../actions/StringConstants';
import Amplify, { Auth } from 'aws-amplify';
import { networkList } from '../../store/actions/BuzzAction';
import Loader from '../../components/Loader';




var tempCheckValues = [];

var countVal= 0

class SignupStarted extends React.Component {

    constructor(props) {
        super(props);
        console.log("propssssss", props.navigation.state.params)
        this.state = {
            firstName: getOr(null, 'data.firstName', props.navigation.state.params),
            lastName: getOr(null, 'data.lastName', props.navigation.state.params),
            mobile: getOr(null, 'data.mobile', props.navigation.state.params),
            email: getOr(null, 'data.email', props.navigation.state.params),
            password: getOr("Password123!", 'data.password', props.navigation.state.params),
            isLoading:false,
            zipCode: "",
            age: "",
            gender: "",
            showButton: false,
            isChecked1: true,
            seletedId: '',
            networks: [],
            checkBoxChecked: [],
            addNetworks: [],
            count: 0
        }
    }

    async componentDidMount() {
     

        this.getNetwokList();
        this.getUser();
        

     }

     getNetwokList=()=>{
        let self = this;

        // get network api calling here

        axios.get('https://290e8otzd9.execute-api.us-west-2.amazonaws.com/dev/network')
            .then(function (response) {
                console.log(response.data);
                let data = [];
                data.push(response.data);
                self.setState({ networks: response.data })
            })
            .catch(function (error) {
                console.log(error);
            });

     }


     async getUser(){
        let self = this;

        const userSession =   await Auth.currentSession();
        let token = userSession.idToken.jwtToken;
        console.log("login token",token)

        const config = {
            headers:{ 'Authorization': 'Bearer ' + token}
        };


          axios.get('https://290e8otzd9.execute-api.us-west-2.amazonaws.com/dev/me',
              config)
            .then(function (response) {
                console.log("user response",response.data.userId);
                self.setState({userData:response.data,isLoading:false})
            })
            .catch(function (error) {
                console.log(error);
            });
     }



    

    checkBoxChanged(id, value) {

        // check & uncheck display 

        this.setState({
            checkBoxChecked: tempCheckValues
        })

        var tempCheckBoxChecked = this.state.checkBoxChecked;
        tempCheckBoxChecked[id] = !value;
        this.setState({
            checkBoxChecked: tempCheckBoxChecked
        })

        // add networks in array

        if (this.state.addNetworks.includes(id)) {

            var i = this.state.addNetworks.indexOf(id);
            if (i != -1) {
                this.state.addNetworks.splice(i, 1);
            }
        } else {
            this.state.addNetworks.push(id);
        }

        console.log("check box value added", this.state.addNetworks)

    }




    _selectInterest = (id) => {
        this.setState({ seletedId: id })

    }
    _updateGender = (gender) => {
        this.setState({ gender: gender })
    }


    // network card for user


    renderItem = ({ item }) => {
        return (

            <View style={{ margin: 10, }}>

                <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center' }}>
                    <CheckBox
                        style={{ width: 1, height: 20 }}
                        isChecked={this.state.checkBoxChecked[item.networkId]}
                        onClick={() => this.checkBoxChanged(item.networkId, this.state.checkBoxChecked[item.networkId])}
                        uncheckedCheckBoxColor={'grey'}
                        checkedCheckBoxColor={'#BC281C'}
                    />



                    <View style={{ flex: 1, marginLeft: 40, }}>
                        <Text style={{ fontSize: 15, fontWeight: '500' }}>{item.name}</Text>
                    </View>
                </View>

                {/* <Text style={{ fontSize: 12, marginLeft: 40, marginTop: 5, color: 'grey' }}>messgaess</Text> */}
            </View>
        );
    }

    // email validation from

    emailValidation = () => {

        let { email } = this.state;

        if (isEmpty(email)) {
            showInfoAlert(EMAIL_EMPTY_MESSAGE)
            return false;
        }

        if (validEmail(email)) {
            showInfoAlert(EMAIL_VALIDATION_MESSAGE)
            return false;
        }
        return true
    }


 


    async addNetworks() {

        

        let self = this;
        let {addNetworks} = self.state
        const userSession = await Auth.currentSession();
        let token = userSession.idToken.jwtToken;
        console.log(token, "tokennnn")
        

       

        addNetworks.map((data, index) => {
        
             fetch('https://290e8otzd9.execute-api.us-west-2.amazonaws.com/dev/me/network/'+data, {
                method: 'POST',
                headers: { 'Authorization': 'Bearer ' + token },
             }).then((response) => response)
            .then((responseJson) => {
                console.log(responseJson);
                countVal = countVal + 1 
                console.log("done",countVal)
                if(countVal === addNetworks.length){
                    self.setState({isLoading:false})
                    this.props.navigation.navigate('AddPhoto')
        
                }
             })
            .catch((error) => {
              console.error(error);
              this.setState({isLoading:false})
          });


        })

        if(countVal === addNetworks.length){

            console.log("done")

        }



       


    }

    async updateUserInfo(){

        let { firstName, lastName, zipCode, gender, age } = this.state;
       
        let obj = {
            firstName: firstName,
            lastName: lastName,
            postal: zipCode,
            age: age
        }

        let self = this;
        // const userSession = await Auth.currentSession();
        // let token = userSession.idToken.jwtToken;
        // console.log(token, "tokennnn","values of obj",obj)


        var token = await AsyncStorage.getItem('userToken');

         let url = "https://290e8otzd9.execute-api.us-west-2.amazonaws.com/dev/me"


        await axios({
            method: 'put',
            url: url,
            headers: {'Authorization': 'Bearer ' + token }, 
            data: {
               firstName:firstName, // This is the body part
               lastName:lastName,
               postal:zipCode,
               age:age,
               gender:gender
            }
          }).then(res => this.addNetworks())
          .catch(e => console.log(e));

    }

    continueProcess = () => {

        this.setState({isLoading:true})


        if (this.emailValidation()) {

            let { firstName, lastName, email, mobile, password, zipCode, gender, age, checkBoxChecked } = this.state;
            let obj = {
                firstName: firstName,
                lastName: lastName,
                //email: email,
                //mobile: mobile,
                //password: password,
                zipCode: zipCode,
                gender: gender,
                age: age
            }

           this.updateUserInfo();






            //    axios.put('https://290e8otzd9.execute-api.us-west-2.amazonaws.com/dev/me', obj, {
            //          config,
            //     }).then(function (response) {
            //         console.log("add network", response.data);
            //         let data = [];
            //         data.push(response.data);
            //         self.setState({ networks: response.data, count: this.state.count + 1 })

            //     }).catch(function (error) {
            //             console.log("add network 66", error);
            //      })



            //this.props.navigation.navigate('AddPhoto', { data: obj })



        }



    }

    render() {

        let { gender, zipCode, age, email, networks,isLoading } = this.state;

        const placeholder = {
            label: ' -- Select Gender --',
            value: null,
            color: 'red',
        };
        return (
            <ScrollView style={{ flex: 1, backgroundColor: '#FFFFFF' }}>

                <View style={{ alignItems: 'center', marginTop: 75 }}>

                    {/* header view */}

                    <View style={{ alignItems: 'center' }}>
                        <Image source={require('../../imgAssests/AppIcons/1024.png')} style={{ width: 80, height: 80, alignItems: 'center' }} />
                        <Text style={styles.headerTextStyle}>Get Started</Text>
                    </View>

                    {/* form view */}

                    <View style={{ marginTop: 20 }}>

                        {this.state.email === null &&

                            <TextField
                                onChangeText={(text) => this.setState({ email: text })}
                                underlineColorAndroid='transparent'
                                value={this.state.email}
                                autoCapitalize='none'
                                label="Email"
                                maxLength={25}
                                placeholderTextColor="#D8D9DB"
                                baseColor={'grey'}
                                tintColor={'grey'}
                                containerStyle={{ width: 300, height: 40, marginBottom: 0 }}

                            />}

                        <TextField
                            onChangeText={(text) => this.setState({ zipCode: text })}
                            underlineColorAndroid='transparent'
                            value={this.state.zipCode}
                            autoCapitalize='none'
                            label="Voting Zip Code"
                            maxLength={5}
                            keyboardType="numeric"
                            placeholderTextColor="#D8D9DB"
                            baseColor={'grey'}
                            tintColor={'grey'}
                            containerStyle={{ width: 300, height: 40, marginTop: 20 }}

                        />

                        <TextField
                            onChangeText={(text) => this.setState({ age: text })}
                            underlineColorAndroid='transparent'
                            autoCapitalize='none'
                            label="Age"
                            value={this.state.age}
                            maxLength={2}
                            keyboardType="numeric"
                            placeholderTextColor="#D8D9DB"
                            baseColor={'grey'}
                            tintColor={'grey'}
                            containerStyle={{ width: 300, height: 40, marginTop: 20 }}

                        />

                    </View>







                    <View style={{ marginTop: 40, paddingHorizontal: 5 }}>

                        <Text style={{ color: 'grey' }}>Gender</Text>
                        {/* gender input with dropdown */}

                        <View style={styles.SectionStyle}>

                            <View style={{ marginTop: 15 }} />


                            <RNPickerSelect
                                ref={el => {
                                    this.inputRefs;
                                }}
                                onOpen={() => {
                                    console.warn("openenn")
                                }}
                                onValueChange={(value) => this._updateGender(value)}
                                placeholder={placeholder}
                                value={this.state.gender}
                                items={[
                                    { label: 'Male', value: 'Male' },
                                    { label: 'Female', value: 'Female' }
                                ]}



                            />
                            <View style={{ width: 300, height: 10, borderBottomColor: 'grey', borderBottomWidth: 0.5 }} />


                        </View>





                    </View>

                </View>

                {/* interest */}

                {zipCode !== '' && age !== '' && gender !== '' ?

                    <View>

                        <View style={{ marginTop: 20, alignItems: 'center' }}>

                            {networks.length > 0 && <Text style={{ fontWeight: '500' }}>Interests</Text>}

                        </View>


                        <View style={{ marginTop: 10, paddingHorizontal: 30 }}>

                            <FlatList
                                style={{ height: 'auto', marginTop: 5, }}
                                data={networks}
                                renderItem={this.renderItem}
                            />

                        </View>

                        {this.state.addNetworks.length > 0 &&

                            <View style={{ alignItems: 'center' }}>
                                <CustomButton title={'Continue'} onpress={() => this.continueProcess()} />
                            </View>}

                    </View>
                    : null}


                    {isLoading && <Loader/>}








            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        margin: 10,
    },

    SectionStyle: {
        // flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5,
    },
    headerTextStyle: {
        textAlign: 'center',
        marginTop: 20,
        fontSize: 22,
        fontWeight: '800',
        color: '#2F3979'
    }
})
export default SignupStarted;