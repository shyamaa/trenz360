import React from 'react';
import {Text} from 'react-native'
import ScrollableTabView, { ScrollableTabBar } from 'react-native-scrollable-tab-view';
import MessageInfoCard from '../components/MessageInfoCard';
import NetworkData from '../constants/NetworkData'

const DynamicTab = (props) => {

    return (

        <ScrollableTabView
            initialPage={0}
            tabBarInactiveTextColor={'grey'}
            tabBarUnderlineStyle={{ backgroundColor: '#BB281C', width: 100, height: 2 }}
            tabBarActiveTextColor={'#BB281C'}
            renderTabBar={() => <ScrollableTabBar />}>

            {/* NetworkData.map((data, index) => {

                console.log("data")

            })  */}
           


           
      
            <MessageInfoCard tabLabel="MCCL MN" onpress={props.onpress} />
            <MessageInfoCard tabLabel="Jason Lewis" onpress={props.onpress} />
            <MessageInfoCard tabLabel="Senator" onpress={props.onpress} />


        </ScrollableTabView>




    );
};

export default DynamicTab;
