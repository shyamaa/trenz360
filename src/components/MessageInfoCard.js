import React from 'react';
import { View, Text, Dimensions, Image, ImageBackground, TouchableOpacity, FlatList,AsyncStorage } from 'react-native';
import { isEmpty } from '../utils/Validations'
import {networkMessageList } from '../store/actions/BuzzAction';
import Loader from './Loader';
import backArrow from '../imgAssests/backRightArrow.png';
const { width, height } = Dimensions.get('window')



class MessageInfoCard extends React.Component {

    constructor(props) {
        super(props);
        console.log("info card", props)
        this.state = {
            loader:false,
            networkId: props.id,
            swiperVisiable: false,
            lists:[],
            messageList:[],
            lastOpen:''
        }
    }



     async componentDidMount() {
         this.getNetworkItems()
         let lastOpen = await AsyncStorage.getItem("lastOpenTime")
         console.warn(lastOpen)
         this.setState({lastOpen:lastOpen})
         

    }

    //  api calling for network posts & messages

   async getNetworkItems(){
    let { networkId } = this.state

    let self = this;

    let reposneData = await networkMessageList(networkId);
   
    reposneData.map((data, index) => {
                    reposneData[index].isPost === false ? 
                     self.state.lists.push(reposneData[index]) :  null
                })
    self.setState({messageList:this.state.lists,loader:false})   

   }

   //   separted line for flatlists 


    renderSeparator = () => {
        return (
            <View
                style={{
                    height: 0.5,
                    width: "90%",
                    alignSelf: 'center',
                    backgroundColor: "grey",
                }}
            />
        );
    };


    
   
    // rendering jsx

    render() {
        let { messageList, swiperVisiable,loader } = this.state;

        let lastOpen = new Date().getTime();


        
        return (
            <View style={{ flex: 1 }}>

                {!isEmpty(messageList) ?

                    <FlatList
                        data={messageList}
                        renderItem={({ item, index }) =>
                       
                            <TouchableOpacity style={{ padding: 15 }} onPress={() => this.props.onpress(index,item)}>
                             {
                                 new Date(item.expiredAt).getTime() < lastOpen ? 

                                 <View style={[styles.msgTextStatusView,{backgroundColor : 'blue'}]}>
                                   <Text style={{ color: '#FFF', fontSize: 10, textAlign: 'center' }}>
                                     Expired
                                  </Text>
                                </View> :
                                 <View style={[styles.msgTextStatusView,{backgroundColor : 'red'}]}>
                                   <Text style={{ color: '#FFF', fontSize: 10, textAlign: 'center' }}>
                                    NEW
                                   </Text>
                                 </View>
                            } 
                             
                                
                            

                                <View style={{ flexDirection: 'row', justifyContent: 'space-between'}}>

                                    <View style={{ alignSelf: 'center' }}>
                                       
                                        <Text>{item.title !== null && item.title.length > 35 ? item.title.substr(0, 35) + "...." : item.title}</Text>
                                        <Text>{item.message.length > 35 ? item.message.substr(0, 35) + "...." : item.message}</Text>
                                    </View>
                                    <View style={{ alignSelf: 'center' }}>
                                    <Image source={backArrow} style={{ height:15, width:15 }} />
                                    </View>
                                </View>
                            </TouchableOpacity>
                        }
                        ItemSeparatorComponent={this.renderSeparator} />

                    : 
                    <View style={{flex:1,justifyContent:'center' }}>

                        <View style={{ alignSelf: 'center' }}>
                           <Text style={{fontSize:15,fontWeight:'bold'}}>No messages avaiable for this network. </Text>
                        </View>
                      </View>
                    }

                    {loader && <Loader/>}

                

              </View>


        )

    }


}

const styles = {
    container: {
        flex: 1,
        backgroundColor: 'red',
        alignItems: 'center',
        // justifyContent: 'center'
    },

    msgTextStatusView: {
        borderRadius: 2, alignSelf: 'flex-start', padding: 4
    },
    cardContainer: {
        width: width - 50,
        height: height - 250,
        backgroundColor: '#FFF',
        borderRadius: 10,
        justifyContent: 'space-between'
    },
    modalContainer: {
        justifyContent: 'center', alignItems: 'center',
        backgroundColor: 'green',
        height: height - 150, width: width - 50, borderRadius: 20
    },
    imgStyle: {
        width: 120,
        height: 120, resizeMode: 'contain'
    },

}


export default MessageInfoCard;