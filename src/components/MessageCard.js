import React from 'react';
import { View, Text, Dimensions, Image, ImageBackground, TouchableOpacity, FlatList } from 'react-native';
import mailbox from '../imgAssests/mailbox.png';
import { networkContactList } from '../store/actions/BuzzAction';
import userProfile from '../constants/UserProfile';


const { width, height } = Dimensions.get('window')
let userName = "Valentine";
let userMsg = "Classic aptent taciticiClassic aptent taciticiClassic aptent taciticiClassic aptent tacitici"



class MessageCard extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            contactList:userProfile,
            dupConList:userProfile,
            networkId: props.networkId,
            isFetching:false
        }
        //this.getNetworkContacts();
    }

    onConnect = () => {

        //this.props.navigation.navigate("ContactsInfo", { data: 'Message' })
        this.props.navigation.navigate("ContactsInfo", { data: 'Message',id :this.state.networkId })

    }

    //network contact list api's calling here

    async getNetworkContacts() {

        let list = await networkContactList(this.state.networkId);
        console.log("listssss", list)
        this.setState({ contactList: list, dupConList: list, isLoading: false, isFetching: false })
        console.warn("uuu")

    }

    async componentWillReceiveProps(props) {
        console.log("propsssssss", props.search)
        let list = await networkContactList(this.state.networkId);
       // this.setState({ contactList: list, dupConList: list, isLoading: false, isFetching: false })
        let isSearch = props.search.length > 0 ? this.setState({ isSearch: true }) : this.setState({ isSearch: false })
        let text = props.search.toLowerCase()
        let contactList = this.state.dupConList
        let filteredName = contactList.filter((item) => {
            return item.firstName.toLowerCase().match(text) || item.lastName.toLowerCase().match(text)
        })
        if (!text || text === '') {

            console.warn('back')
            this.setState({
                contactList: this.state.dupConList
            })
        } else if (!Array.isArray(filteredName) && !filteredName.length) {
            // set no data flag to true so as to render flatlist conditionally
            this.setState({
                noData: true
            })
        } else if (Array.isArray(filteredName)) {

            this.setState({
                noData: false,
                contactList: filteredName
            })
        }
    }

    renderEmptyContactCard = () => {
        return (
            <View style={{ alignItems: 'center', top: 100 }}>
                <Image source={mailbox} style={styles.imgStyle} />
                <Text style={{ textAlign: 'center', color: 'grey', fontSize: 18 }}>
                    You can't send messages until {'\n'} you <Text style={{ textDecorationLine: 'underline', }}
                        onPress={() => this.onConnect()}> connect your contacts</Text>
                </Text>

            </View>


        )
    }
    
    onRefresh() {
        this.setState({ isFetching: true }, function () { this.getNetworkContacts() });
    }



    render() {
        let { contactList } = this.state;
        let dimension = 60
        return (


            <View>

              {contactList.length > 0  &&

                 <View style={{ top: 10, flexDirection: 'row-reverse', right: 20 }}>
                    <View style={{ backgroundColor: 'red', height: 30, width: 100, borderRadius: 5, justifyContent: 'center' }}>
                        <Text onPress={() => this.onConnect()} style={{ color: 'white', fontWeight: '800', textAlign: 'center' }}> + Add User</Text>
                    </View>

                </View>}
                {contactList.length > 0 ?
                    <FlatList
                        data={contactList}
                        onRefresh={() => this.onRefresh()}
                        refreshing={this.state.isFetching}
                        style={{top:30}}
                        renderItem={({ item }) =>

                        //onPress={()=>this.props.navigation.navigate('UserDetails')}
                            <TouchableOpacity style={{ alignItems: 'center' }} onPress={()=>this.props.navigation.navigate('UserDetails')}>

                                <View style={styles.cardViewContainer}>

                                    {/* profile view */}

                                    <View style={{ flexDirection: 'row' }}>
                                        {/* <ImageBackground
                                    style={styles.profileImg}
                                    imageStyle={{ borderRadius: 75 / 2 }}
                                    resizeMode="cover"
                                    source={item.profile}>

                                    <View style={[styles.statusView, { backgroundColor: 'green' }]} />
                                </ImageBackground> */}

                                        <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                                            {item.hasThumbnail ? <Image
                                                style={{ width: dimension, height: dimension, alignSelf: 'center', borderRadius: dimension / 2 }}
                                                source={{ uri: item.thumbnailPath }}
                                            /> : <View style={{ height: dimension, width: dimension, borderRadius: dimension / 2, backgroundColor: "#ccc", justifyContent: 'center', alignItems: 'center' }}>
                                                    <Text style={{ fontSize: 18, fontWeight: '800', color: '#fff' }}>{item.firstName ? item.firstName.charAt(0) : ''}{item.lastName ? item.lastName.charAt(0) : ''}</Text></View>}
                                        </View>

                                        <View style={{ alignSelf: 'center', paddingHorizontal: 10 }}>
                                            <Text>{item.firstName} {item.lastName}</Text>
                                            {/* <Text>{userMsg.length > 5 ? userMsg.substr(0, 30) + "...." : userMsg}</Text> */}
                                        </View>
                                    </View>

                                    {/* timestamp view */}


                                    <View>
                                        <Text>10.00 AM</Text>
                                        <View style={styles.badgeViewContainer}>
                                            <Text style={styles.badgeTextStyle}>2</Text>
                                        </View>
                                    </View>
                                </View>


                            </TouchableOpacity>
                        } /> : this.renderEmptyContactCard()
                }
            </View>

        )
    }




}

const styles = {
    cardViewContainer: {
        flex: 1, flexDirection: 'row', height: 85, width: width - 20,
        alignItems: 'center',
        justifyContent: 'space-between',
        //padding:5,
    },
    badgeViewContainer: {
        height: 20, width: 20, borderRadius: 20, backgroundColor: 'red', alignItems: 'center',
        left: height > 1000 ? (width - 20) / 18 : (width - 20) / 10
    },
    badgeTextStyle: {
        fontSize: 15, color: 'white'
    },
    statusView: {
        height: 10, width: 10, borderRadius: 10, top: 10
    },
    profileImg: {
        height: 60, width: 60, borderRadius: 60 / 2,
        justifyContent: 'flex-start', flexDirection: 'row-reverse'
    },
    imgStyle: {
        height: 200, width: 200, resizeMode: 'contain'
    },
}
export default MessageCard;