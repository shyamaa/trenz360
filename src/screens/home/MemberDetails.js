import React from 'react';
import {
    View, ImageBackground, StyleSheet, Dimensions, Text, Image, TouchableOpacity, ScrollView,
    Modal as Popup, Linking, Share, FlatList
} from 'react-native';
import ScrollableTabView, { ScrollableTabBar } from 'react-native-scrollable-tab-view';
import { RFValue } from "react-native-responsive-fontsize";
import { getOr } from 'lodash/fp'
import backArrow from '../../imgAssests/CustomBack.png'
import Swiper from 'react-native-deck-swiper';
import Hyperlink from 'react-native-hyperlink';
import picture1 from '../../imgAssests/picture1.png'
import OverlayLabel from '../../components/OverlayLabel';
import cross from '../../imgAssests/cross.png';
import shareIcon from '../../imgAssests/shareIcon.png';
import rightArrow from '../../imgAssests/right-arrow-round-red.png';
import SocialButton from '../../components/SocialButton';
import { facebookPost } from '../../components/PostUtils'
import catImage from '../../imgAssests/ctaImage.png';
import Modal from 'react-native-modalbox'
import axios from 'axios'
import { isEmpty } from '../../utils/Validations';
import Loader from '../../components/Loader';
import { networkContactList } from '../../store/actions/BuzzAction'
import chatIcon from '../../assets/buzz360Icon/chatIcon.png'
import phoneIcon from '../../assets/buzz360Icon/phoneIcon.png'
import Communications from 'react-native-communications';
const { width, height } = Dimensions.get('window');

let TwitterParameters = '';


class PostCard extends React.Component {

    constructor(props) {
        super(props);
        console.log("post list props", props)
        this.state = {
            swiperVisiable: true,
            postList: props.postLists,
            networkId: props.id
        }
    }

    componentDidMount() {

        let { networkId } = this.state

        let self = this;


    }




    render() {
        console.log("this.state.postList", this.state.postList)
        console.log("pporpsss", this.props)

        let lastOpen = new Date().getTime()


        return (

            <FlatList
                data={this.state.postList}
                renderItem={({ item, index }) =>

               

               

                    <TouchableOpacity style={{ flex: 1, padding: 20, flexDirection: 'row' }} onPress={() => this.props.onpress(index, item)} disabled>


                        <ImageBackground 
                           source={picture1}
                           imageStyle={{ borderRadius: 5 }} style={{ height: 70, width: 100 }}>
                               {
                                 new Date(item.expiredAt).getTime() < lastOpen ? 

                                 <View style={[styles.msgTextStatusView,{backgroundColor : 'blue'}]}>
                                   <Text style={{ color: '#FFF', fontSize: 10, textAlign: 'center' }}>
                                     Expired
                                  </Text>
                                </View> :
                                 <View style={[styles.msgTextStatusView,{backgroundColor : 'red'}]}>
                                   <Text style={{ color: '#FFF', fontSize: 10, textAlign: 'center' }}>
                                    NEW
                                   </Text>
                                 </View>
                            } 
                           </ImageBackground>

                        <View style={{ paddingLeft: 10, width: 250, marginTop: 5 }}>

                            <View>
                                <Hyperlink linkDefault={true} linkStyle={{ textDecorationLine: 'underline' }}>
                                    <Text style={{ flexWrap: 'wrap', fontWeight: '500', fontSize: 12 }}>
                                        {item.message}</Text>
                                </Hyperlink>
                            </View>

                            <TouchableOpacity style={{ top: 5, flexDirection: 'row' }} onPress={() => this.props.onpress(index, item)}>
                                <Image source={shareIcon}
                                    style={{ height: 15, width: 15 }} />
                                <Text style={{ color: 'grey', left: 5 }}>Share</Text>
                            </TouchableOpacity>

                        </View>
                    </TouchableOpacity>

                } />




        )
    }
}

class MessageCard extends React.Component {

    constructor(props) {
        super(props);
        console.log("info card", props)
        this.state = {
            networkId: 0,
            swiperVisiable: false,
            messageList: props.messageList
        }
    }

    renderSeparator = () => {
        return (
            <View
                style={{
                    height: 0.5,
                    width: "90%",
                    alignSelf: 'center',
                    backgroundColor: "grey",
                }}
            />
        );
    };






    render() {
        let { messageList, swiperVisiable } = this.state;
        let lastOpen = new Date().getTime();
        return (
            <View style={{ flex: 1 }}>

                {!isEmpty(messageList) ?

                    <FlatList
                        data={messageList}
                        renderItem={({ item, index }) =>
                            <TouchableOpacity style={{ padding: 15 }} onPress={() => this.props.onpress(index, item)}>
                                {
                                    new Date(item.expiredAt).getTime() < lastOpen ?

                                        <View style={[styles.msgTextStatusView, { backgroundColor: 'blue' }]}>
                                            <Text style={{ color: '#FFF', fontSize: 10, textAlign: 'center' }}>
                                                Expired
                                  </Text>
                                        </View> :
                                        <View style={[styles.msgTextStatusView, { backgroundColor: 'red' }]}>
                                            <Text style={{ color: '#FFF', fontSize: 10, textAlign: 'center' }}>
                                                NEW
                                   </Text>
                                        </View>
                                }
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>

                                    <View style={{ alignSelf: 'center' }}>
                                        <Text>{item.title !== null && item.title.length > 35 ? item.title.substr(0, 35) + "...." : item.title}</Text>
                                        <Text>{item.message !== null && item.message.length > 35 ? item.message.substr(0, 35) + "...." : item.message}</Text>
                                    </View>
                                    <View style={{ alignSelf: 'center' }}>
                                        <Text>></Text>
                                    </View>
                                </View>
                            </TouchableOpacity>
                        }
                        ItemSeparatorComponent={this.renderSeparator} />

                    :
                    <View style={{ flex: 1, justifyContent: 'center' }}>

                        <View style={{ alignSelf: 'center' }}>
                            <Text style={{ fontSize: 15, fontWeight: 'bold' }}>No messages avaiable for this network. </Text>
                        </View>
                    </View>
                }



            </View>


        )

    }

}

class MemberCard extends React.Component {

    constructor(props) {
        super(props);
        console.log("post list props", props)
        this.state = {
            swiperVisiable: true,
            contactList: props.contactList,

        }
    }



    render() {
        let { contactList } = this.state;
        let dimension = 60;

        return (
            <FlatList
                data={contactList}
                renderItem={({ item }) =>
                    <TouchableOpacity style={{ flex: 1, padding: 0, flexDirection: 'row' }} onPress={this.props.onpress}>

                        <View style={{ paddingLeft: 10, width: width - 50, marginTop: 5 }}>

                            <View style={styles.cardViewContainer}>

                                <View style={{ flexDirection: 'row' }}>
                                    {/* <ImageBackground
                                            style={{height:50,width:50}}
                                            imageStyle={{ borderRadius: 75 / 2 }}
                                            resizeMode="cover"
                                            source={item.profile} /> */}

                                    <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                                        {item.hasThumbnail ? <Image
                                            style={{ width: dimension, height: dimension, alignSelf: 'center', borderRadius: dimension / 2 }}
                                            source={{ uri: item.thumbnailPath }}
                                        /> : <View style={{ height: dimension, width: dimension, borderRadius: dimension / 2, backgroundColor: "#ccc", justifyContent: 'center', alignItems: 'center' }}>
                                                <Text style={{ fontSize: 18, fontWeight: '800', color: '#fff' }}>{item.firstName ? item.firstName.charAt(0) : ''}{item.lastName ? item.lastName.charAt(0) : ''}</Text></View>}
                                    </View>

                                    <View style={{ alignSelf: 'center', paddingHorizontal: 10 }}>
                                        <Text>{item.firstName} {item.lastName}</Text>
                                    </View>







                                </View>


                                <View style={{ flexDirection: 'row', marginTop: 0, }}>
                                    <TouchableOpacity onPress={() => Communications.phonecall(item.phone, true)} >
                                        <Image source={phoneIcon} style={{ height: 20, width: 20, resizeMode: 'contain', top: 5, right: 20 }} />
                                    </TouchableOpacity>

                                    <TouchableOpacity onPress={() => Communications.text(item.phone)}>
                                        <Image source={chatIcon} style={{ height: 30, width: 30, resizeMode: 'contain' }} />
                                    </TouchableOpacity>
                                </View>





                            </View>





                        </View>



                    </TouchableOpacity>

                }></FlatList>
        )
    }
}


class MemberDetails extends React.Component {

    constructor(props) {
        console.log(props.navigation.state.params.data, "member list")
        super(props);
        this.state = {
            title: getOr(null, 'name', props.navigation.state.params.data),
            networkId: getOr(null, 'networkId', props.navigation.state.params.data),
            cards: [1],
            id: 0,
            loader: true,
            swiperVisiable: false,
            isMessage: false,
            messageIndex: null,
            postIndex: null,
            postList: [],
            messageList: [],
            contactList: [],
            swipeImg: require('../../imgAssests/picture1.png'),
            isModalShareVisiable: false,
            postURL: 'https://www.facebook.com/LewisForMN/photos/a.10152982397618472/10157190807993472/?type=3&theater',
            postDescription: 'Jason Lewis for Congress – US Senate – Campaign found in zip code 55044 & 55024',
            TwitterShareURL: 'https://www.facebook.com/LewisForMN/photos/a.10152982397618472/10157190807993472/?type=3&theater',
            TweetContent: 'Jason Lewis for Congress – US Senate – Campaign found in zip code 55044 & 55024',
        }

        this.getNetworkContacts();

    }

    async componentDidMount() {

        let { networkId } = this.state

        let self = this;

        // get network messages

        axios.get(`https://290e8otzd9.execute-api.us-west-2.amazonaws.com/dev/network/` + networkId + '/message')
            .then(function (response) {
                console.log("message list response", response.data);
                self.setState({ dataList: response.data, loader: false })
                response.data.map((data, index) => {
                    response.data[index].isPost == true ?
                        self.state.postList.push(response.data[index]) : self.state.messageList.push(response.data[index])
                })
            })
            .catch(function (error) {
                console.log(error);
            });

    }

    //network contact list api's calling here

    async getNetworkContacts() {

        let list = await networkContactList(this.state.networkId);
        this.setState({ contactList: list, isLoading: false })

    }

    onSwipeRight = () => {

        if (this.state.isMessage) {
            this.refs.modal1.open()
            this.setState({ swiperVisiable: false, isMessage: false })

        } else {
            this.setState({ isModalShareVisiable: true, swiperVisiable: false },
                // this.refs.modal1.open()
            )
        }


    }

   

    // sending messages of their specific types

    onSendingMessage = (type, firstName, phone, message) => {



        switch (type) {
            case 'facebook': this.fbMessagenerSharing(); break;
            case 'email': Communications.email(['emailAddress1', 'emailAddress2'], null, null, 'My Subject', 'My body text'); break;
            case 'text': Communications.text(phone, message); break;
            case 'phone': Communications.phonecall('9999999999'); break;
            default: this.refs.modal1.close(), this.setState({ firstName: firstName }), this.refs.modalContactMethods.open();
                break;
        }

    }

    renderCard = () => {

        let { postList, postIndex } = this.state;

        console.log(postList, "popopopopp")

        return (

            <View style={styles.cardContainer}>

                <View style={{ flex: 1, backgroundColor: 'white', alignItems: 'center', padding: 0, borderRadius: 25, }}>
                    <Image source={this.state.swipeImg}
                        style={{ height: height * 0.25, width: width - 30, borderTopRightRadius: 20, borderTopLeftRadius: 20 }} />
                    <ScrollView style={{ flex: 1 }} persistentScrollbar={true}>
                        <Text style={{ color: '#2F3979', fontWeight: '800', fontSize: RFValue(18), textAlign: 'center', marginTop: 10, width: 300, }}>
                            {postList[postIndex].title} </Text>

                        <Hyperlink linkDefault={true} linkStyle={{ textDecorationLine: 'underline' }}>
                            <Text style={{
                                color: 'grey', flexWrap: 'wrap', flex: 1,
                                fontSize: RFValue(14), textAlign: 'center', top: 12, width: 300,
                            }}>{postList[postIndex].message} </Text>
                        </Hyperlink>
                    </ScrollView>

                </View>


                <View style={{
                    backgroundColor: '#2F3979', borderBottomEndRadius: 10,
                    borderBottomStartRadius: 10, height: 80, alignItems: 'center', flexDirection: 'row', justifyContent: 'space-evenly'
                }}>

                    <TouchableOpacity style={{ bottom: 20 }} onPress={() => this.swiper.swipeLeft()}>
                        <Image source={cross} style={styles.imgStyle} />
                    </TouchableOpacity>
                    <TouchableOpacity style={{ bottom: 20 }} onPress={() => this.swiper.swipeRight()}>
                        <Image source={rightArrow} style={styles.imgStyle} />
                    </TouchableOpacity>

                </View>

            </View>



        )

    }

    renderMessageCard = () => {
        console.warn(this.state.id)

        let { messageIndex } = this.state;

        return (

            <View style={styles.cardContainer}>
                <View style={{ flex: 1, backgroundColor: 'white', alignItems: 'center', padding: 10, borderRadius: 25 }}>
                    <Image source={catImage} style={{ height: 200, width: 200, resizeMode: 'contain', marginTop: 10 }} />
                    <Text style={{ color: '#2F3979', fontWeight: '800', fontSize: 20, textAlign: 'center' }}>
                        {this.state.messageList[messageIndex].title}</Text>

                    <Text style={{
                        color: 'grey',
                        fontSize: 16, textAlign: 'center', top: 20
                    }}>{this.state.messageList[messageIndex].message} </Text>

                </View>

                <View style={{
                    backgroundColor: '#2F3979', borderBottomEndRadius: 10,
                    borderBottomStartRadius: 10, height: 80, alignItems: 'center', flexDirection: 'row', justifyContent: 'space-evenly'
                }}>

                    <TouchableOpacity style={{ bottom: 20 }} onPress={() => this.swiper.swipeLeft()}>
                        <Image source={cross} style={styles.imgStyle} />
                    </TouchableOpacity>
                    <TouchableOpacity style={{ bottom: 20 }} onPress={() => this.swiper.swipeRight()}>
                        <Image source={rightArrow} style={styles.imgStyle} />
                    </TouchableOpacity>

                </View>

            </View>



        )

    }

    onSelect = (title) => {
        this.setState({ setTitle: title })


    }

    sharePost = () => {

        switch (this.state.setTitle) {
            case 'Facebook':
                this.postOnFacebook();
                break;
            case 'Instagram':
                this.onShare();
                break;
            case 'Twitter':
                this.postOnTwitter();
                break;
            case 'Snapchat':
                this.onShare();
                break;
            default: break;
        }
    }

    postOnFacebook = () => {

        let { postURL, postDescription } = this.state;

        let url = 'https://www.facebook.com/sharer/sharer.php' + facebookPost(postURL, postDescription);

        Linking.openURL(url).then((data) => {
            console.log('Facebook Opened');
        }).catch(() => {
            console.log('Something went wrong');
        });

    }

    postOnTwitter = () => {

        let { TwitterShareURL, TweetContent } = this.state;



        if (TwitterShareURL != undefined) {
            if (TwitterParameters.includes('?') == false) {
                TwitterParameters =
                    TwitterParameters + '?url=' + encodeURI(TwitterShareURL);
            } else {
                TwitterParameters =
                    TwitterParameters + '&url=' + encodeURI(TwitterShareURL);
            }
        }
        if (TweetContent != undefined) {
            if (TwitterParameters.includes('?') == false) {
                TwitterParameters =
                    TwitterParameters + '?text=' + encodeURI(TweetContent);
            } else {
                TwitterParameters =
                    TwitterParameters + '&text=' + encodeURI(TweetContent);
            }
       
        }


        let url = 'https://twitter.com/intent/tweet' + TwitterParameters;

        Linking.openURL(url)
            .then(data => {
                console('Twitter Opened');
            })
            .catch(() => {
                console.log("error")
            });
    };

    onShare = () => {

        let shareOptions = {
            title: 'Swipe',
            message: this.state.postDescription,
            url: this.state.postURL,
        };

        Share.share(shareOptions);
    }


    childHandler(dataFromChild) {
        let self = this
        // log our state before and after we updated it
        console.log('%cPrevious Parent State: ' + dataFromChild, "color:orange");
        self.setState({ swiperVisiable: true, isMessage: true, id: self.state.networkId, messageIndex: dataFromChild })

    }

    postChildRes(dataFromChild) {
        let self = this
        // log our state before and after we updated it
        console.log('%cPrevious Parent State: ' + dataFromChild, "color:orange");
        self.setState({ swiperVisiable: true, isMessage: false, id: self.state.networkId, postIndex: dataFromChild })

    }





    render() {
        let { title, swiperVisiable, messageList, messageIndex, contactList } = this.state;
        console.log("contactList", contactList)
        return (
            <View style={{ flex: 1, backgroundColor: '#FFF' }}>

                {/* header view container */}

                <ImageBackground source={picture1} style={styles.imageHeaderView} imageStyle={{ opacity: 0.9 }} >

                    <View style={styles.headerContainerView}>

                        <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                            <Image source={backArrow} style={styles.backBtn} />
                        </TouchableOpacity>


                        <Text style={styles.headerText}>{title}</Text>

                    </View>

                </ImageBackground>

                {/* main view container */}



                <ScrollableTabView
                    initialPage={0}
                    tabBarInactiveTextColor={'grey'}
                    tabBarUnderlineStyle={{ backgroundColor: '#4151AF', width: 100, height: 2 }}
                    tabBarActiveTextColor={'#4151AF'}
                    tabBarTextStyle={{ fontSize: 12 }}
                    renderTabBar={() => <ScrollableTabBar />}>

                    <PostCard tabLabel="Posts" onpress={this.postChildRes.bind(this)} id={this.state.networkId} postLists={this.state.postList} />
                    <MessageCard tabLabel="Messages" id={this.state.networkId} onpress={this.childHandler.bind(this)}
                        messageList={this.state.messageList} />
                    <MemberCard tabLabel="Members" contactList={this.state.contactList} />








                </ScrollableTabView>


                {swiperVisiable &&
                    <Swiper
                        ref={swiper => {
                            this.swiper = swiper
                        }}
                        verticalSwipe={false}
                        cards={this.state.cards}
                        renderCard={this.state.isMessage ? this.renderMessageCard : this.renderCard}
                        stackSize={3}
                        stackSeparation={15}
                        animateOverlayLabelsOpacity
                        onSwipedRight={() => this.onSwipeRight()}
                        onSwipedLeft={() => this.setState({ swiperVisiable: false, isMessage: false })}
                        animateCardOpacity
                        showSecondCard={false}
                        onTapCardDeadZone={1}
                        backgroundColor={'#00000070'}
                        overlayLabels={{
                            left: {
                                title: 'Close',
                                element: <OverlayLabel label="left" color="grey" direction={"left"} isClose={true} />,
                                style: {
                                    label: {
                                        backgroundColor: 'black',
                                        borderColor: 'black',
                                        color: 'white',
                                        borderWidth: 1
                                    },
                                    wrapper: {
                                        flexDirection: 'column',
                                        alignItems: 'center',
                                        justifyContent: 'center',
                                        marginTop: 30,
                                        marginLeft: -30
                                    }
                                }
                            },
                            right: {
                                title: 'Forward',
                                element: <OverlayLabel label="right" color="grey" direction={"right"} />,
                                style: {
                                    label: {
                                        backgroundColor: 'black',
                                        borderColor: 'black',
                                        color: 'white',
                                        borderWidth: 1
                                    },
                                    wrapper: {
                                        flexDirection: 'column',
                                        alignItems: 'center',
                                        justifyContent: 'center',
                                        marginTop: 30,
                                        marginLeft: 30
                                    }
                                }
                            },
                        }}
                        containerStyle={{
                            justifyContent: 'center', top: 0, backgroundColor: '#00000070'
                        }} />
                }



                {/* modal for user contact lists */}

                <Popup visible={this.state.isModalShareVisiable} backdrop={false} transparent={true} style={{ flex: 1, backdropColor: '#999999', backdropOpacity: 0.5 }} >

                    <View style={styles.modalContainer}>

                        <View style={styles.mainViewContainer}>

                            <Text onPress={() => this.setState({ isModalShareVisiable: false })} style={{ padding: 20, color: 'black' }}>X</Text>

                            <View style={{ alignItems: 'center', padding: 0 }}>
                                <Text style={styles.infoTextStyle}>Where would you like to {'\n'} share this post ? </Text>
                                <View style={{ flex: 1, padding: 30, justifyContent: 'space-between' }}>

                                    <SocialButton title={'Facebook'} bgColor={'blue'} setTitle={this.state.setTitle}
                                        onpress={() => this.onSelect('Facebook')}
                                        isCheck={true} titleColor={'#FFF'} />
                                    <View style={{ height: 10 }} />
                                    <SocialButton title={'Instagram'} bgColor={'red'} onpress={() => this.onSelect('Instagram')}
                                        setTitle={this.state.setTitle}
                                        titleColor={'#FFF'} />
                                    <View style={{ height: 10 }} />
                                    <SocialButton title={'Snapchat'} bgColor={'yellow'} setTitle={this.state.setTitle}
                                        onpress={() => this.onSelect('Snapchat')} titleColor={'#000'} />
                                    <View style={{ height: 10 }} />
                                    <SocialButton title={'Twitter'} bgColor={'#0077be'} setTitle={this.state.setTitle}
                                        onpress={() => this.onSelect('Twitter')} titleColor={'#FFF'} />
                                    <View style={{ height: 20 }} />
                                    <TouchableOpacity
                                        disabled={this.state.setTitle !== null ? false : true}
                                        style={[styles.shareBtnStyle, { backgroundColor: this.state.setTitle !== null ? 'blue' : '#D3D3D3' }]}
                                        onPress={() => this.sharePost()} >
                                        <Text style={styles.shareBtnTextStyles}>Share</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>



                        </View>

                    </View>

                </Popup>


                {/* modal for user contact lists */}

                <Modal ref={"modal1"} backdrop={false} style={styles.modal}>

                    <View style={styles.modalView}>

                        <Text onPress={() => this.refs.modal1.close()} style={{ padding: 20, color: 'black' }}>X</Text>
                        <View style={{ alignItems: 'center', padding: 20 }}>
                            <Text style={{
                                color: '#004772', fontWeight: '500',
                                fontSize: 22, textAlign: 'center'
                            }}>{messageList[messageIndex] !== undefined && messageList[messageIndex].title} </Text>

                            <Text style={{
                                color: 'grey',
                                fontSize: 16, textAlign: 'center', top: 20
                            }}>{messageList[messageIndex] !== undefined && messageList[messageIndex].message}  </Text>
                        </View>

                        <View style={{ height: 20 }} />

                        <FlatList
                            data={this.state.contactList}
                            renderItem={({ item, index }) =>

                                <View style={{ padding: 15, flexDirection: 'row', justifyContent: 'space-between' }}>
                                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                        {/* <Image source={item.profile} style={{ height: 40, width: 40, borderRadius: 20 }} /> */}
                                        <View style={{ height: 60, width: 60, borderRadius: 60 / 2, backgroundColor: "#ccc", justifyContent: 'center', alignItems: 'center' }}>
                                            <Text style={{ fontSize: 18, fontWeight: '800', color: '#fff' }}>{item.firstName ? item.firstName.charAt(0) : ''}{item.lastName ? item.lastName.charAt(0) : ''}</Text></View>
                                        <Text style={{ color: '#000', fontSize: 10, textAlign: 'center', paddingHorizontal: 5, fontWeight: '500' }}>{item.firstName} {item.lastName}</Text>
                                    </View>
                                    <View style={{ flexDirection: 'row', alignSelf: 'center' }}>
                                        <TouchableOpacity onPress={() => this.onSendingMessage("text", item.firstName, item.phone, messageList[messageIndex].message)}
                                            style={{ borderRadius: 2, alignSelf: 'flex-start', padding: 4, borderColor: 'blue', borderWidth: 1, right: 10 }}>
                                            <Text style={{ color: 'blue', fontSize: 10, textAlign: 'center', fontWeight: '500' }}>
                                                Send via text
                                            </Text>
                                        </TouchableOpacity>
                                        <TouchableOpacity>

                                            <Text style={{ color: 'red', fontSize: 15 }}>x</Text>

                                        </TouchableOpacity>

                                    </View>
                                </View>

                            }
                            ItemSeparatorComponent={this.renderSeparator} />
                    </View>

                </Modal>

                {this.state.loader && <Loader />}

            </View>
        )
    }
}

const styles = StyleSheet.create({

    cardViewContainer: {
        flex: 1, flexDirection: 'row', height: 85, width: width - 20,
        padding: 10,
        alignItems: 'center',
        justifyContent: 'space-between',
    },

    imageHeaderView: {
        height: height / 3,
        width: width,

    },
    headerContainerView: {
        flexDirection: 'row', marginTop: height / 14, paddingHorizontal: 10
    },
    headerText: {
        fontSize: 15,
        color: '#FFF',
        fontWeight: '800',
        paddingHorizontal: 10
    },
    backBtn: {
        height: 25, width: 25, resizeMode: 'contain', top: -2
    },
    cardContainer: {
        width: width - 30,
        height: height > 700 ? 550 : 450,
        backgroundColor: '#FFF',
        alignSelf: 'center',
        shadowColor: '#000',
        borderRadius: 50,
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.5,
        shadowRadius: 2,
        elevation: 2,
        top: height * 0.1
    },
    imgStyle: {
        width: 120,
        height: 120, resizeMode: 'contain'
    },
    modalContainer: {
        justifyContent: 'center', alignItems: 'center',
        height: height, width: width, borderRadius: 20,
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.5,
        shadowRadius: 2,
        elevation: 2,
        backgroundColor: '#00000070',



    },
    modalView: {
        backgroundColor: 'white', height: height, width: width - 20, borderRadius: 20
    },
    modal: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#00000070',
        width: width - 20,
        height: height,
        borderRadius: 20,
        top: height / 12
    },
    mainViewContainer: {
        backgroundColor: 'white',
        height: 450, width: width - 50,
        borderRadius: 20
    },
    infoTextStyle: {
        color: '#004772', fontWeight: '500',
        fontSize: 18, textAlign: 'center'
    },
    shareBtnStyle: {
        width: width - 70, height: 50, borderRadius: 10, justifyContent: 'center'
    },
    shareBtnTextStyles: {
        color: '#FFF',
        fontWeight: '500', fontSize: 14, textAlign: 'center'
    },
    msgTextStatusView: {
        borderRadius: 2, alignSelf: 'flex-start', padding: 4
    },



})
export default MemberDetails;