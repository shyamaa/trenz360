export default {

    headerTextColor:'#000000',
    placeholderColor:'#666666',
    lineColor:'#E4E4E4',
    loaderColor:'#008161',
    textColor:{
        teal:'#008161',
        black:'#030303',
        gray:'#666666',
        darkBlack:'#000000',
        white:'#FFFFFF'
    },
    button:{
        bgColor:'#008161',
        textColor:'#FFFFFF'
    },
    background:{
        white:'#FFFFFF',
        lightGray:'#F7F7F7',
        gray:'#666666',
    },


}