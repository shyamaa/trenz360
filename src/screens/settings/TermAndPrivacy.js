import React, { Component } from 'react';
import { Text, View, TouchableOpacity, ScrollView, FlatList, Modal, Dimensions, Image } from 'react-native';
import { WebView } from 'react-native-webview';
import cancel from '../../imgAssests/cancel.png';
import backArrow from '../../imgAssests/backRightArrow.png';
import backArrowGrey from '../../imgAssests/backArrowGrey.png';

const { width, height } = Dimensions.get('window')





const listItems = [
    {
        'itemName': 'Privacy Policy',
        'label': 'Privacy'
    },
    {
        'itemName': 'User Terms of Service',
        'label': 'Terms'
    },
    {
        'itemName': 'Acceptable Use Policy',
        'label': 'Acceptable'
    },
    {
        'itemName': 'Cookie Statement',
        'label': 'Cookie'
    },


]

export default class TermAndPrivacy extends Component {

    constructor() {
        super();
        this.state = {
            isLoading: false,
            url: "",
            isModalVisible: false
        }
    }


   

    // handleClick for each items

    handleClick = (type) => {
        switch (type) {
            case 'Privacy': this.setState({ url: 'https://www.google.com', isModalVisible: true }); break;
            case 'Terms': this.setState({ url: 'https://www.facebook.com/', isModalVisible: true }); break;
            case 'Acceptable': this.setState({ url: 'https://www.amazon.in/', isModalVisible: true }); break;
            case 'Cookie': this.setState({ url: 'https://www.amazon.in/', isModalVisible: true }); break;
            default: break;
        }
    }

    // line separotor for flatlist

    renderSeparator = () => {
        return (
            <View
                style={{
                    height: 0.5,
                    width: "95%",
                    alignSelf: 'center',
                    backgroundColor: "#d3d3d3",
                }}
            />
        );
    };


    renderWebView = () => {
        return (
            <WebView
                source={{ uri: this.state.url }}
                style={{ marginTop: 20 }}
            />
        )
    }




    render() {

        return (
            <ScrollView style={{ flex: 1, backgroundColor: '#FFFFFF' }} keyboardShouldPersistTaps={'handled'}>

                 {/* header view */}

                 <View style={{ flexDirection: 'row', padding: 20,marginTop:height>700 ? 50 : 20 }}>
                        <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                            <Image source={backArrowGrey} style={{ height: 20, width: 20 }} />
                        </TouchableOpacity>

                        <Text style={{ paddingHorizontal: 20, fontWeight: '800', fontSize: 15, color: '#565656' }}>Terms & Privacy</Text>
                    </View>
                    <View style={{height:0.2,width:width,backgroundColor:'#696969'}}/>



                {/* main view conatiner */}

                <View style={{ flex: 1, backgroundColor: '#FFFFFF' }}>

                    <FlatList
                        data={listItems}
                        renderItem={({ item }) =>
                            <TouchableOpacity style={styles.container} onPress={() => this.handleClick(item.label)}>
                                <View style={{ paddingHorizontal: 30 }}>
                                    <Text style={styles.textStyle}>{item.itemName} </Text>
                                </View>

                                <View>
                                    <Image source={backArrow} style={{height:15,width:15,resizeMode:'contain'}}/>
                                </View>


                            </TouchableOpacity>

                        } ItemSeparatorComponent={this.renderSeparator} />



                </View>

                {/*  */}

                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={this.state.isModalVisible}
                    onRequestClose={() => {
                        alert('Modal has been closed.');
                    }}>
                    <View style={{ flex: 1, backgroundColor: '#F4F4F4' }}>

                        <View style={styles.modalView}>

                            {/* header */}
                            <View style={{ padding: 20, flexDirection: 'row', justifyContent: 'space-between' }}>
                                <TouchableOpacity onPress={() => this.setState({ isModalVisible: false })}>
                                    <Image source={cancel} style={{ height: 20, width: 20 }} />
                                </TouchableOpacity>
                                <Text>{this.state.url.length >25 ? this.state.url.substring(0,20)+"....": this.state.url }</Text>
                                <TouchableOpacity>
                                    <Text></Text>
                                </TouchableOpacity>

                            </View>
                            <View style={{ width: width - 50, height: 0.5, backgroundColor: 'grey' }} />

                            {/* body */}

                            <WebView
                                source={{ uri: this.state.url }}
                                style={{ flex: 1, marginTop:0 }}
                            />


                            <View style={{ width: width - 50, height: 0.5, backgroundColor: 'grey' }} />

                            {/* footer */}
                            
                            <View style={{ height:40, alignItems: 'center', justifyContent: 'center' }}>
                                
                                

                            </View>


                        </View>

                    </View>
                </Modal>




            </ScrollView>


        );
    }
}

const styles = {

    container: {
        margin: 10,
        paddingVertical: 10,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },

    textStyle: {
        fontSize: 14, fontWeight: '600', color: '#959595'
    },
    rightArrow: {
        paddingHorizontal: 10,
    },
    modalView: {
        height: height - 150,
        width: width - 50,
        backgroundColor: 'white',
        alignSelf: 'center',
        borderRadius: 10, marginTop:70,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.5,
        shadowRadius: 2,
        elevation: 2,

    }
}