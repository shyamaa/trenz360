
import {AsyncStorage} from 'react-native';

const TAG = "SaveDetails File => \n"

export function saveLoginDetails(userDetails) {
    AsyncStorage.setItem('loggedUserDetails', userDetails, () => {
        console.log("Saved LoggedIn UserDetails");
    });
}


export function saveGlobalConfigurations(gConfs) {
    AsyncStorage.setItem('glConfs', gConfs, () => {
        console.log("Saved Global Configurations");
    });
}

export function saveFitbitURL(auth_URL) {
    AsyncStorage.setItem('ftbtAuthURL', auth_URL, () => {
        console.log("Saved Fitbit url");
    });
}

export function deleteFitbitURL(auth_URL) {
    AsyncStorage.removeItem('ftbtAuthURL',() => {
        console.log("Removed Fitbit URL");
    })
}

export async function fetchFitbitAuthURL(){
    let fitbitURL = await AsyncStorage.getItem('ftbtAuthURL')
    if (fitbitURL)
        return fitbitURL;
    else
        return '';
}

export async function removeLoggedUserCredentials() {
   await AsyncStorage.clear().then(r => {
        console.log('The aync storage is cleared');
    })
}

export async function getAuthToken() {
    let loggedInUserDetails = await AsyncStorage.getItem('loggedUserDetails')
    let userInfo = JSON.parse(loggedInUserDetails);
    if (loggedInUserDetails){
        return userInfo.user.session_id;
    } else {
        return false;
    }
}


export async function getPointName() {
    let configs = await AsyncStorage.getItem('glConfs')
    let configInfo = JSON.parse(configs);
    console.log('THe user is' + JSON.stringify(configInfo))
    if (configs){
        return configInfo.data.HN_POINT_NAME.toString();
    } else {
        return "";
    }
}

export async function getStepsThreshold() {
    let configs = await AsyncStorage.getItem('glConfs')
    let configInfo = JSON.parse(configs);
    if (configs){
        let rules = configInfo.data.rules;
        var index = rules.findIndex(p => p.activity == "steps")
        //console.log(TAG+"Steps ThreshHold => "+rules[index].initial_criteria);
        return rules[index].initial_criteria;
    } else {
        return 0;
    }
}

export async function getUserName() {
    let loggedInUserDetails = await AsyncStorage.getItem('loggedUserDetails')
    let userInfo = JSON.parse(loggedInUserDetails);
    if (loggedInUserDetails){
        return userInfo.user.email;
    } else {
        return false;
    }
}

export async function getUserFullName() {
    let loggedInUserDetails = await AsyncStorage.getItem('loggedUserDetails')
    let userInfo = JSON.parse(loggedInUserDetails);
    if (loggedInUserDetails){
        return userInfo.user.name;
    } else {
        return false;
    }
}

export async function getUserProvider() {
    let loggedInUserDetails = await AsyncStorage.getItem('loggedUserDetails')
    let userInfo = JSON.parse(loggedInUserDetails);
    if (loggedInUserDetails){
        return userInfo.user.provider;
    } else {
        return false;
    }
}

// Convenience wrapper around AsyncStorage methods

export const getKeyValue = async function (key) {
    let value = null;
    await AsyncStorage.getItem(key, (err, result) => {
        value = result;
    });
    return value;
};

//Save Social Login Information
export async function saveFBAccessToken(ac_Token){
    await AsyncStorage.setItem('fb_AccessToken', ac_Token, () => {
        console.log("Saved FB LoggedIn UserDetails");
    });
}

export async function getFBAccessToken() {
    let fbAccessToken = await AsyncStorage.getItem('fb_AccessToken')
    return fbAccessToken;
};


