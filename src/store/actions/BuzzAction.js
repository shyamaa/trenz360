import React from 'react';
import { AsyncStorage } from 'react-native'
import { showActionAlert, showInfoAlert } from '../../utils/AlertPopUp';
import axios from 'axios';
import Amplify, { Auth } from 'aws-amplify';
import config from '../../config/aws-exports'
Amplify.configure(config);

let baseUrl = "https://290e8otzd9.execute-api.us-west-2.amazonaws.com/dev/"


// Get My Network Lists 

export const networkList = (token) => {

    const config = {
        headers: { 'Authorization': 'Bearer ' + token }
    };

    return axios.get(baseUrl+'me/networks',
        config)
        .then(function (response) {
            console.log("network response", response.data);
            return response.data;
        })
        .catch(function (error) {
            console.log(error);
        });

}

//Get My Network List Messages

export const networkMessageList = (networkId) => {

    return axios.get(baseUrl+'/network/' + networkId + '/message')
        .then(function (response) {
            console.log("network message list  response", response.data);
            return response.data;
           // self.setState({ messageList: response.data })
        })
        .catch(function (error) {
            console.log(error);
        });

}



// Get My All Posts


export const allPostLists = (token) => {
   
    const config = {
        headers: { 'Authorization': 'Bearer ' + token }
    };

    return axios.get(baseUrl+'/me/posts',
        config)
        .then(function (response) {
            console.log("post list response", response.data);
            return response.data;
        })
        .catch(function (error) {
            console.log(error);
        });
}


  //Get My Network Post



export const networkPostLists = (networkId) => {
   

    return axios.get(`https://290e8otzd9.execute-api.us-west-2.amazonaws.com/dev/network/` + networkId + '/post')
        .then(function (response) {
            console.log("network post response", response.data);
            return response.data;
        })
        .catch(function (error) {
            console.log(error);
        });
}

   // Get My Information


export const getUserInfo = (token)=>{

    const config = {
        headers:{ 'Authorization': 'Bearer ' + token}
    };


     return axios.get(baseUrl+'/me',
          config)
        .then(function (response) {
            console.log("user response",response.data);
            return response.data
        })
        .catch(function (error) {
            console.log(error);
        });


}

export const updateProfilePhoto = (token,data)=>{

    let url = 'https://290e8otzd9.execute-api.us-west-2.amazonaws.com/dev/me/photo'
    return axios({
        method: 'post',
        url: url,
        headers:{ 'Authorization': 'Bearer ' + token}, 
        data: [{
            "isDefault": true,
            "sortOrder": 0
        }]
      }).then(function (response) {
        console.log("update profile response",response);
        return response.data
    })
    .catch(function (error) {
        console.log(error);
    });;


}

export const getProfilePhoto =(token) =>{
    let url = 'https://290e8otzd9.execute-api.us-west-2.amazonaws.com/dev/me/photos'
    return axios({
        method: 'get',
        url: url,
        headers:{ 'Authorization': 'Bearer ' + token}, 
      }).then(function (response) {
        console.log("get profile response",response);
        return response.data
    })
    .catch(function (error) {
        console.log(error);
    });;
}

export const finalUpload=(response,data,token)=>{

    
    return axios({
        method: 'put',
        url: response, 
        headers:{       
             'Content-Type': 'binary/octet-stream',
             'process-Data': 'false'
         }, 
        data: data
      }).then(function (response) {
         console.log("final image response",response);
         return response
    })
    .catch(function (error) {
        console.log(error);
       
    });;
}

  // Upload user images


export const getContacts =(token)=>{

    const config = {
        headers:{ 'Authorization': 'Bearer ' + token}
    };


     return axios.get(baseUrl+'me/contacts',
          config)
        .then(function (response) {
            console.log("user response",response.data);
            return response.data
        })
        .catch(function (error) {
            console.log(error);
        });

}

  // create contacts

  export const createConnect=(token,data)=>{


    return axios({
        method: 'post',
        url: baseUrl+'/me/contacts',
        headers:{ 'Authorization': 'Bearer ' + token}, 
        data: data
      }).then(function (response) {
        console.log("contact response",response);
        return response
    })
    .catch(function (error) {
        console.log(error);
    });;
}


  //get networkContactList


  export const networkContactList=(networkId)=>{

    return axios.get(baseUrl+'network/' + networkId + '/candidate')
    .then(function (response) {
        console.log("network user list", response);
        return response.data;
    })
    .catch(function (error) {
        console.log(error);
    });
  }

  //add networkContactList

  export const addNetworkContactList=(token,networkId,data)=>{

    console.log("data objectsss",data)


    return axios({
        method: 'post',
        url:baseUrl+'network/' + networkId + '/candidates',
        headers:{ 'Authorization': 'Bearer ' + token}, 
        data: data
      }).then(function (response) {
        console.log("contact response",response);
        return response
    })
    .catch(function (error) {
        console.log(error);
    });;

    
}





