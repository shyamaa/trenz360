import React from 'react';
import { View, ImageBackground, StyleSheet, Dimensions, Text, Image, TouchableOpacity, ScrollView, Modal as Popup, Linking, Share, FlatList } from 'react-native';
import ScrollableTabView, { ScrollableTabBar } from 'react-native-scrollable-tab-view';
import backArrow from '../../imgAssests/CustomBack.png'
import picture1 from '../../imgAssests/picture1.png'
import { isEmpty } from '../../utils/Validations';
const { width, height } = Dimensions.get('window');
import DefaultUserPic from '../../imgAssests/DefaultUser.png'
import DemoPost from '../../imgAssests/DemoPost.jpg'

let TwitterParameters = '';


class HistoryCard extends React.Component {

    constructor(props) {
        super(props);
        console.log("post list props", props)
        this.state = {
            swiperVisiable: true,
            postList: props.postLists,
            networkId: props.id,
           // propsvalue: props.propsvalue
        }
    }



    determineRelationship() {
        console.warn("determineRelationship ............");
    }




    render() {
        let dimension = 50;
        let dotDimension = 10;
        

        
        return (
            <View style={{ flex: 1, marginTop: 15, }}>
                <Text style={{fontSize:17, color: 'black',left:20 }}>Relationship</Text> 

                <TouchableOpacity style={{height:50,paddingHorizontal:10,paddingVertical:10, }}
                onPress={() => this.determineRelationship()}>
                <View style={styles.contactsLink} >
                    <Text
                        underlineColorAndroid='black'
                        style={styles.contactsText}>Determine Relationship</Text>
                </View>
            </TouchableOpacity>



                <Text style={{ fontSize: 15, color: 'black', left: 20 }}>Messages</Text>

                <View style={{ flexDirection: 'row', marginTop: 10, minHeight: 60, marginLeft: 20 }}>
                    <ImageBackground
                        style={{
                            height: dimension, width: dimension, borderRadius: dimension / 2,
                            justifyContent: 'flex-start', flexDirection: 'row-reverse'
                        }}
                        imageStyle={{ borderRadius: 75 / 2 }}
                        resizeMode="cover"
                        source={DefaultUserPic}>

                    </ImageBackground> 


                    <View style={{ flex: 1, marginLeft: 10, alignSelf: 'center', paddingHorizontal: 5, }}>
                        <Text style={{
                            fontSize: 13,
                        }}>David Lewis</Text>
                        <Text style={{
                            fontSize: 13,
                        }}>Class aptent taciti sociosqu class....</Text>
                    </View>
                </View>

                <Text style={{ fontSize: 15, color: 'black', left: 20, marginTop: 10, }}>Events</Text>

                <FlatList
                    data={[0, 9, 9, 6, 6]}
                    renderItem={({ item, index }) =>

                        <TouchableOpacity style={{ padding: 20, flexDirection: 'row' }}
                            onPress={() => this.props.onpress(index, item)} disabled>
                            <ImageBackground source={picture1}
                                imageStyle={{ borderRadius: 5 }} style={{ height: 70, width: 100 }} />



                            <View style={{ flex: 1, marginLeft: 10, alignSelf: 'center' }}>
                                <Text style={{

                                    paddingHorizontal: 5,
                                    paddingTop: 5,
                                    fontSize: 15,
                                }}>Climate Change Request</Text>
                                <Text style={{


                                    paddingHorizontal: 5,
                                    //                                
                                    fontSize: 13,
                                }}>May 23 , 2019</Text>
                            </View>


                        </TouchableOpacity>
                    }
                    ItemSeparatorComponent={this.renderSeparator}
                />
            </View>
        )
    }


    renderSeparator = () => {
        return (
            <View
                style={{
                    height: 0.5,
                    width: "100%",
                    alignSelf: 'center',
                    backgroundColor: "grey",
                }}
            />
        );
    };
}

class MessageCard extends React.Component {

    constructor(props) {
        super(props);
        console.log("info card", props)
        this.state = {
            networkId: 0,
            swiperVisiable: false,
            messageList: props.messageList
        }
    }

    renderSeparator = () => {
        return (
            <View
                style={{
                    height: 0.5,
                    width: "90%",
                    alignSelf: 'center',
                    backgroundColor: "grey",
                }}
            />
        );
    };






    render() {
        let { messageList } = this.state;
        return (
            <View style={{ flex: 1 }}>

                {!isEmpty(messageList) ?

                    <FlatList
                        data={messageList}
                        renderItem={({ item, index }) =>
                            <TouchableOpacity style={{ padding: 15 }} onPress={() => this.props.onpress(index, item)}>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>

                                    <View style={{ alignSelf: 'center' }}>
                                        <Text>{item.title !== null && item.title.length > 35 ? item.title.substr(0, 35) + "...." : item.title}</Text>
                                        <Text>{item.message !== null && item.message.length > 35 ? item.message.substr(0, 35) + "...." : item.message}</Text>
                                    </View>
                                    <View style={{ alignSelf: 'center' }}>
                                        <Text>></Text>
                                    </View>
                                </View>
                            </TouchableOpacity>
                        }
                        ItemSeparatorComponent={this.renderSeparator} />

                    :
                    <View style={{ flex: 1, justifyContent: 'center' }}>

                        <View style={{ alignSelf: 'center' }}>
                            <Text style={{ fontSize: 15, fontWeight: 'bold' }}>No Contact Info avaiable for this user. </Text>
                        </View>
                    </View>
                }



            </View>


        )

    }

}




class UserDetails extends React.Component {

    constructor(props) {
        //console.log(props.navigation.state.params.data, "member list")
        super(props);
        this.state = {
            title: '',
            networkId: '',
            cards: [1],
            id: 0,
            loader: false,
            swiperVisiable: false,
            isMessage: false,
            messageIndex: null,
            postIndex: null,
            postList: [],
            messageList: [],
            swipeImg: require('../../imgAssests/picture1.png'),

        }

    }















    childHandler(dataFromChild) {
        let self = this
        // log our state before and after we updated it
        console.log('%cPrevious Parent State: ' + dataFromChild, "color:orange");
        self.setState({ swiperVisiable: true, isMessage: true, id: self.state.networkId, messageIndex: dataFromChild })

    }

    postChildRes(dataFromChild) {
        let self = this
        // log our state before and after we updated it
        console.log('%cPrevious Parent State: ' + dataFromChild, "color:orange");
        self.setState({ swiperVisiable: true, isMessage: false, id: self.state.networkId, postIndex: dataFromChild })

    }





    render() {
        let dimension = 90;

        let { title, swiperVisiable, messageList, propData } = this.state;

       

        return (
            <View style={{ flex: 1, backgroundColor: '#FFF' }}>

                {/* header view container */}
                <ImageBackground source={DemoPost} style={styles.imageHeaderView} imageStyle={{ opacity: 0.9 }} >

                    <View style={styles.headerContainerView}>

                        <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                            <Image source={backArrow} style={styles.backBtn} />
                        </TouchableOpacity>

                        <Text style={styles.headerText}>Contact</Text>

                    </View>


                    <View style={{ flex: 1, justifyContent: 'center', alignSelf: 'center', }}>

                        {/* for image profile  */}

                         <ImageBackground
                            style={{
                                height: dimension, width: dimension, borderRadius: dimension / 2,
                                justifyContent: 'flex-start', flexDirection: 'row-reverse'
                            }}
                            imageStyle={{ borderRadius: 75 / 2 }}
                            resizeMode="cover"
                            source={DefaultUserPic}>

                        </ImageBackground> 

                       

                        <Text style={{
                            flex: 1,
                            textAlign: 'center',
                            paddingHorizontal: 5,
                            paddingVertical: 10,
                            color: 'red',
                            fontSize: 15,
                        }}>David {'\n'} Lewis </Text>
                    </View>



                </ImageBackground>

                {/* main view container */}



                <ScrollableTabView
                    initialPage={0}
                    tabBarInactiveTextColor={'grey'}
                    tabBarUnderlineStyle={{ backgroundColor: '#4151AF', width: 100, height: 2 }}
                    tabBarActiveTextColor={'#4151AF'}
                    tabBarTextStyle={{ fontSize: 12 }}
                    renderTabBar={() => <ScrollableTabBar />}>

                    <HistoryCard tabLabel="History"
                        onpress={this.postChildRes.bind(this)} id={this.state.networkId} postLists={this.state.postList} />
                    <MessageCard tabLabel="Contact Info"
                        id={this.state.networkId}
                        onpress={this.childHandler.bind(this)}
                        messageList={this.state.messageList} />
                   








                </ScrollableTabView>















            </View>
        )
    }
}

const styles = StyleSheet.create({

    imageHeaderView: {
        height: height / 3,
        width: width,

    },
    headerContainerView: {
        flexDirection: 'row', marginTop: 45,
        marginBottom: 15, paddingHorizontal: 10, justifyContent: 'space-between'
    },
    headerText: {
        flex: .65,
        marginTop: 20,
        fontSize: 15,
        color: 'red',
        fontWeight: '800',
        //paddingHorizontal: 10,
        justifyContent: 'center', alignSelf: 'center',
    },
    backBtn: {
        height: 25, width: 25, resizeMode: 'contain', top: -2
    },
    cardContainer: {
        width: width - 30,
        height: height > 700 ? 550 : 450,
        backgroundColor: '#FFF',
        alignSelf: 'center',
        shadowColor: '#000',
        borderRadius: 50,
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.5,
        shadowRadius: 2,
        elevation: 2,
        top: height * 0.1
    },
    imgStyle: {
        width: 120,
        height: 120, resizeMode: 'contain'
    },
    modalContainer: {
        justifyContent: 'center', alignItems: 'center',
        height: height, width: width, borderRadius: 20,
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.5,
        shadowRadius: 2,
        elevation: 2,
        backgroundColor: '#00000070',



    },
    modalView: {
        backgroundColor: 'white', height: height, width: width - 20, borderRadius: 20
    },
    modal: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#00000070',
        width: width - 20,
        height: height,
        borderRadius: 20,
        top: height / 12
    },
    mainViewContainer: {
        backgroundColor: 'white',
        height: 450, width: width - 50,
        borderRadius: 20
    },
    infoTextStyle: {
        color: '#004772', fontWeight: '500',
        fontSize: 18, textAlign: 'center'
    },
    shareBtnStyle: {
        width: width - 70, height: 50, borderRadius: 10, justifyContent: 'center'
    },
    shareBtnTextStyles: {
        color: '#FFF',
        fontWeight: '500', fontSize: 14, textAlign: 'center'
    },

    contactsLink: {
        flex: 1,
        marginLeft: 8
    },
    contactsText: {
        fontSize: 14, color: 'grey', 
        fontWeight: '600', 
        textDecorationLine: 'underline',
    },



})
export default UserDetails;