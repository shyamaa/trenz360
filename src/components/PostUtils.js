export function facebookPost(postURL,postDescription) {

    let parameters = ''

    if (postURL != undefined) {
        if (parameters.includes("?") == false) {
            parameters = parameters + "?u=" + encodeURI(postURL);
        } else {
            parameters = parameters + "&u=" + encodeURI(postURL);
        }
    }
    if (postDescription != undefined) {
        if (parameters.includes("?") == false) {
            parameters = parameters + "?quote=" + encodeURI(postDescription);
        } else {
            parameters = parameters + "&quote=" + encodeURI(postDescription);
        }
    }

    return parameters;

}

export function twitterPost(postURL,postDescription) {

    let parameters = ''

    if (postURL != undefined) {
        if (parameters.includes("?") == false) {
            parameters = parameters + "?u=" + encodeURI(postURL);
        } else {
            parameters = parameters + "&u=" + encodeURI(postURL);
        }
    }
    if (postDescription != undefined) {
        if (parameters.includes("?") == false) {
            parameters = parameters + "?quote=" + encodeURI(postDescription);
        } else {
            parameters = parameters + "&quote=" + encodeURI(postDescription);
        }
    }

    return parameters;

}