import {APPURL_DEV,LOGIN,FORGOT_PASSWORD} from './NetworkString';

export default (()=>{
    return{
       'LOGIN':APPURL_DEV+LOGIN,
       'SIGNUP':APPURL_DEV+SIGNUP,
       'FORGOT_PASSWORD' :APPURL_DEV+FORGOT_PASSWORD
    }
})()