import React, { Component } from 'react';
import {
    Text, View, TouchableOpacity, Dimensions, Image, Alert, ScrollView, FlatList, Platform, AsyncStorage
} from 'react-native';
import CardView from 'react-native-cardview';
import LinearGradient from 'react-native-linear-gradient';
import ImagePicker from 'react-native-image-picker';
import Amplify, { Auth } from 'aws-amplify';
import backArrow from '../../imgAssests/backRightArrow.png';
import config from '../../config/aws-exports';
import axios from 'axios'
import { getUserInfo, updateProfilePhoto, finalUpload, getProfilePhoto } from '../../store/actions/BuzzAction';
import Loader from '../../components/Loader';
import { NavigationActions, StackActions } from 'react-navigation'
Amplify.configure(config);

const { width, height } = Dimensions.get('window')



const listItems = [
    {
        'itemName': 'Account',
        'itemImage': require('../../imgAssests/settings/account.png')
    },
    {
        'itemName': 'Notifications',
        'itemImage': require('../../imgAssests/settings/notifications.png')
    },
    {
        'itemName': 'Resync my contacts',
        'itemImage': require('../../imgAssests/settings/resync.png')
    },
    {
        'itemName': 'Terms & Privacy',
        'itemImage': require('../../imgAssests/settings/terms.png')
    },
    {
        'itemName': 'Logout',
        'itemImage': require('../../imgAssests/settings/logout.png')
    }

]

export default class Settings extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            avatarSource: require('../../imgAssests/DefaultUser.png'),
            userData: [],
            uploadAvatar: []
        }
       // this.getUserInfo();
    }

    async componentDidMount() {

        //this.getUserInfo();
       // this.getProfileImg();

        this.props.navigation.addListener("didFocus", () => {
            //this.setState({isLoading:true})
           // this.getUserInfo();
        })


    }

    async getUserInfo() {
        let self = this;

        const userSession = await Auth.currentSession();
        let token = userSession.idToken.jwtToken;
        console.log("aws token", token)

        let response = await getUserInfo(token);
        self.setState({ userData: response, isLoading: false })
    }

    async componentWillReceiveProps(props) {
        let self = this;
        self.setState({ isLoading: true })

       // self.getUserInfo();

    }

    async getProfileImg() {

        let self = this;

        const userSession = await Auth.currentSession();
        let token = userSession.idToken.jwtToken;
        console.log("aws token", token)

        let response = await getProfilePhoto(token);

        console.log("respomseeeeeeeeeeeee", response[0].path)


        //self.setState({userData:response,isLoading:false})
    }


    imageClicked=()=> {

        console.warn("picker")



        let options = {
            title: 'SwipeRed',
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
        };



        // ImagePicker.showImagePicker(options, (response) => {
        //     console.log('Response = ', response);


        //     if (response.didCancel) {
        //         console.log('User cancelled image picker');

        //     } else if (response.error) {
        //         console.log('ImagePicker Error: ', response.error);
        //     } else if (response.customButton) {
        //         console.log('User tapped custom button: ', response.customButton);
        //     } else {
        //         const source = { uri: response.uri };
        //         this.setState({
        //             avatarSource: source,
        //             uploadAvatar: response,
        //             //isLoading: true
        //         }, () => 
        //              //this.updateUserInfo()
        //              console.log("update profile")
        //         );
        //     }
        // });

    }

    async updateUserInfo() {

        console.log("avatarSource.uri", this.state.avatarSource.uri)

        let { avatarSource, uploadAvatar } = this.state;
        var token = await AsyncStorage.getItem('userToken');
        let response = await updateProfilePhoto(token, avatarSource.uri);

        if (response.uploadUrl !== '') {
            let uploadRes = await finalUpload(response.uploadUrl, avatarSource);
            console.log(uploadRes, "uploadRes")
            this.setState({ isLoading: false })

        }
        //  if (response.status === 201){
        //     this.setState({isLoading:false})
        //    alert("pjolee ")
        //  }
        //  this.setState({isLoading:false})
        //  console.log("photo res _profile",response.uploadUrl)
        //  let fg = await finalUpload(response.uploadUrl,uploadAvatar,token);
        //  console.log("finalllll",fg)
        // this.setState({isLoading:false})

    }





    // navigate to account profile

    account() {
        this.props.navigation.navigate("MyAccount", { userInfo: this.state.userData });
    }

    // navigate to notification

    notification() {
        alert("Under Development!")
    }


    reSyncMyAccount() {
        //alert("Under Development!")
    }

    // navigate to terms_privacy

    termAndPrivacy() {
        this.props.navigation.navigate("TermAndPrivacy");
    }

    // handle logout

    logout() {
        //alert("Logout")
        Alert.alert(
            'Log out',
            'Do you want to logout?',
            [
                { text: 'Cancel', },
                {
                    text: 'Confirm', onPress: () => {
                        AsyncStorage.clear();
                        Auth.signOut();
                        // this.props.navigation.navigate.reset('Login')
                        this.props
                            .navigation
                            .dispatch(StackActions.reset({
                                index: 0,
                                actions: [
                                    NavigationActions.navigate({
                                        routeName: 'Login',
                                    }),
                                ],
                            }))

                    }
                },
            ],
            { cancelable: false }
        );
    }

    // handleClick for each items

    handleClick = (type) => {
        switch (type) {
            case 'Account': this.account(); break;
            case 'Notifications': console.warn(type); break;
            case 'Resync my contacts': this.reSyncMyAccount(); break;
            case 'Terms & Privacy': this.termAndPrivacy(); break;
            case 'Logout': this.logout(); break;
            default: break;
        }
    }

    // line separotor for flatlist

    renderSeparator = () => {
        return (
            <View
                style={{
                    height: 0.5,
                    width: "100%",
                    alignSelf: 'center',
                    backgroundColor: "grey",
                }}
            />
        );
    };



    render() {

        let dimension = 40;
        let dimensionImage = 60;
        let { avatarSource, isLoading, userData } = this.state;

        return (

            <ScrollView style={{ flex: 1, backgroundColor: 'white' }} keyboardShouldPersistTaps={'handled'}>

                {/* header view container */}

                <LinearGradient colors={['#085d87', '#27c7bb']} style={{ flex: 1, }}>

                    <View style={styles.headerViewStyle}>
                        <Text style={styles.headerTittleStyle}>My Profile</Text>
                    </View>

                    {/* user profile card view container */}

                    <View style={{ flex: 1, backgroundColor: 'white' }}>

                        <CardView
                            cardElevation={2}
                            cardMaxElevation={2}
                            cornerRadius={10}
                            style={styles.cardView}>

                            <TouchableOpacity style={{ marginTop: 20, }} onPress={() => this.imageClicked()}>
                                <Image
                                    style={{ width: dimensionImage, height: dimensionImage, borderRadius: dimensionImage / 2, alignSelf: 'center', paddingBottom: 20, marginLeft: 0, }}
                                    source={avatarSource}
                                />
                            </TouchableOpacity>

                            <Text style={styles.userName}>Shyama Lahiri</Text>
                            <View style={{ height: 30, backgroundColor: '#fff', borderRadius: 10 }} />

                        </CardView>

                    </View>


                </LinearGradient>


                {/* main view conatiner */}

                <FlatList
                    data={listItems}
                    renderItem={({ item }) =>

                        <TouchableOpacity onPress={() => this.handleClick(item.itemName)}
                            style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between', backgroundColor: 'white' }}>

                            {/* left view */}
                            <View style={{ flexDirection: 'row', alignItems: 'center', padding: 10 }}>
                                <Image
                                    style={{ width: dimension, height: dimension, alignSelf: 'center', borderRadius: dimension / 2 }}
                                    source={item.itemImage}
                                />
                                <Text style={{
                                    textAlign: 'center', fontWeight: '500',
                                    paddingHorizontal: 10, fontSize: 15
                                }}>{item.itemName}</Text>

                            </View>

                            {/* right view */}

                            <View style={{ marginTop: 20 }}>
                                <Image source={backArrow} style={{ height: 15, width: 15, resizeMode: 'contain', right: 10 }} />

                            </View>

                        </TouchableOpacity>

                    } ItemSeparatorComponent={this.renderSeparator} />


                {isLoading && <Loader />}

            </ScrollView>


        );
    }
}

const styles = {
    cardView: {
        flex: 1,
        alignSelf: 'center',
        backgroundColor: '#FFFFFF',
        width: 140,
        height: 'auto',
        marginTop: Platform.OS === 'android' ? -60 : -40,
        marginBottom: 15,
    },
    headerViewStyle: {
        height: 150,
        paddingHorizontal: 20,
        paddingTop: height > 700 ? 50 : 30,
        //paddingTop:50,
    },
    userName: {
        flex: 1,
        alignSelf: 'center',
        paddingHorizontal: 1,
        paddingTop: 20,
        fontSize: 12,
    },
    headerTittleStyle: {
        fontSize: 24, color: 'white', fontWeight: '600', marginTop: 10
    }
}