import React from 'react';
import {
    TouchableOpacity,
    StyleSheet,
    Text,
} from 'react-native';
import {SCREEN_WIDTH} from '../styles/dimension';




const CustomButton = ({title,onpress}) => {


    return (
        <TouchableOpacity  style={styles.buttonView} onPress={onpress} underlayColor='#fff'>
            <Text style={styles.buttonTitleStyle}>{title}</Text>
        </TouchableOpacity>
    );

};



const styles = StyleSheet.create({
   
    buttonView: {
        width: SCREEN_WIDTH - 100,
        height: 50,
        paddingTop: 15,
        paddingBottom: 5,
        backgroundColor: '#3f50b5',
        borderRadius: 5,
        borderWidth: 1,
        borderColor: '#fff',
        top:10
    },
    buttonTitleStyle:{
        color:'#fff',textAlign: 'center', paddingLeft: 10, paddingRight: 10

    }

});

export default CustomButton;
