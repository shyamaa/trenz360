import React from 'react';
import { View, Text, ActivityIndicator, Modal,StyleSheet } from 'react-native';
//import Modal from "react-native-modal";



export default class Loader extends React.Component {

    static defaultProps = {
        title: "Loading",
    };


    render() {

        return (
            <Modal animationType={"slide"} transparent={true} visible={true} onRequestClose={() => { console.log("Modal has been closed.") }}>
                <View style={styles.modalView}>
                    
                     <ActivityIndicator size='large' color='#FFF' />
                     <Text style={{ marginTop: 12, fontSize: 18, fontWeight: 'bold', color: '#FFF' }}>{this.props.title}</Text>
               </View>
           </Modal>
        )
    }
};

const styles = StyleSheet.create({
    modalView: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'black',
        opacity: 0.11
    },
})




