import React from 'react';
import { View, Text, Dimensions, Image, ImageBackground, TouchableOpacity, FlatList, Linking } from 'react-native';
import CustomButton from './CustomButton'
import chatIcon from '../assets/buzz360Icon/chatIcon.png'
import phoneIcon from '../assets/buzz360Icon/phoneIcon.png'
import mobileIcon from '../assets/buzz360Icon/mobileIcon.png'
import mobilehands from '../imgAssests/mobile-with-hands.png'
import Communications from 'react-native-communications';
import { networkContactList,addNetworkContactList } from '../store/actions/BuzzAction';
import DefaultUserPic from '../imgAssests/DefaultUser.png'
import UserProfile from '../constants/UserProfile';
import Loader from './Loader';
import {getOr} from 'lodash/fp'



const { width, height } = Dimensions.get('window')
let userName = "Valentine";


const titleOfEmptyList = "Unlock this features by" + '\n' + "connecting your contacts"


class ContactCard extends React.Component {

    constructor(props) {
        super(props);
        console.log(UserProfile, "UserProfile")

        this.state = {
            isLoading: true,
            memberName: "MCCL MN",
            contactList:UserProfile,
            dupConList:UserProfile,
            networkId: props.networkId,
            isFetching: false,
            isSearch: false

        }
        //this.getNetworkContacts()
    }


    onConnect = () => {

        this.props.navigation.navigate("ContactsInfo", { data: 'Contacts',id :this.state.networkId })

    }
    //network contact list api's calling here

    async getNetworkContacts() {

        let list = await networkContactList(this.state.networkId);
        this.setState({ contactList: list, dupConList: list, isLoading: false, isFetching: false })
        console.warn("uuu")

    }

    async componentWillReceiveProps(props) {
        console.log("propsssssss", props.search)
        let list = await networkContactList(this.state.networkId);
        //this.setState({ contactList: list, dupConList: list, isLoading: false, isFetching: false })
        let isSearch = props.search.length > 0 ? this.setState({ isSearch: true }) : this.setState({ isSearch: false })
        let text = props.search.toLowerCase()
        let contactList = this.state.dupConList
        let filteredName = contactList.filter((item) => {
            console.warn("yyyyy")
            return item.firstName.toLowerCase().match(text) || item.lastName.toLowerCase().match(text)
        })
        if (!text || text === '') {

            console.warn('back')
            this.setState({
                contactList: this.state.dupConList
            })
        } else if (!Array.isArray(filteredName) && !filteredName.length) {
            // set no data flag to true so as to render flatlist conditionally
            this.setState({
                noData: true
            })
        } else if (Array.isArray(filteredName)) {

            this.setState({
                noData: false,
                contactList: filteredName
            })
        }
    }





    renderEmptyContactCard = () => {
        let { dupConList, isSearch } = this.state;

        let url = 'http://buzz360-admin.s3-website.ap-south-1.amazonaws.com/#/dashboard/network/1/advocates'
        return (
            isSearch === false ?
                <View style={{ alignItems: 'center', padding: 40, backgroundColor: '#FFF' }}>
                    <Image source={mobilehands} style={styles.imgStyle} />
                    <View style={{ top: 20, alignItems: 'center' }}>
                        <Text style={styles.emptyListTitleStyle}>{titleOfEmptyList}</Text>
                        <Text style={styles.emptyListTextStyle}>
                            For you to best support {this.state.memberName}, we{'\n'}will check your contacts to see who is{'\n'}a likely supporter. To do this, please give us your permission below
                            . Please see our privacy policy at
                      
                    <Text style={{ textDecorationLine: 'underline'}} onPress={()=>Linking.openURL(url).catch(err => console.error('An error occurred', err))}> buzz360.co</Text>
                 
                    </Text>
                    </View>

                    <View style={{ top: 50 }}>
                        <CustomButton title={'Connect'} onpress={() => this.onConnect()} />
                    </View>


                </View> :


                 <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>

                    <Text style={styles.serachNotFound}>
                        Search Not Found !
                    </Text>
                </View>




        )
    }

    onRefresh() {
       // this.setState({ isFetching: true }, function () { this.getNetworkContacts() });
    }




    render() {

        let dimension = 60;
        let { isLoading,contactList} = this.state
        console.log("contactList",contactList)
        return (
               <View style={{ backgroundColor: '#FFF', flex: 1 }}>

                   {this.state.contactList.length > 0  &&
                     <View style={{ top:10,flexDirection:'row-reverse',right:20}}>
                       <View style={{backgroundColor:'red',height:30,width:100,borderRadius:5,justifyContent:'center'}}>
                       <Text onPress={() => this.onConnect()} style={{color:'white',fontWeight:'800',textAlign:'center'}}> + Add User</Text>
                       </View>
                       
                    </View> }
                   {this.state.contactList.length > 0 ?

                    <FlatList
                        data={contactList}
                        onRefresh={() => this.onRefresh()}
                        refreshing={this.state.isFetching}
                        style={{top:30}}
                        renderItem={({ item }) =>

                          //onPress={()=>this.props.navigation.navigate('UserDetails')}

                            <TouchableOpacity style={{ alignItems: 'center', backgroundColor: '#FFF',marginTop:0 }} onPress={()=>this.props.navigation.navigate('UserDetails')} >

                              

                                <View style={styles.cardViewContainer}>

                                    {/* profile view */}

                                    <View style={{ flexDirection: 'row' }}>
                                        {/* <ImageBackground
                                            style={styles.profileImg}
                                            imageStyle={{ borderRadius: 75 / 2 }}
                                            resizeMode="cover"
                                            source={DefaultUserPic} /> */}
                                        <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                                            {item.hasThumbnail ? <Image
                                                style={{ width: dimension, height: dimension, alignSelf: 'center', borderRadius: dimension / 2 }}
                                                source={{ uri: item.thumbnailPath }}
                                            /> : <View style={{ height: dimension, width: dimension, borderRadius: dimension / 2, backgroundColor: "#ccc", justifyContent: 'center', alignItems: 'center' }}>
                                                    <Text style={{ fontSize: 18, fontWeight: '800', color: '#fff' }}>{item.firstName ? item.firstName.charAt(0) : ''}{item.lastName ? item.lastName.charAt(0) : ''}</Text></View>}
                                        </View>

                                        <View style={{ alignSelf: 'center', paddingHorizontal: 10, flexDirection: 'row' }}>
                                            <Text>{item.name}</Text>
                                            <Text style={{ left: 10 }}>{item.lastName}</Text>
                                        </View>
                                    </View>


                                    


                                    <View style={{ flexDirection: 'row' }}>
                                        <TouchableOpacity onPress={() => Communications.phonecall(item.phone, true)} >
                                            <Image source={phoneIcon} style={{ height: 20, width: 20, resizeMode: 'contain', top: 5, right: 20 }} />
                                        </TouchableOpacity>
                                        <TouchableOpacity onPress={() => Communications.text(item.phone)}>
                                            <Image source={chatIcon} style={{ height: 30, width: 30, resizeMode: 'contain' }} />
                                        </TouchableOpacity>
                                    </View>
                                </View>


                            </TouchableOpacity>
                       
                        
                       
                        } /> : this.renderEmptyContactCard()
                }

                



            </View>



        )
    }




}

const styles = {

    cardViewContainer: {
        flex: 1, flexDirection: 'row',
       // height: 85, 
        width: width - 20,
        padding: 10,
        alignItems: 'center',
        justifyContent: 'space-between',
        
    },
    badgeViewContainer: {
        height: 20, width: 20, borderRadius: 20, backgroundColor: 'red', alignItems: 'center',
        left: height > 1000 ? (width - 20) / 18 : (width - 20) / 10
    },
    badgeTextStyle: {
        fontSize: 15, color: 'white'
    },
    statusView: {
        height: 20, width: 20, borderRadius: 10,
    },
    profileImg: {
        height: 60, width: 60, borderRadius: 60 / 2,
        justifyContent: 'flex-start', flexDirection: 'row-reverse'
    },
    imgStyle: {
        height: 120, width: 120, resizeMode: 'contain'
    },
    emptyListTitleStyle: {

        color: '#505050', fontSize: 22, fontWeight: '800', textAlign: 'center'

    },
    emptyListTextStyle: {
        color: '#B3B3B3', fontSize: 15, fontWeight: '200', textAlign: 'center', top: 20
    },
    serachNotFound: {
        color: '#000', fontSize: 18, fontWeight: '500', textAlign: 'center', top: 20
    }
}
export default ContactCard;