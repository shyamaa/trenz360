import React, { Component } from 'react';
import {
    Text,
    View,
    TouchableOpacity,
    Dimensions,
    TextInput,
    Image,
    Alert,
    KeyboardAvoidingView,
    Platform,
    PermissionsAndroid,
    FlatList,
    Linking,
    AsyncStorage

} from 'react-native';
let deviceWidth = Dimensions.get('window').width;
import Contacts from 'react-native-contacts';
import CustomButton from '../../components/CustomButton';
import { isEmpty } from '../../utils/Validations';
import {createConnect,addNetworkContactList} from '../../store/actions/BuzzAction'
import Loader from '../../components/Loader';







const TAG = "RecentPost => \n";


export default class ContactsInfo extends Component {

    constructor(props) {
        super(props);
        console.log(props.navigation.state.params, "con info")
        this.state = {
            isLoading: false,
            showButton: false,
            email: "",
            password: "",
            contactsArray: [],
            contactsSavedArray: [],
            searchedText: "",
            filterArray: [],
            isSelect: false,
            selectId: '',
            contactId: [],
            contactList: [],
            isSelectAll: true,
            navigatePath: props.navigation.state.params.data,
            networkId:props.navigation.state.params.id
        }
    }

    backAction() {
        this.props.navigation.goBack()
    }

    componentDidMount() {

        if (Platform.OS === 'android') {
            console.log("componentDidMount Mounting... Andorid ");
            this.requestPermission();
        }
        if (Platform.OS === 'ios') {
            Contacts.getAll((err, contacts) => {
                if (err) {
                    throw err;
                }
                // console.log("Mounting...", contacts[0].familyName);
                console.log("Mounting...", contacts);
                // let contactsData = JSON.stringify(contacts);
                this.setState({
                    contactsArray: contacts,
                    contactsSavedArray: contacts
                });
            })
        }
    }






    // select al user in contact lists



    selectAll = () => {

        if (this.state.isSelectAll || this.state.contactId.length == 0) {
            this.state.contactsArray.map((data) => {
                console.warn("seleelel")
                this.state.contactId.push(data.recordID)
                this.state.contactList.push({ firstName: data.givenName, lastName: data.familyName })
                this.setState({ selectId: data.recordID, isSelectAll: false })
            })
        } else {
            this.state.contactsArray.map((data) => {
                console.warn("seleelel")
                //            this.state.contactId.pop(data.recordID)
               

                this.setState({ isSelectAll: true, contactId: [] ,contactList:[]})
            })
        }

    }


    // serach user in contact lists

    async onSearchTextChange(text) {
        await this.setState({ searchedText: text });
        let userSearched = this.state.searchedText;

        const storedContacts = this.state.contactsSavedArray;

        let filteredData = [];


        for (var i = 0; i < storedContacts.length; i++) {
            console.log(" loop ->" + storedContacts[i].givenName + " text " + text);

            if (storedContacts[i].givenName.startsWith(text) || storedContacts[i].familyName.startsWith(text)) {
                filteredData.push(storedContacts[i])
                console.log(" save " + text)
                let myArray = [...this.state.filterArray, ...filteredData];
                this.setState({ contactsArray: myArray })
            }
        }

    }
    async requestPermission() {
        try {
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.READ_CONTACTS,
            );
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                this.accessContacts();
                console.log('You can use the contacts');
            } else {
                console.log('Permission denied');
                this.permissionDeniedMessage();
            }
        } catch (err) {
            console.warn(err);
        }
    }

    accessContacts() {
        Contacts.getAll((err, contacts) => {
            if (err === 'denied') {
                console.log("read contacts permission denied")
                this.permissionDeniedMessage();
            } else {
                // contacts returned in Array
                console.log("Mounting... Andorid 1 ->", contacts);
                console.log("Mounting...Andorid 2 ->", JSON.stringify(contacts));
                // let contactsData = JSON.stringify(contacts);
                this.setState({
                    contactsArray: contacts,
                    contactsSavedArray: contacts
                });
            }
        })
    }

    permissionDeniedMessage() {
        //alert("Logout")
        Alert.alert(
            'Permission Denied ?',
            'If you want to access contacts. go to settings and allow contacts Permission',
            [
                { text: 'Cancel', },
                {
                    text: 'Go to setting', onPress: () => {
                        console.log("click go to setting")
                        Linking.openSettings(2)
                    }
                },
            ],
            { cancelable: false }
        );
    }

    // add user in contact lists

    addContacts = (item, index) => {
        console.log("itemsss", item.givenName, item.familyName, item.recordID, item.phoneNumbers)
        console.log("itemssgggggs",isEmpty(item.emailAddresses))
        let email = !isEmpty(item.emailAddresses) ? item.emailAddresses[0].email : ''
        if (this.state.contactId.includes(item.recordID)) {

            var i = this.state.contactId.indexOf(item.recordID);
            if (i != -1) {
                this.state.contactId.splice(i, 1);
                this.state.contactList.pop({ firstName: '', lastName: '' })
            }
            this.setState({ selectId: item.recordID })


        } else {
            this.state.contactId.push(item.recordID);
            this.setState({ selectId: item.recordID })
            this.state.contactList.push({ firstName: item.givenName, lastName: item.familyName,phone:item.phoneNumbers[0].number,email:email})
        }


    }


    // contact user card component

    renderItemContacts = ({ item, index }) => {

        //console.log(TAG + "renderItemContacts => " + JSON.stringify(item));
        let fName = item.givenName || false;
        let lName = item.familyName || false;
        let thumbNailName = (fName ? fName.charAt(0) : '') + (lName ? lName.charAt(0) : '');
        let dimension = 40;
        return (
            <View style={{
                flex: 1, flexDirection: 'row', width: '100%',
                paddingHorizontal: 20, paddingVertical: 10, top: 30, alignItems: 'center', borderBottomWidth: .5, borderBottomColor: '#ccc'
            }}>

                <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between', marginTop: 5 }}>
                    {/* first view */}
                    <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                        {item.hasThumbnail ? <Image
                            style={{ width: dimension, height: dimension, alignSelf: 'center', borderRadius: dimension / 2 }}
                            source={{ uri: item.thumbnailPath }}
                        /> : <View style={{ height: dimension, width: dimension, borderRadius: dimension / 2, backgroundColor: "#ccc", justifyContent: 'center', alignItems: 'center' }}><Text style={{ fontSize: 18, fontWeight: '800', color: '#fff' }}>{thumbNailName}</Text></View>}
                        <View>
                            <Text style={{
                                flex: 1,
                                paddingHorizontal: 10,
                                marginTop: 10,
                                fontSize: 16, fontWeight: '600', color: '#505050'
                            }}>{item.givenName + ' ' + item.familyName} </Text>
                        </View>
                    </View>

                    {/* 2nd view */}


                    <TouchableOpacity onPress={() => this.addContacts(item, index)} hitSlop={{ top: 20, bottom: 20, left: 50, right: 50 }}
                        style={[styles.addBtnView, { backgroundColor: this.state.contactId.includes(item.recordID) ? '#3f50b5' : 'white' }]}>
                        <Text style={{ color: this.state.contactId.includes(item.recordID) ? 'white' : 'black' }}>{this.state.contactId.includes(item.recordID) ? 'Added' : 'Add'}</Text>
                    </TouchableOpacity>

                </View>
            </View>
        )
    }



    render() {

        let { isSelect } = this.state;
        return (


            <View style={{ flex: 1, backgroundColor: '#FFFFFF' }}>

                {/* header view */}

                <View style={{ flexDirection: 'row', marginTop: 60, }}>



                    <TouchableOpacity style={[{ paddingHorizontal: 10, justifyContent: 'center' }]} onPress={() => { this.backAction() }}>
                        <Image
                            style={{ height: 20, width: 25, marginLeft: 10, }}
                            resizeMode="contain"
                            source={require('../../imgAssests/backArrowGrey.png')}>
                        </Image>
                    </TouchableOpacity>

                    <View style={styles.container}>
                        <Text style={styles.selectText}>Select friends you'd like to{'\n'}contact for MCCL MN:</Text>
                    </View>

                </View>

                <View style={{ paddingVertical: 20, }}>
                    <Text style={styles.textStyle}>We thought you may know some of these {'\n'}people, Please "add" the once you know.</Text>
                </View>
                <View style={{ paddingHorizontal: 6, paddingVertical: 10 }}>
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <View style={{ flex: 1, marginLeft: 10, height: 40 }}>
                            <TextInput
                                onChangeText={(text) => this.onSearchTextChange(text)}
                                placeholder="Search"
                                placeholderTextColor="gray"
                                style={{ flex: 1, borderWidth: .5, borderColor: '#ccc', borderRadius: 3, paddingVertical: 2, paddingHorizontal: 10, fontSize: 16 }}></TextInput>
                        </View>
                        <View style={{ paddingHorizontal: 5, height: 40 }}>
                            <TouchableOpacity style={styles.buttonSubmit} onPress={() => this.selectAll()}>
                                <Text style={{ color: '#fff', paddingVertical: 1 }}>SELECT ALL</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>


                {/* main view conatiner */}

                <FlatList
                    style={{ marginTop: 30, marginBottom: 10, height: 100 }}
                    data={this.state.contactsArray}
                    showsHorizontalScrollIndicator={false}
                    renderItem={this.renderItemContacts}
                />

                {/* bottom view container */}

                {this.state.contactId.length > 0 &&

                    <View style={{ alignItems: 'center', marginBottom: 30 }}>
                        <View style={{ height: 1, width: deviceWidth, backgroundColor: '#000' }} />
                        <CustomButton title={'Continue'} onpress={() => this.continue()} />

                    </View>}


                    {this.state.isLoading && <Loader/>}






            </View>


        );
    }

    async continue () {
        console.log(this.state.contactList, "litssssss",this.state.networkId)
        let { contactId, navigatePath,networkId } = this.state;
        this.setState({isLoading:true})

        let contactData;
        var token = await AsyncStorage.getItem('userToken');
        console.log(navigatePath,"pppppppp")

        if(navigatePath === "Home"){

            contactData  = await createConnect(token,this.state.contactList)
            console.log(contactData,"pppppppp")
           this.setState({isLoading:false})
           let isAdded = isEmpty(contactId) ? false : true
           this.props.navigation.navigate(navigatePath.toString(), { data: isAdded });

         

        }else{
            //alert("jjjjj")
            let isAdded = isEmpty(contactId) ? false : true
            contactData = await addNetworkContactList(token,networkId,this.state.contactList)
            console.log(contactData,"pppppppp")
            this.props.navigation.navigate(navigatePath.toString(), { data: isAdded });
            this.setState({isLoading:false})
            
        }
        
        
     }


}
const styles = {

    mainContainer: {
    },
    container: {
        flex: 0.85,
        alignSelf: 'center'

    },
    selectText: {
        fontSize: 18,
        color: '#2d397d',
        fontWeight: '800',
        textAlign: 'center',

    },
    textStyle: {
        fontSize: 15,
        color: '#5a6871',
        textAlign: 'center',
    },
    buttonSubmit: {
        paddingHorizontal: 15,
        paddingVertical: 10,
        borderRadius: 3, backgroundColor: '#3f50b5'
    },
    addBtnView: {
        padding: 10, borderWidth: 1, borderColor: '#ccc', borderRadius: 5
    }
}