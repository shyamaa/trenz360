import React, { Component } from 'react';
import {
    Text,
    View,
    TouchableOpacity,
    TextInput,
    Image,
    Platform,
    StyleSheet,
    Dimensions
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import ScrollableTabView, { ScrollableTabBar } from 'react-native-scrollable-tab-view';
import ContactCard from '../../components/ContactCard';
import search from '../../imgAssests/search.png';
import UserProfile from '../../constants/UserProfile';
import axios from 'axios'
import { networkList } from '../../store/actions/BuzzAction';
import Amplify, { Auth } from 'aws-amplify';
import Loader from '../../components/Loader';

const paddingValue = Platform.isPad ? true : false;
const {width,height} = Dimensions.get('window')


export default class ContactTabs extends Component {

   
    constructor(props) {
        super(props);
        console.log("props of contact",props)
        this.state = {
            isLoading: false,   //true
            users:UserProfile,    //UserProfile
            dupUser:UserProfile,  //UserProfile
            noDate: true,
            membersArray:[{'name':'Bharatiya Janata Party'},{'name':'Indian National Congress'},{'name':'Aam Aadmi Party'},{'name':'YSR Congress Party'}],
            //:[{'name':'MCCL MN'},{'name':'Jason Lewis'}],
            searchtext:''
        }
    }

    componentWillReceiveProps(props){
        console.log("props of contact will",props.navigation.state.params.data)
        props.navigation.state.params.data ? this.setState({users:UserProfile,dupUser:UserProfile}) : null
    }

    async componentDidMount() {

        this.getNetwokList()
    }

    // network lists api's calling

   async getNetwokList(){
    const userSession =  await Auth.currentSession();
    let token = userSession.idToken.jwtToken;
    let list = await networkList(token);
    this.setState({membersArray:list,isLoading:false})
  }

    




    // handle seaching 

    searchText = (e) => {
        console.warn(e)
         this.setState({searchtext:e})
    }

    renderNetworkCard=()=>{
        return(
        this.state.membersArray.map((data)=>{
            console.warn(data)
            return(
                <ContactCard tabLabel={data.name} 
                userLists={this.state.users}  
                search={this.state.searchtext}
                navigation={this.props.navigation} networkId={data.networkId} />
            )
        })
      ) 
    
     
    }


    render() {
        let {isLoading} = this.state;
        return (
            <View style={{ flex: 1,backgroundColor:'#FFF' }}>
                {/* header view */}

                <LinearGradient colors={['#085d87', '#27c7bb']} style={{ height:height * 0.25 }}>
                    <View style={styles.headerView}>
                        <View style={styles.headerTitleView}>
                            <Text style={styles.headerTitleStyles}>Contacts</Text>
                        </View>
                        <View style={styles.inputContainer}>

                            <TextInput
                                style={{ flex: 1, fontSize:14, marginHorizontal:15,fontWeight:'200'}}
                                autoCapitalize='none'
                                placeholder="Search Contacts"
                                placeholderTextColor="gray"
                                underlineColorAndroid="transparent"
                                onChangeText={this.searchText.bind(this)}
                            />
                            <TouchableOpacity style={styles.searchView}>
                                <Image source={search} style={styles.searchIconStyle} />
                            </TouchableOpacity>


                        </View>
                    </View>

                </LinearGradient>

                {/* main view container */}

                <ScrollableTabView
                    initialPage={0}
                    tabBarInactiveTextColor={'#5D5D5D'}
                    tabBarUnderlineStyle={{ backgroundColor: '#BB281C', }}
                    tabBarActiveTextColor={'#BB281C'}
                    renderTabBar={() => <ScrollableTabBar />}>

                {this.renderNetworkCard()}
                

                </ScrollableTabView>

                {isLoading && <Loader/>}


            </View>
        );
    }
}

const styles = StyleSheet.create({

    headerView: {
        paddingHorizontal: paddingValue ? 120 : 20, 
        paddingTop:height >700 ? 50 :30, 
       // paddingTop:50, 
    },
    headerTitleView: {
        paddingVertical: 5, marginBottom: Platform.OS === 'android' ? 10 : 20
    },
    headerTitleStyles: {
        fontSize: 26, color: 'white', fontWeight: '600'
    },
    inputContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
        height: 50,
        borderRadius: 5,

    },
    searchView: {
       paddingLeft: 20, 
        paddingTop: 5,
    },
    searchIconStyle: {
        right:10,
        height: 20,
        width: 20,
        bottom:2,
        resizeMode: 'stretch',
        alignItems: 'center'
    },

});
