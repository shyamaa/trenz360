export function validEmail(emailText){
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/ ;
    if(reg.test(emailText) === false)
    {
        console.log("Email is Not Correct");
        return true;
    }
    else {
        console.log("Email is Correct");
        return false;
    }
}

export function validPassword(password){
    if (password.length >7){
        return false;
    } else {
        return true;
    }
}
export function validPasswordFormat(password){

    var reg = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!"#$%&'()*+,-./:;<=>?@[\]^_`{|}~]).{8,}/
    if (reg.test(password) === false){
        console.warn("password is Not Correct");
        return true;
    }else{
        console.warn("password is Correct");
        return false;
    }
    

}


export function validName(username){
    if (username.length >0){
        return false;
    } else {
        return true;
    }
}

export function validPhoneNumber(phonenumber){
    if (phonenumber.length >9){
        return false;
    } else {
        return true;
    }

}


export function loginValidPassword(password) {
    if (password)
        return false;

    if (password.length > 0){
        return false;
    } else {
        return true;
    }
}


export function validConfirmPassword(password,confirmPassword){
    if (password === confirmPassword){
        return false;
    } else {
        return true;
    }
}



export function validNewPassword(oldPassword,newPassword){
    if (oldPassword === newPassword){
        return true;
    } else {
        return false;
    }
}

export function checkDigit(username) {
    if (username.match(/\d/)) {
        return true;
        //console.log("Digit Found")
    } else {
        return false;
    }
}



export function isEmpty(text){
    return text.length < 1;

}
