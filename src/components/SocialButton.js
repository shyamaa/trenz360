import React from 'react'
import { View, Text,Image,Dimensions,TouchableOpacity } from 'react-native';


const {width,height} = Dimensions.get('window')

const SocialButton = ({ title,titleColor,bgColor,setTitle,onpress}) => {
   
    console.log(title,titleColor,bgColor,setTitle)
    return (
        
        <TouchableOpacity onPress={onpress}
        style={[styles.container,{backgroundColor:bgColor}]}>
             <View style={{justifyContent:'center'}}>
                <Text style={{color:titleColor,fontWeight:'500',fontSize:18}}>{title}</Text>
             </View>
             {title == setTitle &&
              <View style={{backgroundColor:'#FFF',height:25,width:25,borderRadius:25,justifyContent:'center',alignSelf:'center'}}>
               <Text style={{textAlign:'center'}}>✓</Text>
           </View> }
             
            
        </TouchableOpacity>
    )
}

const styles = {
    
    container: {
        width: width-70, height: 50,
        borderRadius: 10, backgroundColor: 'yellow',
        flexDirection:'row', 
        justifyContent:'space-between',
        paddingHorizontal:20
    },
    imgStyle:{
       height:30,width:30,resizeMode:'contain'
    }
}

export default SocialButton;