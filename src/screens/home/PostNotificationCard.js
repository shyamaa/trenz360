import React from 'react';
import { View, Text, Dimensions,ImageBackground, TouchableOpacity, FlatList, StyleSheet } from 'react-native';
import picture1 from '../../imgAssests/picture1.png';
import {networkMessageList } from '../../store/actions/BuzzAction';




class PostNotificationCard extends React.Component {

    constructor(props) {
        super(props);
        console.warn("info card",props.networkId)
        this.state = {
            cards: [1],
            networkId:props.networkId,
            postList:[]
        }
        //this.getPostList()
    }

    // api's calling for network post lists

    async getPostList(){
       
        let { networkId } = this.state
        let self = this;
        let reposneData = await networkMessageList(networkId);
        console.log("reposneData daatttaaa",reposneData)
        self.setState({messageList:reposneData})
                reposneData.map((data, index) => {
                    reposneData[index].isPost == true ? 
                    self.state.postList.push(reposneData[index]) :  null
                })
    }

    // calling getpostlist function

    async componentDidMount(){

       this.getPostList()

    }



     //  SeparteLine for flatlist   

    renderSeparator = () => {
        return (
            <View style={styles.separteLine} />
        );
    };

    // rendering jsx 

   

    render() {
        console.log("render response",this.state.postList)
        let lastOpen = new Date().getTime();
        return (
            <View style={{flex:1}}>
            
             <FlatList
                    data={this.state.postList}
                    renderItem={({ item,index }) =>

                        <TouchableOpacity style={{ padding: 15,flexDirection:'row',justifyContent:'space-around' }} onPress={()=>this.props.onpress(index,item)}>

                           
                            <ImageBackground source={picture1} 
                             imageStyle={{borderRadius:10}}  style={{height:70,width:100}}>
                             {
                                 new Date(item.expiredAt).getTime() < lastOpen ? 

                                 <View style={[styles.msgTextStatusView,{backgroundColor : 'blue'}]}>
                                   <Text style={{ color: '#FFF', fontSize: 10, textAlign: 'center' }}>
                                     Expired
                                  </Text>
                                </View> :
                                 <View style={[styles.msgTextStatusView,{backgroundColor : 'red'}]}>
                                   <Text style={{ color: '#FFF', fontSize: 10, textAlign: 'center' }}>
                                    NEW
                                   </Text>
                                 </View>
                            } 
                            </ImageBackground>

                             <View style={{left:10,padding:5 }}>

                                <View style={styles.postView}>
                                    <Text style={styles.postTitle}>{item.title}</Text>
                                </View>
                                <View style={{ alignSelf: 'center',width:250,top:5,}}>
                                    <Text style={{fontSize:12}}>{item.message}</Text>
                                </View>
                               
                            </View>

                        </TouchableOpacity>
                    }
                    ItemSeparatorComponent={this.renderSeparator} />
             </View>


        )

    }
}

const styles = StyleSheet.create({
    
    separteLine:{
        height: 0.5, width: "95%",alignSelf: 'center',backgroundColor: "grey",
    },
    postTitle:{
        flexWrap:'wrap',fontWeight:'800',fontSize:12
    },
    postView:{
        alignSelf: 'center',width:250
    },
    msgTextStatusView: {
        borderRadius: 2, alignSelf: 'flex-start', padding: 4
    },

    

})
export default PostNotificationCard;