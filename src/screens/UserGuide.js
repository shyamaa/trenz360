import React, { Component } from 'react';
import {
    AppRegistry,
    Text,
    View,
    TouchableOpacity,
    ImageBackground,
    Dimensions,
    TextInput,
    Image,
    Alert,
    ActivityIndicator,
    ScrollView,
    KeyboardAvoidingView,
    Platform,Button,
    TouchableHighlight
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';


let deviceWidth = Dimensions.get('window').width;
let deviceHeight = Dimensions.get('window').height;
const paddingValue = Platform.isPad ? true : false;
const TAG = "UserGuide Screen => \n";

export default class UserGuide extends Component {

    constructor() {
        super();
        this.state = {
            isLoading: false,
            showButton: false,
            email: "",
            password: "",
        }
    }

    componentDidMount() {
    }

    loginClicked() {

     alert("Hit API")

    }

    signupClicked() {

    }


//backgroundColor:'#8e2829'

    render() {
        let { width } =  Dimensions.get('window');


        return (
            <KeyboardAvoidingView
                style={{flex:1, justifyContent:'center'}}
                behavior={Platform.OS === 'ios' ? 'padding' : null}>



                <View
                    style={{ flex: 1,backgroundColor: 'rgba(52, 52, 52, 0.8)', }} keyboardShouldPersistTaps={'handled'}>

                        <View style={{  paddingHorizontal: paddingValue ? 120 : 20, paddingVertical: 50}}>
                            <View style={{flex:0, alignItems:'center', justifyContent:'flex-start'}}>
                                <View style={{paddingVertical: 5 ,marginBottom:20}}>
                                    <Text style={{fontSize:26, color:'white', fontWeight: '600'}}>Welcome!</Text>
                                </View>
                            </View>

                         <View style={{paddingVertical: 5 ,marginBottom:20,flex:0, alignItems:'center', justifyContent:'flex-start'}}>

                                    <Text style={{fontSize:16, color:'white', fontWeight: '600'}}>Chekc the top right corner for any new posts or message to share, and start advocating for your group!!</Text>
                            </View>


                            <TouchableOpacity
                                style={{
                                    marginRight:40,
                                    marginLeft:30,
                                    marginTop:40,
                                    height:50,
                                    paddingTop:15,
                                    paddingBottom:5,
                                    backgroundColor:'#8e2829',
                                    borderRadius:5,
                                    borderWidth: 1,
                                    borderColor: '#fff'}}
                                onPress={ () => this.loginClicked()}
                                underlayColor='#fff'>
                                <Text style={{ color:'#fff',
                                    textAlign:'center',
                                    paddingLeft : 10,
                                    paddingRight : 10}}>Got IT</Text>
                            </TouchableOpacity>

                        </View>
                    </View>

            </KeyboardAvoidingView>
        );
    }
}
