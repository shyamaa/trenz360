import React, { Component } from 'react';
import {
    Text,
    View,
    TouchableOpacity,
    Dimensions,
    Image,
    ScrollView,
    KeyboardAvoidingView,
    Platform,
    StyleSheet,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { TextField } from 'react-native-material-textfield';
import { EMAIL_VALIDATION_MESSAGE } from '../../actions/StringConstants';

import { showInfoAlert } from "../../utils/AlertPopUp";
import { validEmail, isEmpty } from "../../utils/Validations";
import logo from '../../imgAssests/logo.png';
import loginarrow from '../../imgAssests/login-arrow.png'
import { Auth } from 'aws-amplify';


let deviceWidth = Dimensions.get('window').width;
let deviceHeight = Dimensions.get('window').height;




const TAG = "Forgot Screen => \n";

export default class Forgot extends Component {

    constructor() {
        super();
        this.state = {
            isLoading: false,
            showButton: false,
            email: "",
        }
    }


    // email change event


    onEmailTextChange(text) {
        this.setState({ email: text });
        let { email } = this.state;
        if (email.length > 0) {
            this.setState({ showButton: true });
        } else {
            this.setState({ showButton: false });
        }
    }

    // handle forgot password functionality

    forgotClicked() {
        let { email } = this.state;
        let self = this

        if (validEmail(email)) {
            showInfoAlert(EMAIL_VALIDATION_MESSAGE)
            return;
        }

        else {

            Auth.forgotPassword(email)
                .then(data => {
                    console.log(data),
                    // self.props.navigation.navigate("ResetPassword",{data:email}),
                     self.props.navigation.navigate("ResetPassword",{data:email})
                })
                .catch(err => 
                    showInfoAlert("This email is not found!")
            );
            //this.props.navigation.navigate("ResetLink")

        }
    }

    // navigate to login screen 

    loginClicked() {
        this.props.navigation.navigate("Login");
    }

    // return jsx

    render() {
        return (




            <LinearGradient colors={['#085d87', '#27c7bb']} style={{ flex: 1 }}>

                <ScrollView keyboardShouldPersistTaps={'handled'}>

                    {/*header view */}

                    <View style={{ marginTop: deviceHeight > 700 ? 65 : 35, alignItems: 'center', }}>
                        <Image
                            style={{ height: 75, width: 300 }}
                            resizeMode="contain"
                            source={logo} />
                    </View>

                    {/*main view container */}



                    <View style={{ flex: 1, alignItems: 'center', height: 'auto', marginTop: deviceHeight > 700 ? 75 : 50 }}>


                        <View style={{
                            width: deviceWidth - 50, padding: 10, borderRadius: 10,
                            backgroundColor: 'white', alignItems: 'center', marginTop: '10%'
                        }}>

                            <Text style={{ fontSize: 26, color: '#3f50b5', fontWeight: '800', marginTop: 20 }}>Forgot Password</Text>

                            <Text style={styles.textReset}>Please enter your email address and we will send a link to reset.</Text>


                            <TextField
                                onChangeText={(text) => this.onEmailTextChange(text)}
                                underlineColorAndroid='transparent'
                                autoCapitalize='none'
                                label="Email Address"
                                value={this.state.email}
                                placeholderTextColor="#D8D9DB"
                                baseColor={'grey'}
                                tintColor={'grey'}
                                containerStyle={styles.textInput}

                            />




                            {/*button view container*/}

                            <View style={{
                                marginTop: 30, backgroundColor: '#fff', width: deviceWidth - 50,
                                alignItems: 'center', borderRadius: 10
                            }}>

                                {
                                    this.state.showButton === true ?
                                        <TouchableOpacity
                                            style={styles.buttonView}
                                            onPress={() => this.forgotClicked()}
                                            underlayColor='#fff'>
                                            <Text style={{
                                                color: '#fff',
                                                textAlign: 'center',
                                                paddingLeft: 10,
                                                paddingRight: 10
                                            }}>Submit</Text>
                                        </TouchableOpacity>
                                        : <View style={{
                                            paddingTop: 15,
                                            height: 50,
                                        }} />
                                }

                                <View style={{ width: deviceWidth - 50, height: 10, borderRadius: 30 }}></View>


                            </View>


                        </View>


                    </View>

                    <View style={{ flex: 1, justifyContent: 'flex-end', marginTop: deviceHeight > 700 ? deviceHeight / 3.5 : deviceHeight / 4 }}>
                        <TouchableOpacity style={{ marginBottom: 10, alignSelf: 'center', flexDirection: 'row' }} onPress={() => this.loginClicked()}>
                            <Text style={{ fontSize: 15, color: '#fff', fontWeight: '800' }}>Login</Text>
                            <Image source={loginarrow} style={{ height: 20, width: 20, resizeMode: 'contain', left: 10, top: 2 }} />
                        </TouchableOpacity>
                    </View>

                </ScrollView>


            </LinearGradient>




        )
    }
}

const styles = StyleSheet.create({

    textInput: {
        height: 45,
        marginTop: 10,
        //fontSize: 16,
        backgroundColor: 'rgba(255, 255, 255, 0.6)',
        borderBottomColor: 'gray',
        width: deviceWidth - 80,
        //color: 'gray'
    },
    buttonView: {
        width: deviceWidth - 100,
        height: 50,
        paddingTop: 15,
        backgroundColor: '#3f50b5',
        borderRadius: 5,
        borderWidth: 1,
        borderColor: '#fff',
    },
    textReset: {
        marginTop: 20,
        textAlign: 'center',
        justifyContent: 'center',
        fontSize: 15, color: '#3f50b5',
        fontWeight: '200',
    },

})
