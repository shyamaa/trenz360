import React, { Component } from 'react';
import {
    Text,
    View,
    TouchableOpacity,
    Dimensions,
    Image,
    ScrollView,
    KeyboardAvoidingView,
    Platform, Button,
    StyleSheet,
    BackHandler, Modal, ActivityIndicator
} from 'react-native';

import LinearGradient from 'react-native-linear-gradient';
import { TextField } from 'react-native-material-textfield';

import logo from '../../imgAssests/logo.png';
import loginarrow from '../../imgAssests/login-arrow.png';
import { EMAIL_EMPTY_MESSAGE, EMAIL_VALIDATION_MESSAGE, PASSWORD_EMPTY_MESSAGE, PASSWORD_VALIDATION_MESSAGE } from '../../actions/StringConstants';
import { showInfoAlert } from "../../utils/AlertPopUp";
import { validEmail, validPassword, isEmpty } from "../../utils/Validations";
import { onUserLogin } from '../../store/actions/AuthAction'
import Amplify, { Auth } from 'aws-amplify';
import config from '../../config/aws-exports'
Amplify.configure(config);


let deviceWidth = Dimensions.get('window').width;
let deviceHeight = Dimensions.get('window').height;



const paddingValue = Platform.isPad ? true : false;
import { LoginManager, AccessToken, GraphRequest, GraphRequestManager } from "react-native-fbsdk";

import { getFBAccessToken, saveFBAccessToken, saveGlobalConfigurations, saveLoginDetails } from '../../utils/SaveDetails';
import Loader from '../../components/Loader';
import OverlayLabel from '../../components/OverlayLabel';

const TAG = "SignIn Screen => \n";

export default class Login extends Component {


    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            showButton: false,
            email: "",
            password: "",
            loginDisable: false
        }
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    }

    UNSAFE_componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    handleBackButtonClick() {
        //this.props.navigation.goBack(null);
        BackHandler.exitApp();
        return true;
    }


    async onEmailTextChange(text) {
        let { email, password } = this.state;
        await this.setState({ email: text });

        if (!isEmpty(email) && !isEmpty(password)) {
            this.setState({ showButton: true });
        } else {
            this.setState({ showButton: false });
        }

    }

    async onPasswordTextChange(text) {
        let { email, password } = this.state;

        await this.setState({ password: text });
        if (!isEmpty(email) && !isEmpty(password)) {
            this.setState({ showButton: true });
        } else {
            this.setState({ showButton: false });
        }

    }

    facebookLogin() {
        //showInfoAlert("Under Development!")

        this.setState({ isLoading: true });

        if (Platform.OS === 'android') {
            try {
                LoginManager.setLoginBehavior('WEB_ONLY');
            } catch (error) {
                console.log("facebook android Error")
            }
        }

        LoginManager.logInWithPermissions(["public_profile", "email"]).then((result) => {
            if (result) {
                if (result.isCancelled) {
                    this.setState({ isLoading: false });
                    console.log("Login cancelled");
                } else {
                    AccessToken.getCurrentAccessToken().then((data) => {
                        let accessToken = data.accessToken;
                        if (accessToken)
                            saveFBAccessToken(accessToken)
                        else
                            accessToken = getFBAccessToken();
                        const responseInfoCallback = (error, result) => {
                            this.setState({ isLoading: false });
                            if (error) {
                                console.log(TAG + error)
                            } else {
                                // alert("Hit Fb" + result.id + " accessToken -> " + accessToken)
                                console.log(result, "fb deatils")
                                let obj = {
                                    firstName: result.first_name,
                                    lastName: result.last_name,
                                    email: result.email,
                                    id: result.id,
                                }
                                let user = {
                                    username: result.first_name,
                                    email: result.email,

                                }
                                Auth.federatedSignIn(

                                    {
                                        accessToken,
                                        expires_at: 209889 * 1000 + new Date().getTime() // the expiration timestamp
                                    },
                                    user
                                ).then(cred => {
                                    // If success, you will get the AWS credentials
                                    console.log("cred", cred);
                                    return Auth.currentAuthenticatedUser();
                                }).then(user => {
                                    // If success, the user object you passed in Auth.federatedSignIn
                                    console.log(user);
                                }).catch(e => {
                                    console.log(e)
                                });
                                // this.props.navigation.navigate("SignupStarted", { data: obj })

                                //this.executeHealthNickelLogin(result.id, accessToken, 'FACEBOOK');
                            }
                        }
                        const infoRequest = new GraphRequest(
                            '/me',
                            {
                                accessToken: accessToken,
                                parameters: {
                                    fields: {
                                        string: 'email,name,first_name,middle_name,last_name,picture.type(large)'
                                    }
                                }
                            },
                            responseInfoCallback,

                        );

                        // Start the graph request.
                        new GraphRequestManager().addRequest(infoRequest).start();
                    })

                }
            } else {
                this.setState({ isLoading: false });
            }
        });
    }

    isValidation = () => {

        let {email,password} = this.state;



        if (email.length <= 0) {
            showInfoAlert(EMAIL_EMPTY_MESSAGE)
            return false;
        }

        if (validEmail(email)) {
            showInfoAlert(EMAIL_VALIDATION_MESSAGE)
            return false;
        }

        if (password.length <= 0) {
            showInfoAlert(PASSWORD_EMPTY_MESSAGE)
            return false;
        }


        if (validPassword(password)) {
            showInfoAlert(PASSWORD_VALIDATION_MESSAGE)
            return false;
        }

        return true


    }


    async loginClicked() {

        let self = this;
        let { email, password } = this.state



        self.setState({ isLoading: true })




        if (this.isValidation()) {

            console.log("validateee")
            this.props.navigation.navigate("tabs");

            // let loginSession = await onUserLogin(email, password);
            // console.log(loginSession, "sessionnn")
            // if (loginSession !== undefined) {
            //     self.setState({ isLoading: false,password:'' })
            //     this.props.navigation.navigate("tabs");
            // } else {
            //     self.setState({ isLoading: false })
            // }
        } else {
            self.setState({ isLoading: false })
        }
    }

    signupClicked() {

        this.props.navigation.navigate("Signup", { "userid": "yasaswigera@gmail.com" });
    }

    forgotAction() {
        this.props.navigation.navigate("Forgot");
    }

    render() {

        let { email, password, isLoading } = this.state

        return (



            <LinearGradient colors={['#085d87', '#27c7bb']} style={{ flex: 1 }}>



                <ScrollView style={{ flex: 1 }} keyboardShouldPersistTaps="handled" >


                    <View style={{ flex: 1, }}>

                        {/*header view*/}

                        <View style={{ marginTop: deviceHeight > 700 ? 70 : 50, alignItems: 'center' }}>
                            <Image
                                style={{ height: 75, width: 300 }}
                                resizeMode="contain"
                                source={logo} />
                        </View>

                        {/*main view container */}

                        <KeyboardAvoidingView behavior={Platform.OS === "ios" ? "position" : null}
                            keyboardVerticalOffset={Platform.OS === "ios" ? -60 : 0}>

                            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'space-around', height: 'auto', marginBottom: 100 }}>


                                <View style={{
                                    width: deviceWidth - 50, padding: 10, borderRadius: 10,
                                    backgroundColor: 'white', alignItems: 'center', marginTop: '20%'
                                }}>

                                    <Text style={{ fontSize: 26, color: '#3f50b5', fontWeight: '800', marginTop: 20 }}>Login</Text>



                                    <TextField
                                        ref={input => { this.textInput = input }}
                                        onChangeText={(text) => this.onEmailTextChange(text)}
                                        underlineColorAndroid='transparent'
                                        autoCapitalize='none'
                                        label="Email Address"
                                        value={email}
                                        placeholderTextColor="#D8D9DB"
                                        baseColor={'grey'}
                                        tintColor={'grey'}
                                        containerStyle={styles.textInput}

                                    />

                                    <View style={{ flexDirection: 'row' }}>

                                        <TextField
                                            ref={input => { this.textInput = input }}
                                            onChangeText={(text) => this.onPasswordTextChange(text)}
                                            underlineColorAndroid='transparent'
                                            autoCapitalize='none'
                                            label="Password"
                                            value={password}
                                            secureTextEntry={true}
                                            placeholderTextColor="#D8D9DB"
                                            baseColor={'grey'}
                                            tintColor={'grey'}
                                            containerStyle={[styles.textInput, { width: deviceWidth - 80, right: 0 }]}

                                        />



                                    </View>

                                    <TouchableOpacity onPress={() => this.forgotAction()} style={{ left: deviceHeight > 700 ? deviceWidth - 250 : deviceWidth - 270, bottom: 5 }}>
                                        <Text style={{ color: 'blue', fontWeight: '200', fontSize: 12 }}>Forgot?</Text>
                                    </TouchableOpacity>

                                    {/*button view container*/}

                                    <View style={{
                                        marginTop: 20, backgroundColor: '#fff', width: deviceWidth - 50,
                                        alignItems: 'center', borderRadius: 10
                                    }}>

                                        {
                                            this.state.showButton === true ?
                                                <TouchableOpacity
                                                    style={[styles.buttonFacebook, { backgroundColor: this.state.loginDisable ? 'grey' : '#3f50b5' }]}
                                                    disabled={this.state.loginDisable}
                                                    onPress={() => this.loginClicked()}
                                                    underlayColor='#fff'>
                                                    <Text style={{
                                                        color: '#fff',
                                                        textAlign: 'center',
                                                        paddingLeft: 10,
                                                        paddingRight: 10
                                                    }}>Login</Text>
                                                </TouchableOpacity>
                                                : <View style={{
                                                    paddingTop: 15,
                                                    height: 50,
                                                }} />
                                        }

                                        <View style={{ width: deviceWidth - 50, height: 10, borderRadius: 30 }}></View>


                                    </View>

                                    <TouchableOpacity
                                        style={[styles.buttonFacebook, { marginTop: 15 }]}
                                        onPress={() => this.facebookLogin()}
                                        underlayColor='#fff'>
                                        <Text style={{
                                            color: '#fff',
                                            textAlign: 'center',
                                            paddingLeft: 10,
                                            paddingRight: 10
                                        }}>Login With Facebook</Text>

                                    </TouchableOpacity>


                                </View>



                            </View>

                        </KeyboardAvoidingView>

                    </View>

                    <TouchableOpacity style={{ marginBottom: deviceHeight > 700 ? 0 : 150, alignSelf: 'center', flexDirection: 'row', marginTop: deviceHeight > 700 ? deviceHeight / 10 : -20 }} onPress={() => this.signupClicked()}>
                        <Text style={{ fontSize: 15, color: '#fff', fontWeight: '800' }}>Sign Up</Text>
                        <Image source={loginarrow} style={{ height: 20, width: 20, resizeMode: 'contain', left: 10, top: 2 }} />
                    </TouchableOpacity>

                </ScrollView>

                {/* modal loader */}

                {isLoading && <Loader />}



            </LinearGradient>









        )
    }
}

const styles = StyleSheet.create({

    textInput: {
        height: 45,
        marginTop: 20,
        backgroundColor: 'rgba(255, 255, 255, 0.6)',
        borderBottomColor: 'gray',
        width: deviceWidth - 80,
        //color: 'gray'
    },
    buttonFacebook: {
        width: deviceWidth - 100,
        height: 50,
        paddingTop: 15,
        backgroundColor: '#3f50b5',
        // paddingBottom: 5,

        borderRadius: 5,
        borderWidth: 1,
        borderColor: '#fff',
    },
    textReset: {
        marginTop: 20,
        textAlign: 'center',
        justifyContent: 'center',
        fontSize: 15, color: '#3f50b5',
        fontWeight: '200',
    },
    inputContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        height: 45,
        width: deviceWidth - 80,
        // borderBottomWidth: 1,
        // borderBottomStartRadius: 10,
        borderBottomColor: 'gray',
        marginTop: 20
    },
    modalView: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'black',
        opacity: 0.9
    },

})