import React, { Component } from 'react';
import { Text, View, TouchableOpacity, Dimensions, Image, Platform, FlatList, Linking } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import ScrollableTabView, { ScrollableTabBar } from 'react-native-scrollable-tab-view';
import backArrow from '../../imgAssests/backArrow.png';
import Modal from "react-native-modalbox";
import Swiper from 'react-native-deck-swiper';
import catImage from '../../imgAssests/ctaImage.png';
import cross from '../../imgAssests/cross.png';
import rightArrow from '../../imgAssests/right-arrow-round-red.png'


const paddingValue = Platform.isPad ? true : false;
import OverlayLabel from '../../components/OverlayLabel';
import MessageInfoCard from '../../components/MessageInfoCard';
import axios from 'axios';
import { getOr } from 'lodash/fp'
import { isEmpty } from '../../utils/Validations';
import Communications from 'react-native-communications';
import Amplify, { Auth } from 'aws-amplify';
import {getContacts} from '../../store/actions/BuzzAction'
import DefaultUserPic from '../../imgAssests/DefaultUser.png'




const { width, height } = Dimensions.get('window')


const TAG = "RecentPost => \n";

const ContactMethods = [
    {
        "key": "Text",
        "type": 'text',
        "value": "9654267338",
    },
    {
        "key": "Facebook",
        "type": 'facebook',
        "value": "@steve.smith",
    },
    {
        "key": "Email",
        "type": 'email',
        "value": "support@swipered.com",
    },
    {
        "key": "Phone",
        "type": 'phone',
        "value": "18002703311",
    }


];


export default class MessageInfoList extends Component {


    constructor(props) {
        super(props);
        console.log("propssssssss", props.navigation.state.params.membersArray)
        this.state = {
            isLoading: false,
            cards: [1],
            swiperVisiable: false,
            networkId: 0,
            messageList: [],
            membersArray: getOr([], 'membersArray', props.navigation.state.params),
            activeMembersArray:[],
            firstName:''
        }
       // this.getContact();
    }

    renderNetworkCard = () => {
        return (
            this.state.membersArray.map((data) => {
                return (
                    <MessageInfoCard tabLabel={data.name.substring(0, 10) + "..."} onpress={this.childHandler.bind(this)} messageList={this.state.messageList} id={data.networkId} />

                )
            })
        )
    }

     // get contacts api's calling

     async getContact(){
        const userSession =  await Auth.currentSession();
        let token = userSession.idToken.jwtToken;
        console.log("token",token)
        let list = await getContacts(token);
        console.log("contact response",list)
        this.setState({activeMembersArray:list})

    }





    openSwiper(swipeVisiable, networkId) {

        let self = this;
        self.setState({ swiperVisiable: swipeVisiable });

        // axios.get(`https://290e8otzd9.execute-api.us-west-2.amazonaws.com/dev/network/`+networkId+'/message')
        //     .then(function (response) {
        //         console.log("message response",response.data);
        //         self.setState({messageList:response.data})
        //     })
        //     .catch(function (error) {
        //         console.log(error);
        //     });

    }

    // getting response from its child calling


    childHandler(dataFromChild, data) {
        let self = this
        self.openSwiper(true, 2);
        console.log('%cPrevious: ' + dataFromChild, "color:orange", "data", data);
        self.setState({ swiperVisiable: true, isMessage: true, id: self.state.networkId, messageIndex: dataFromChild, messageList: data })

    }





    // swipe card component

    renderCard = () => {

        console.log(this.state.messageList, "opopopopoooo")

        return (

            <View style={styles.cardContainer}>
                <View style={{ flex: 1, backgroundColor: 'white', alignItems: 'center', padding: 10, borderRadius: 25 }}>
                    <Image source={catImage} style={{ height: 200, width: 200, resizeMode: 'contain', marginTop: 10 }} />
                    <Text style={{ color: '#2F3979', fontWeight: '800', fontSize: 20, textAlign: 'center' }}>
                        {this.state.messageList.title}</Text>

                    <Text style={{
                        color: 'grey',
                        fontSize: 16, textAlign: 'center', top: 20
                    }}> {this.state.messageList.message} </Text>

                </View>

                <View style={{
                    backgroundColor: '#2F3979', borderBottomEndRadius: 10,
                    borderBottomStartRadius: 10, height: 80, alignItems: 'center', flexDirection: 'row', justifyContent: 'space-evenly'
                }}>

                    <TouchableOpacity style={{ bottom: 20 }} onPress={() => this.swiper.swipeLeft()}>
                        <Image source={cross} style={styles.imgStyle} />
                    </TouchableOpacity>
                    <TouchableOpacity style={{ bottom: 20 }} onPress={() => this.swiper.swipeRight()}>
                        <Image source={rightArrow} style={styles.imgStyle} />
                    </TouchableOpacity>

                </View>

            </View>



        )

    }
    // swipe with right direction

    onSwipeRight = () => {
        console.warn("right swipe")
        this.setState({ swiperVisiable: false })
        this.refs.modal1.open()
    }

    fbMessagenerSharing = () => {

        let msg = this.state.messageList.message

        if (msg) {
            let url = `fb-messenger://share?link=${msg}`
            Linking.openURL(url).then((data) => {
                console.log('facebook Opened');
            }).catch(() => {
                alert('Make sure facebook messenger installed on your device');
            });
        } else {
            alert('Please insert message to send');
        }
    }



    // sending messages of their specific types

    onSendingMessage = (type,firstName,phone) => {

        

         switch (type) {
            case 'facebook': this.fbMessagenerSharing(); break;
            case 'email': Communications.email(['emailAddress1', 'emailAddress2'], null, null, 'My Subject', 'My body text'); break;
            case 'text': Communications.text(phone, this.state.messageList.message); break;
            case 'phone': Communications.phonecall('9999999999'); break;
            default: this.refs.modal1.close(),this.setState({firstName:firstName}),this.refs.modalContactMethods.open();
                break;
        }

    }

    // line separotor for flatlist

    renderSeparator = () => {
        return (
            <View
                style={{
                    height: 0.5,
                    width: "100%",
                    alignSelf: 'center',
                    backgroundColor: "grey",
                }}
            />
        );
    };

    // returning jsx component 

    render() {
        const { swiperVisiable, messageList, messageIndex,activeMembersArray } = this.state;
        console.log(messageList, "litsssss")
        let dimension=40

        return (
            <View style={{ flex: 1 }}>

                {/* header view container */}
                <LinearGradient colors={['#085d87', '#27c7bb']} style={{ height: 150, backgroundColor: '#000' }}>
                    <View style={{
                        paddingVertical: 5, paddingHorizontal: paddingValue ? 120 : 20,
                        paddingTop: Platform.OS === 'android' ? 50 : 50, paddingBottom: Platform.OS === 'android' ? 50 : 50,
                        flexDirection: 'row', top: 20
                    }}>
                        <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                            <Image source={backArrow} style={{ height: 25, width: 25, resizeMode: 'contain' }} />
                        </TouchableOpacity>
                        <Text style={{ fontSize: 18, color: 'white', fontWeight: '600', left: 10 }}>Messages to send</Text>
                    </View>
                </LinearGradient>

                {/* main view container */}



                <ScrollableTabView
                    initialPage={0}
                    tabBarInactiveTextColor={'grey'}
                    tabBarUnderlineStyle={{ backgroundColor: '#BB281C', width: 100, height: 2 }}
                    tabBarActiveTextColor={'#BB281C'}
                    renderTabBar={() => <ScrollableTabBar />}>


                    {this.renderNetworkCard()}


                </ScrollableTabView>




                {/* swiping card component */}

                {swiperVisiable &&
                    <Swiper
                        ref={swiper => {
                            this.swiper = swiper
                        }}
                        verticalSwipe={false}
                        cards={this.state.cards}
                        renderCard={this.renderCard}
                        stackSize={3}
                        stackSeparation={15}
                        animateOverlayLabelsOpacity
                        onSwipedRight={() => this.onSwipeRight()}
                        onSwipedLeft={() => this.setState({ swiperVisiable: false })}
                        animateCardOpacity
                        showSecondCard={false}
                        onTapCardDeadZone={1}
                        backgroundColor={'#00000070'}
                        overlayLabels={{
                            left: {
                                title: 'Close',
                                element: <OverlayLabel label="left" color="grey" direction={"left"} isClose={true} />,
                                style: {
                                    label: {
                                        backgroundColor: 'black',
                                        borderColor: 'black',
                                        color: 'white',
                                        borderWidth: 1
                                    },
                                    wrapper: {
                                        flexDirection: 'column',
                                        alignItems: 'center',
                                        justifyContent: 'center',
                                        marginTop: 30,
                                        marginLeft: -30
                                    }
                                }
                            },
                            right: {
                                title: 'Forward',
                                element: <OverlayLabel label="right" color="grey" direction={"right"} />,
                                style: {
                                    label: {
                                        backgroundColor: 'black',
                                        borderColor: 'black',
                                        color: 'white',
                                        borderWidth: 1
                                    },
                                    wrapper: {
                                        flexDirection: 'column',
                                        alignItems: 'center',
                                        justifyContent: 'center',
                                        marginTop: 30,
                                        marginLeft: 30
                                    }
                                }
                            },
                        }}
                        containerStyle={{
                            justifyContent: 'center', backgroundColor: '#00000070'
                        }} />
                }

                {/* modal for user contact lists */}

                <Modal ref={"modal1"} backdrop={false} style={styles.modal}>
                    <View style={styles.modalView}>

                        <Text onPress={() => this.refs.modal1.close()} style={{ padding: 20, color: 'black' }}>X</Text>
                        <View style={{ alignItems: 'center', padding: 20 }}>
                            <Text style={{
                                color: '#004772', fontWeight: '500',
                                fontSize: 22, textAlign: 'center'
                            }}>{!isEmpty(messageList) && messageList.title} </Text>

                            <Text style={{
                                color: 'grey',
                                fontSize: 16, textAlign: 'center', top: 20
                            }}>{messageList.message} </Text>
                        </View>

                        <View style={{ height: 20 }} />

                        {isEmpty(activeMembersArray)?
                        <View style={{alignItems:'center',justifyContent:'center',marginTop:50}}>
                            <Text style={{fontSize:15,fontWeight:'500'}}>There are no added user in your contact list ! </Text> 
                            <Text style={{fontSize:15,fontWeight:'500'}}>Please add user from home tab </Text> 
                        </View>
                        :

                        <FlatList
                            data={activeMembersArray}
                            renderItem={({ item }) =>

                                <View style={{ padding: 15, flexDirection: 'row', justifyContent: 'space-between' }}>
                                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                        {/* <Image source={DefaultUserPic} style={{ height: 40, width: 40, borderRadius: 20 }} /> */}

                                        <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                                            {item.hasThumbnail ? <Image
                                                style={{ width: dimension, height: dimension, alignSelf: 'center', borderRadius: dimension / 2 }}
                                                source={{ uri: item.thumbnailPath }}
                                            /> : <View style={{ height: dimension, width: dimension, borderRadius: dimension / 2, backgroundColor: "#ccc", justifyContent: 'center', alignItems: 'center' }}>
                                                    <Text style={{ fontSize: 18, fontWeight: '800', color: '#fff' }}>{item.firstName ? item.firstName.charAt(0) : ''}{item.lastName ? item.lastName.charAt(0) : ''}</Text></View>}
                                        </View>

                                        <Text style={{ color: '#000', fontSize: 10, textAlign: 'center', paddingHorizontal: 5, fontWeight: '500' }}>{item.firstName} {item.lastName}</Text>
                                    </View>
                                    <View style={{ flexDirection: 'row', alignSelf: 'center' }}>
                                        <TouchableOpacity onPress={() => this.onSendingMessage("text",item.firstName,item.phone)}
                                            style={{ borderRadius: 2, alignSelf: 'flex-start', padding: 4, borderColor: 'blue', borderWidth: 1, right: 10 }}>
                                            <Text style={{ color: 'blue', fontSize: 10, textAlign: 'center', fontWeight: '500' }}>
                                            Send via text
                                            </Text>
                                        </TouchableOpacity>
                                        <TouchableOpacity>

                                            <Text style={{ color: 'red', fontSize: 15 }}>x</Text>

                                        </TouchableOpacity>

                                    </View>
                                </View>

                            }
                            ItemSeparatorComponent={this.renderSeparator} />

                      }    
                    </View>

                </Modal>


                {/* modal for user select contact methods*/}

                <Modal ref={"modalContactMethods"} backdrop={false} style={styles.modalContacts}>
                    <View style={styles.modalContactsView}>

                        <View style={{ backgroundColor: 'white', borderRadius: 20, }}>

                            <View style={{ flexDirection: 'row', padding: 15, }}>
                                <Image
                                    style={{ width: 50, height: 50, alignSelf: 'center', borderRadius: 50 / 2 }}
                                    source={DefaultUserPic}
                                />
                                <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                                    <Text style={{

                                        paddingHorizontal: 10,
                                        textAlign: 'center',
                                        fontSize: 16, fontWeight: '600', color: '#505050'
                                    }}>{this.state.firstName}</Text>
                                </View>
                            </View>

                            <View
                                style={{
                                    height: 0.5,
                                    width: "100%",
                                    alignSelf: 'center',
                                    backgroundColor: "grey",
                                }}
                            />

                            <FlatList
                                data={ContactMethods}
                                renderItem={({ item, index }) =>

                                    <TouchableOpacity onPress={() => this.modalContactsAction(item, index)} style={{ flex: 1, padding: 15, flexDirection: 'row', }}>
                                        <View style={{ flex: 1, flexDirection: 'row', }}>
                                            <Text style={{ color: '#3f50b5', fontSize: 15, textAlign: 'left', fontWeight: '500' }}>{item.key}</Text>
                                        </View>

                                        <Text style={{ color: '#000', fontSize: 15, textAlign: 'right', fontWeight: '500' }}>{item.value}</Text>

                                    </TouchableOpacity>

                                }
                            />

                        </View>



                        <TouchableOpacity
                            style={styles.buttonClose}
                            onPress={() => this.modalContactsClose()}
                            underlayColor='#fff'>

                            <Text style={{
                                color: '#3f50b5',
                                textAlign: 'center',
                                justifyContent: 'center',
                                fontSize: 15,
                                paddingLeft: 10,
                                paddingRight: 10,
                            }}>Cancel</Text>

                        </TouchableOpacity>



                    </View>

                </Modal>



            </View>
        );
    }

    modalContactsClose() {

        this.refs.modalContactMethods.close();
    }
    modalContactsAction(item, index) {

        this.onSendingMessage(item.type);


    }

}

const styles = {
    container: {
        flex: 1,
        backgroundColor: 'red',
        alignItems: 'center',
        justifyContent: 'center'
    },
    modal: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#00000070',
        width: width - 20,
        height: height,
        borderRadius: 20,
        top: height / 12
    },
    modalView: {
        backgroundColor: 'white', height: height, width: width - 20, borderRadius: 20
    },
    cardContainer: {
        width: width - 30,
        height: height > 700 ? height * 0.7 : height * 0.8,
        backgroundColor: '#FFF',
        alignSelf: 'center',
        shadowColor: '#000',
        borderRadius: 50,
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.5,
        shadowRadius: 2,
        elevation: 2,
        top: 50
    },
    swipeBtncontainer: {
        width: 120,
        height: 90,
        bottom: 40,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.5,
        shadowRadius: 2,
        elevation: 2,
        borderRadius: 35, backgroundColor: 'white', alignItems: 'center', justifyContent: 'center'
    },
    imgStyle: {
        width: 120,
        height: 120, resizeMode: 'contain'
    },
    titleStyle: {
        top: 10,
        fontWeight: '500',
    },
    modalContacts: {
        justifyContent: 'flex-end',
        alignItems: 'center',
        width: width,
        height: height,
        borderRadius: 10,
        backgroundColor: 'rgba(0,0,0,0.4)',
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.5,
        shadowRadius: 2,
        elevation: 2,

    },
    modalContactsView: {
        backgroundColor: 'transparent',
        height: height / 2,
        width: width - 50,
        borderRadius: 10,
    },

    buttonClose: {
        backgroundColor: 'white',
        width: width - 50,
        height: 55,
        marginTop: 20,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 10,
    },

}
