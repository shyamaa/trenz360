import React from 'react'
import { View, Text,Image,TouchableOpacity } from 'react-native';
import rightArrow from '../imgAssests/right-arrow-red.png';
import leftArrow from '../imgAssests/left-arrow-grey.png';



const SwipeButton = ({ title,direction,textColor,onpress }) => {
    return (
        <TouchableOpacity style={styles.container} onPress={onpress}>
             <Image source={ direction == "left" ? leftArrow : rightArrow } style={styles.imgStyle}/> 
             <Text style={[styles.titleStyle ,{ color:textColor}]}>{title}</Text>
        </TouchableOpacity>
    )
}

const styles = {
    
    container: {
        width:120, 
        height:90,
        bottom:40,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.5,
        shadowRadius: 2,
        elevation: 2,
        borderRadius:35, backgroundColor: 'white', alignItems: 'center', justifyContent: 'center'
    },
    imgStyle:{
       height:30,width:30,resizeMode:'contain'
    },
    titleStyle:{
        top:10,
        fontWeight:'500',
    }
}

export default SwipeButton;