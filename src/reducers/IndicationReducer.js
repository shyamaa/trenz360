import { SHOW_LOADER } from '../actions/ActionsConstants';
import { HIDE_LOADER } from '../actions/ActionsConstants';

const initialState = {
    isLoaderShowing:false,
    name:"name",
    loggedInUserData:null,
    loggedInUserType:null
};

const indicationReducer = (state = initialState, action) => {
    switch(action.type) {
        case SHOW_LOADER:
            return{
                ...state,
            isLoaderShowing: true
        };
        case HIDE_LOADER:
            return{
                ...state,
                isLoaderShowing: false
        };
        default:
            return state;
    }
}

const loginRedcuer = (state = initialState, action) => {
    switch(action.type) {
        case SAVE_FACEBOOK_DATA:
            return{
                ...state,
                loggedInUserData: action.data,
                loggedInUserType: 'facebook',
                isLoaderShowing: true
            };
        case SAVE_GOOGLE_DATA:
            return{
                ...state,
                loggedInUserData: action.data,
                loggedInUserType: 'google',
                isLoaderShowing: false
            };
        case SAVE_HEALTHKIT_DATA:
            return{
                ...state,
                loggedInUserData: action.data,
                loggedInUserType: 'manual',
            };
        default:
            return state;
    }
}

export default indicationReducer;