
import React from 'react';
import {
    AppRegistry,
    AsyncStorage
} from 'react-native';

import { Provider } from 'react-redux';
import {appContainer, AppContainer} from './navigation/Router'
import configureStore from './store';
import {APP_DISPLAY_NAME} from './actions/StringConstants';

const store = configureStore()

export default function native(platform) {
    class Buzz360 extends React.Component {

        constructor() {
            super();
            this.state = {
                isLoggerIn: false,
            }
        }

        async componentDidMount() {
            let v = await this.checkIfUserLoggedIn();
            this.setState({isLoggerIn:v});
        }

        async checkIfUserLoggedIn(){
            let isLogin = await AsyncStorage.getItem('isLogin');
            if (isLogin === "true"){
                return true;
            } else {
                return false;
            }
        }


        render() {
            let isUserLoggedIn = this.state.isLoggerIn;

            const AppContainer = appContainer(isUserLoggedIn)
            return (
                <Provider store={store}>
                    <AppContainer/>
                </Provider>
            );
        }
    }
    AppRegistry.registerComponent(APP_DISPLAY_NAME, ()=>Buzz360);
}
