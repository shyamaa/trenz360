import React, { Component } from 'react';
import {
    AppRegistry,
    Text,
    View,
    TouchableOpacity,
    ImageBackground,
    Dimensions,
    TextInput,
    Image,
    Alert,
    ActivityIndicator,
    ScrollView,
    KeyboardAvoidingView,
    Platform, Button,
    TouchableHighlight,
    StyleSheet, Modal
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { TextField } from 'react-native-material-textfield';
import logo from '../../imgAssests/logo.png';
import loginarrow from '../../imgAssests/login-arrow.png'
import { showInfoAlert } from '../../utils/AlertPopUp';
import { EMAIL_VALIDATION_MESSAGE, PASSWORD_VALIDATION_MESSAGE, CNF_PASSWORD_VALIDATION_MESSAGE, MOBILE_VALIDATION_MESSAGE, PASSWORD_FORMAT_VALIDATION_MESSAGE } from '../../actions/StringConstants';
import { validEmail, validPassword, validConfirmPassword, validPhoneNumber, validPasswordFormat } from '../../utils/Validations';
import { onUserRegister, onUserLogin } from '../../store/actions/AuthAction'
import Amplify, { Auth } from 'aws-amplify';
import config from '../../config/aws-exports'
import Loader from '../../components/Loader';
Amplify.configure(config);



let deviceWidth = Dimensions.get('window').width;
let deviceHeight = Dimensions.get('window').height;


const TAG = "SignIn Screen => \n";

export default class Signup extends Component {

    constructor() {
        super();
        this.state = {
            isLoading: false,
            firstName: "",
            lastName: "",
            email: "",
            mobile: "",
            password: "",
            confirmPassword: "",
            showButton: false,
        }
    }

    showButtonValidate = () => {

        let { firstName, lastName, email, mobile, password, confirmPassword } = this.state;

        if (firstName.length > 0 && lastName.length > 0
            && email.length > 0 && mobile.length > 0
            && password.length > 0 && confirmPassword.length > 0) {
            this.setState({ showButton: true });
        } else {
            this.setState({ showButton: false });
        }

    }


    loginClicked() {
        this.props.navigation.navigate("Login");
    }

    async onFirstNameTextChange(text) {
        await this.setState({ firstName: text });
        this.showButtonValidate();
    }
    async onLastNameTextChange(text) {
        await this.setState({ lastName: text });
        this.showButtonValidate();
    }
    async onEmailTextChange(text) {
        await this.setState({ email: text });
        this.showButtonValidate();
    }
    async onMobileTextChange(text) {
        await this.setState({ mobile: text });
        this.showButtonValidate();
    }
    async onPasswordTextChange(text) {
        await this.setState({ password: text });
        this.showButtonValidate();
    }
    async onConfirmPasswordTextChange(text) {
        await this.setState({ confirmPassword: text });
        this.showButtonValidate();
    }

   
    inputValidation = () => {

        let { email, password, confirmPassword, mobile } = this.state;

        if (validPhoneNumber(mobile)) {
            showInfoAlert(MOBILE_VALIDATION_MESSAGE)
            return false

        }

        if (validEmail(email)) {
            showInfoAlert(EMAIL_VALIDATION_MESSAGE)
            return false;
        }

        if (validPassword(password)) {
            showInfoAlert(PASSWORD_VALIDATION_MESSAGE)
            return false;
        }

        if (validPasswordFormat(password)) {
            showInfoAlert(PASSWORD_FORMAT_VALIDATION_MESSAGE)
            return false;
        }

        if (validPassword(confirmPassword)) {
            showInfoAlert(PASSWORD_VALIDATION_MESSAGE)
            return false;
        }

        if (validConfirmPassword(confirmPassword, password)) {
            showInfoAlert(CNF_PASSWORD_VALIDATION_MESSAGE)
            return false;
        }


        return true
    }

    async signupClicked() {

        this.setState({ isLoading: true })

        if (this.inputValidation()) {

            let { firstName, lastName, email, mobile, password } = this.state;
            let obj = {
                firstName: firstName,
                lastName: lastName,
                email: email,
                mobile: mobile,
                password: password
            }
            // this.props.navigation.navigate("SignupStarted",{data:obj});

            let loginSession = await onUserRegister(email, password, mobile);
            console.log(loginSession, "sessionnn")
            if (loginSession !== undefined) {
                let session = await onUserLogin(email, password);
                if (session !== undefined) {
                    this.setState({ isLoading: false })
                    this.props.navigation.navigate("SignupStarted", { data: obj });

                }
            } else {
                this.setState({ isLoading: false })
            }

        } else {
            this.setState({ isLoading: false })
        }

    }




    render() {

        let { isLoading } = this.state;

        return (


            <LinearGradient colors={['#085d87', '#27c7bb']} style={{ flex: 1 }}>

                <ScrollView style={{ flex: 1, }} keyboardShouldPersistTaps={'handled'}>

                    <View style={{ flex: 1 }}>

                        {/*header view*/}
                        <View style={{ marginTop: deviceHeight > 700 ? 70 : 50, alignItems: 'center' }}>
                            <Image
                                style={{ height: 75, width: 300 }}
                                resizeMode="contain"
                                source={logo} />
                        </View>

                        {/*main view container */}
                        <KeyboardAvoidingView behavior={Platform.OS === "ios" ? "position" : null}
                            keyboardVerticalOffset={Platform.OS === "ios" ? -200 : 0}>
                            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'space-around', height: 'auto', marginBottom: 40 }}>


                                <View style={{
                                    width: deviceWidth - 50, padding: 10, borderRadius: 10,
                                    backgroundColor: 'white', alignItems: 'center', marginTop: '10%'
                                }}>


                                    <Text style={{ color: 'blue', fontSize: 20, fontWeight: '500', marginTop: 5 }}>Sign Up</Text>


                                    <TextField
                                        onChangeText={(text) => this.onFirstNameTextChange(text)}
                                        underlineColorAndroid='transparent'
                                        autoCapitalize='none'
                                        label="First Name"
                                        value={this.state.firstName}
                                        placeholderTextColor="#D8D9DB"
                                        baseColor={'grey'}
                                        tintColor={'grey'}
                                        containerStyle={styles.textInput}

                                    />

                                    <TextField
                                        onChangeText={(text) => this.onLastNameTextChange(text)}
                                        underlineColorAndroid='transparent'
                                        autoCapitalize='none'
                                        label="Last Name"
                                        value={this.state.lastName}
                                        placeholderTextColor="#D8D9DB"
                                        baseColor={'grey'}
                                        tintColor={'grey'}
                                        containerStyle={styles.textInput}

                                    />

                                    <TextField
                                        onChangeText={(text) => this.onEmailTextChange(text)}
                                        underlineColorAndroid='transparent'
                                        autoCapitalize='none'
                                        label="Email Address"
                                        value={this.state.firstName}
                                        placeholderTextColor="#D8D9DB"
                                        baseColor={'grey'}
                                        tintColor={'grey'}
                                        textContentType='telephoneNumber'
                                        dataDetectorTypes='phoneNumber'

                                        containerStyle={styles.textInput}

                                    />



                                    <TextField
                                        onChangeText={(text) => this.onMobileTextChange(text)}
                                        underlineColorAndroid='transparent'
                                        autoCapitalize='none'
                                        label="Phone Number"
                                        value={this.state.mobile}
                                        placeholderTextColor="#D8D9DB"
                                        baseColor={'grey'}
                                        tintColor={'grey'}
                                        textContentType='telephoneNumber'
                                        dataDetectorTypes='phoneNumber'
                                        keyboardType='phone-pad'
                                        maxLength={10}
                                        containerStyle={styles.textInput}

                                    />

                                    <TextField
                                        onChangeText={(text) => this.onPasswordTextChange(text)}
                                        underlineColorAndroid='transparent'
                                        autoCapitalize='none'
                                        label="Password"
                                        maxLength={25}
                                        secureTextEntry={true}
                                        value={this.state.password}
                                        placeholderTextColor="#D8D9DB"
                                        baseColor={'grey'}
                                        tintColor={'grey'}
                                        containerStyle={styles.textInput}

                                    />

                                    <TextField
                                        onChangeText={(text) => this.onConfirmPasswordTextChange(text)}
                                        underlineColorAndroid='transparent'
                                        autoCapitalize='none'
                                        label="Re-type Password"
                                        maxLength={25}
                                        secureTextEntry={true}
                                        value={this.state.confirmPassword}
                                        placeholderTextColor="#D8D9DB"
                                        baseColor={'grey'}
                                        tintColor={'grey'}
                                        containerStyle={styles.textInput}

                                    />

                                    {/*button view container*/}

                                    <View style={{
                                        marginTop: 30, backgroundColor: '#fff', width: deviceWidth - 50,
                                        alignItems: 'center', borderRadius: 10
                                    }}>

                                        {
                                            this.state.showButton === true ?
                                                <TouchableOpacity
                                                    style={styles.buttonFacebook}
                                                    onPress={() => this.signupClicked()}
                                                    underlayColor='#fff'>
                                                    <Text style={{
                                                        color: '#fff',
                                                        textAlign: 'center',
                                                        paddingLeft: 10,
                                                        paddingRight: 10
                                                    }}>Sign Up</Text>
                                                </TouchableOpacity>
                                                : <View style={{
                                                    paddingTop: 15,
                                                    height: 50,
                                                }} />
                                        }

                                        <View style={{ width: deviceWidth - 50, height: 10, borderRadius: 30 }}></View>


                                    </View>
                                </View>
                            </View>

                            <TouchableOpacity style={{ marginBottom: 40, alignSelf: 'center', flexDirection: 'row' }} onPress={() => this.loginClicked()}>
                                <Text style={{ fontSize: 15, color: '#fff', fontWeight: '800' }}>Login</Text>
                                <Image source={loginarrow} style={{ height: 20, width: 20, resizeMode: 'contain', left: 10, top: 2 }} />
                            </TouchableOpacity>
                        </KeyboardAvoidingView>

                    </View>
                </ScrollView>


                <Modal animationType={"slide"} transparent={true} visible={isLoading} onRequestClose={() => { console.log("Modal has been closed.") }}>
                    <View style={styles.modalView}>
                        <ActivityIndicator size='large' color='#FFF' />
                        <Text style={{ marginTop: 12, fontSize: 18, fontWeight: 'bold', color: '#FFF' }}>Loading</Text>
                    </View>
                </Modal>




            </LinearGradient>


        )
    }
}


const styles = StyleSheet.create({

    textInput: {
        height: 45,
        marginTop: 20,
        backgroundColor: 'rgba(255, 255, 255, 0.6)',
        //borderBottomWidth: 1,
        borderBottomColor: 'gray',
        width: deviceWidth - 80,
        // color: 'gray'
    },
    buttonFacebook: {
        width: deviceWidth - 100,
        height: 50,
        paddingTop: 15,
        // paddingBottom: 5,
        backgroundColor: '#3f50b5',
        borderRadius: 5,
        borderWidth: 1,
        borderColor: '#fff',
    },
    modalView: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'black',
        opacity: 0.8
    },

})