import React, {Component} from 'react';
import {
    TouchableOpacity,
    StyleSheet,
    Text,
    Image,
} from 'react-native';
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from 'react-native-responsive-screen'
import {colorTheme, buttonTextColor} from '../res/Colors'


export default class MyButton extends Component{

    static navigationOptions = {
        header: null,
    };


    static defaultProps = {
        title: "Button Default",
        buttonStyle: {},
        iconStyle: {},
        icon:'',
        textStyle: {},
        onPress: () => {},
    }


    render() {
        return (
            <TouchableOpacity style={[ styles.button, this.props.buttonStyle ]} onPress={this.props.onPress}>
                <Image style={[styles.icon, this.props.iconStyle]} source={this.props.icon}/>
                <Text style={[styles.text, this.props.textStyle]}> {this.props.title} </Text>
            </TouchableOpacity>
        );
    }
};



const styles = StyleSheet.create({
    button:{
        flexDirection: 'row',
        alignItems:'center',
        justifyContent: 'center',
        height: hp("6%"),
        width: wp('50%'),
        backgroundColor: colorTheme,
        borderRadius: wp(1.5)
    },

    text:{
        color: buttonTextColor ,
        fontSize: hp("2.5%")
    },

    icon: {
        width: wp(4.5),
        height: wp(4.5),
        marginRight: wp(1),
        tintColor: 'white'
    }

});
