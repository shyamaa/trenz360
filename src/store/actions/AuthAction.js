import React from 'react';
import {AsyncStorage} from 'react-native'
import { showActionAlert,showInfoAlert } from '../../utils/AlertPopUp';
import Amplify, { Auth } from 'aws-amplify';
import config from '../../config/aws-exports'
Amplify.configure(config);

export const onUserLogin = (email,password) =>{
  
    return Auth.signIn(email,password)
                .then(user => {
                    const token = user.signInUserSession.idToken.jwtToken;
                    AsyncStorage.setItem('userToken', token);
                    AsyncStorage.setItem('isLogin', "true");
                    return token;
                })
                .catch(err => {
                    console.log('error signing in: ', err)
                    showInfoAlert(err.message)
     })

}

export const onUserRegister =(email,password,mobile)=>{

    console.log("mobile",mobile)
   
    return Auth.signUp({
        username:email,
        password:password, 
        attributes: {
            email:email,
            phone_number:"+1"+mobile,

        }
     }).then(user => {
            console.log('res signup in: ',user);
            return user.userSub;
            
        })
        .catch(err => {
            console.log('error signing up: ', err)
            showInfoAlert(err.message)
            
        })

}
export const onUserUpdate =(obj)=>{
     const putMethod = {
        method: 'PUT', // Method itself
        headers: {
         'Content-type': 'application/json; charset=UTF-8' // Indicates the content 
        },
        body: JSON.stringify(obj) // We send data in JSON format
    }
}

export const onUserForgotPassword = (email) =>{
    console.log(email)
}

export const onUserPasswordUpdate = (password) =>{
    console.log(email)
}