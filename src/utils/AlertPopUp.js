import {Alert} from "react-native";
//import {APP_DISPLAY_NAME} from '../actions/StringConstants';

let APP_DISPLAY_NAME = "SwipeRed"

export function showInfoAlert(alertMessage) {
    Alert.alert(
        APP_DISPLAY_NAME,
        alertMessage,
        [
            {text: 'Ok', onPress: () => console.log('OK Pressed')},
        ],
        {cancelable: false},
    )
}

export function showActionAlert(alertMessage) {
    Alert.alert(
        APP_DISPLAY_NAME,
        alertMessage,
        [
            {text: 'Ok', onPress: () => console.log('Ask me later pressed')},
            {
                text: 'Cancel',
                onPress: () => console.log('Cancel Pressed'),
                style: 'cancel',
            },
            {text: 'Ok', onPress: () => console.log('OK Pressed')},
        ],
        {cancelable: false},
    )
}
