import React, { Component } from 'react'
import Swiper from 'react-native-deck-swiper'
import { ScrollView, StyleSheet, Text, View,Dimensions,Image } from 'react-native'
import OverlayLabel from '../components/OverlayLabel'
import SwipeButton from '../components/SwipeButton'
import LinearGradient from 'react-native-linear-gradient';
import mobile from '../imgAssests/mobile.png'
import logo from '../imgAssests/logo.png'
const { width, height } = Dimensions.get('window')

export default class InitialLoadingScreen extends Component {

    constructor(props) {
        super(props)
        this.state = {
            cards: [1],
            swipedAllCards: false,
            swipeDirection: '',
            cardIndex: 0
        }
    }

    renderCard = (card, index) => {

        return (
            <View style={styles.cardContainer}>
           
            <View style={{ flex: 1, backgroundColor: 'white', alignItems: 'center', padding: 10, borderRadius: 25 }}>
                <Image source={mobile} style={styles.swipeImgStyle} />
                <Text style={{ color: '#3F3D54', fontWeight: '800', fontSize:25, textAlign: 'center' }}>
                    Lets make a {'\n'} difference, together </Text>

                <Text style={{
                    color: 'grey',
                    fontSize:15, textAlign: 'center', top: 20
                }}>Swipe right to get started, or left to login to{'\n'} an existing account.</Text>

            </View>

            <View style={{
                backgroundColor: '#2F3979',borderBottomEndRadius:10,
                borderBottomStartRadius: 10, height: 80, alignItems: 'center', flexDirection: 'row', justifyContent: 'space-evenly'
            }}>
                <SwipeButton direction={"left"} title={'Login'} textColor={'#000'} onpress={()=>this.swiper.swipeLeft()} />
                <SwipeButton direction={"right"} title={'Lets Go'} textColor={'red'} onpress={()=>this.swiper.swipeRight()}/>
            </View>

        </View>
        )
    };

   // swipe with right direction

     onSwipeRight = () => {

        this.props.navigation.navigate("Signup");
       
    }

      // swipe with left direction

      onSwipeLeft = () => {

        this.props.navigation.navigate("Login");
        
    }

    render() {
        return (

             <LinearGradient colors={['#085d87', '#27c7bb']} style={styles.container}>
               

                <View style={{alignItems:'center',marginTop:height>700 ? 70 :60}}>
                
                    <Image style={styles.logoStyle} resizeMode="contain" source={logo} />
                              

                </View>

                <Swiper
                    ref={swiper => {
                        this.swiper = swiper
                    }}
                    verticalSwipe={false}
                    cards={this.state.cards}
                    renderCard={this.renderCard}
                    stackSize={3}
                    stackSeparation={15}
                    animateOverlayLabelsOpacity
                    onSwipedRight={() => this.onSwipeRight()}
                    onSwipedLeft={() => this.onSwipeLeft()}
                    animateCardOpacity
                    showSecondCard={false}
                    onTapCardDeadZone={1}
                    backgroundColor={'transaparent'}
                    overlayLabels={{
                        left: {
                            title: 'Close',
                            element: <OverlayLabel label="left" color="grey" direction={"left"} />,
                            style: {
                                label: {
                                    backgroundColor: 'black',
                                    borderColor: 'black',
                                    color: 'white',
                                    borderWidth: 1
                                },
                                wrapper: {
                                    flexDirection: 'column',
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                    marginTop: 30,
                                    marginLeft: -30
                                }
                            }
                        },
                        right: {
                            title: 'Forward',
                            element: <OverlayLabel label="right" color="grey" direction={"right"} />,
                            style: {
                                label: {
                                    backgroundColor: 'black',
                                    borderColor: 'black',
                                    color: 'white',
                                    borderWidth: 1
                                },
                                wrapper: {
                                    flexDirection: 'column',
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                    marginTop: 30,
                                    marginLeft: 30
                                }
                            }
                        },
                    }}
                    containerStyle={{
                        justifyContent: 'center', top: 100,
                     }} />

                     

           </LinearGradient>   





          
        )
    }
}

const styles = StyleSheet.create({
   
    container: {
        flex: 1,
       // backgroundColor: 'red',
     },
    cardContainer: {
        width: width - 30,
        height: height>700 ?height*0.6:height*0.65,
        backgroundColor: '#FFF',
        shadowColor: '#000',
        borderRadius: 50,
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.5,
        shadowRadius: 2,
        elevation: 2,
        marginTop:30,
        alignSelf:'center'


    },
    text: {
        textAlign: 'center',
        fontSize: 30,
        backgroundColor: 'transparent'
    },
    swipeImgStyle:{
        height:height>700 ?200:150, 
        width:height>700 ? 200:150,
        resizeMode:'contain' 
    },
    logoStyle:{
        height:75,
        width: 300
    }

})