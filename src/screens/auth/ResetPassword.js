import React, { Component } from 'react';
import {
    AppRegistry,
    Text,
    View,
    TouchableOpacity,
    ImageBackground,
    Dimensions,
    TextInput,
    Image,
    Alert,
    ActivityIndicator,
    ScrollView,
    KeyboardAvoidingView,
    Platform, Button,
    TouchableHighlight,
    StyleSheet,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { TextField } from 'react-native-material-textfield';
import logo from '../../imgAssests/logo.png';
import loginarrow from '../../imgAssests/login-arrow.png'

import {
    APP_DISPLAY_NAME,
    EMAIL_EMPTY_MESSAGE,
    EMAIL_VALIDATION_MESSAGE, INTERNET_CONNECTION_ISSUE,
    LOGIN_ERROR,
    PASSWORD_EMPTY_MESSAGE, PASSWORD_VALIDATION_MESSAGE,CONFIRM_CODE_VALIDATION_MESSAGE,CONFIRM_CODE_EMPTY_MESSAGE
} from '../../actions/StringConstants';

import { showInfoAlert } from "../../utils/AlertPopUp";
import { validEmail } from "../../utils/Validations";
import { Auth } from 'aws-amplify';


let deviceWidth = Dimensions.get('window').width;
let deviceHeight = Dimensions.get('window').height;
const paddingValue = Platform.isPad ? true : false;
import { LoginManager, AccessToken, GraphRequest, GraphRequestManager } from "react-native-fbsdk";

import { getFBAccessToken, saveFBAccessToken, saveGlobalConfigurations, saveLoginDetails } from '../../utils/SaveDetails';
import Loader from '../../components/Loader';

const TAG = "SignIn Screen => \n";

export default class ResetPassword extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            showButton: false,
            email:props.navigation.state.params.data,
            otp: "",
            password: "",
            confirmPassword: "",
        }
    }

    componentDidMount() {
    }

    async onOtpTextChange(text) {
        await this.setState({ otp: text });

        let userPassword = this.state.password;
        let userConfirmPassword = this.state.confirmPassword;
        let userOTP = this.state.otp;

        if (userOTP.length > 0
            && userPassword.length > 0 && userConfirmPassword.length > 0) {
            this.setState({ showButton: true });
        } else {
            this.setState({ showButton: false });
        }


    }

    onPasswordTextChange(text) {
        this.setState({ password: text });
        let { password, confirmPassword,otp} = this.state

        if (password !== '' && confirmPassword !== '' && otp !== '') {
            this.setState({ showButton: true });
        } else {
            this.setState({ showButton: false });
        }




    }

    async onConfirmPasswordTextChange(text) {
        await this.setState({ confirmPassword: text });

        this.setState({ password: text });
        let { password, confirmPassword,otp } = this.state

        if (password !== '' && confirmPassword !== '' && otp !== '') {
            this.setState({ showButton: true });
        } else {
            this.setState({ showButton: false });
        }

    }

    submitClicked() {

        let self = this;

        let {email,password,otp} = self.state;

        if(otp.length <=0){
            showInfoAlert(CONFIRM_CODE_EMPTY_MESSAGE)
            return;
        }

        if (otp.length < 6 ) {
            showInfoAlert(CONFIRM_CODE_VALIDATION_MESSAGE)
            return;
        }


        if (email.length <= 0) {
            showInfoAlert(EMAIL_EMPTY_MESSAGE)
            return;
        }

        if (validEmail(email)) {
            showInfoAlert(EMAIL_VALIDATION_MESSAGE)
            return;
        }

        if (password.length <= 0) {
            showInfoAlert(PASSWORD_EMPTY_MESSAGE)
            return;
        }



        if (password.length < 8) {
            showInfoAlert(PASSWORD_VALIDATION_MESSAGE)
            return;
        }

        else {
            this.setState({ isLoading: true });
            //   alert("Hit API")

            // Collect confirmation code and new password, then
            // Auth.forgotPasswordSubmit(userEmail, code, userPassword)
            //     .then(data => console.log(data))
            //     .catch(err => console.log(err));

            let {email,otp,password} = this.state;

            Auth.forgotPasswordSubmit(email, otp, password)
            .then(data => 
               
                 self.setState({ isLoading: false },()=>{
                    showInfoAlert("Password update successfully."),
                    self.props.navigation.navigate("Login")
                 }),
               
           ).catch(e=>{
               console.log(e)
           })

           // this.props.navigation.navigate("RecentPost");

        }

    }

    loginClicked() {
        this.props.navigation.navigate("Login");
    }

    render() {
        return (


            <LinearGradient colors={['#FF0000', '#A92217']} style={{ flex: 1 }}>


                <View style={{ flex: 1, height: 'auto' }}>

                    {/*header view */}
                    <View style={{ marginTop: 60, alignItems: 'center' }}>
                        <Image
                            style={{ height: 60, width: 300, marginTop: 30 }}
                            resizeMode="contain"
                            source={logo} />
                    </View>

                    {/*main view container */}
                    <View style={{ flex: 1, alignSelf: 'center', justifyContent: 'space-around', height: 'auto' }}>


                        <View style={{
                            width: deviceWidth - 50, padding: 10, borderRadius: 10,
                            backgroundColor: 'white', alignItems: 'center', marginTop: '10%'
                        }}>

                            <Text style={{ color: '#3f50b5', fontSize: 25, fontWeight: '800', marginTop: 15 }}>New Password</Text>
                            
                             
                             
                            <TextField
                                onChangeText={(text) => this.onOtpTextChange(text)}
                                underlineColorAndroid='transparent'
                                autoCapitalize='none'
                                label="Confirm Code"
                                value={this.state.otp}
                                secureTextEntry={true}
                                placeholderTextColor="#D8D9DB"
                                baseColor={'grey'}
                                tintColor={'grey'}
                                maxLength={6}
                                containerStyle={styles.textInput}

                            />
                            
                           
                            <TextField
                                onChangeText={(text) => this.onPasswordTextChange(text)}
                                underlineColorAndroid='transparent'
                                autoCapitalize='none'
                                label="Password"
                                value={this.state.password}
                                secureTextEntry={true}
                                placeholderTextColor="#D8D9DB"
                                baseColor={'grey'}
                                tintColor={'grey'}
                                containerStyle={styles.textInput}

                            />

                            <TextField
                                onChangeText={(text) => this.onConfirmPasswordTextChange(text)}
                                underlineColorAndroid='transparent'
                                secureTextEntry={true}
                                autoCapitalize='none'
                                label="Re-Enter New Password"
                                value={this.state.confirmPassword}
                                placeholderTextColor="#D8D9DB"
                                baseColor={'grey'}
                                tintColor={'grey'}
                                containerStyle={styles.textInput}

                            />
                            <View style={{ backgroundColor: 'grey', height: 0, width: deviceWidth - 90, marginTop: 20 }} />





                            {/*button view container*/}

                            <View style={{
                                marginTop: 20, backgroundColor: '#fff', width: deviceWidth - 50,
                                alignItems: 'center', borderRadius: 10
                            }}>

                                {
                                    this.state.showButton === true ?
                                        <TouchableOpacity
                                            style={styles.buttonFacebook}
                                            onPress={() => this.submitClicked()}
                                            underlayColor='#fff'>
                                            <Text style={{
                                                color: '#fff',
                                                textAlign: 'center',
                                                paddingLeft: 10,
                                                paddingRight: 10,
                                                fontWeight: '800'
                                            }}>Submit</Text>
                                        </TouchableOpacity>
                                        : <View style={{
                                            marginTop: 40,
                                            height: 50,
                                        }} />
                                }

                                <View style={{ width: deviceWidth - 50, height: 10, borderRadius: 30 }}></View>


                            </View>




                        </View>

                        <TouchableOpacity style={{ marginTop: 100, marginBottom: 20, alignSelf: 'center', flexDirection: 'row' }} onPress={() => this.loginClicked()}>
                            <Text style={{ fontSize: 20, color: '#fff', fontWeight: '500', }}>Login</Text>
                            <Image source={loginarrow} style={{ height: 20, width: 20, resizeMode: 'contain', left: 10, top: 2 }} />

                        </TouchableOpacity>

                    </View>

                </View>

               {this.state.isLoading && <Loader/>}

            </LinearGradient>


        )
    }
}


const styles = StyleSheet.create({

    textInput: {
        height: Platform.OS === 'ios' ? 40 : 45,
        marginTop: Platform.OS === 'android' ? 20 : 30,
       // fontSize: 16,
        backgroundColor: 'rgba(255, 255, 255, 0.6)',
        borderBottomColor: 'gray',
        //borderBottomWidth:0.5,
        width: deviceWidth - 90,
        //color: 'gray'
    },
    buttonFacebook: {
        width: deviceWidth - 100,
        height: 50,
        paddingTop: 15,
        // paddingBottom: 5,
        backgroundColor: '#3f50b5',
        borderRadius: 5,
        borderWidth: 1,
        borderColor: '#fff',
    }

})