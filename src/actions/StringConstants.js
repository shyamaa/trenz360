




//Alert Messages

export const APP_DISPLAY_NAME   =   "Buzz360";
export const ACCOUNT_NOT_VERIFIED = "Login successful but user is not active.";
export const LOGIN_ERROR = "Invalid credentials. Please try again.";
export const SIGNUP_ERROR = "Signup not successful. Please try again later.";
export const CHANGEPASSWORD_ERROR = "Change password not successful. Please try again later.";
export const EMAIL_EMPTY_MESSAGE = "Username must be an email and can't be empty";
export const EMAIL_VALIDATION_MESSAGE = "Email is not valid. Please enter valid email address.";
export const FIRST_NAME_EMPTY_MESSAGE = "First Name can't be blank.";
export const LAST_NAME_EMPTY_MESSAGE = "Last Name can't be blank.";
export const MOBILE_EMPTY_MESSAGE = "Mobile can't be blank.";
export const BLANK_EMAIL_ID_MESSAGE = "Email Id can't be blank.";
export const VOTING_ZIP_COde_EMPTY_MESSAGE = "Voting Zip Code can't be blank.";
export const AGE_EMPTY_MESSAGE = "Age can't be blank.";

export const PASSWORD_EMPTY_MESSAGE = "Password can't be empty.";
export const CONFIRM_PASSWORD_EMPTY_MESSAGE = "Confirm password field can't be empty.";
export const NAME_VALIDATION_MESSAGE = "Name field can't be blank. Please enter valid name";
export const PASSWORD_VALIDATION_MESSAGE = "Password length cannot be less than 8 characters.";
export const CONFIRM_CODE_EMPTY_MESSAGE = "Confirm code can't be empty.";
export const CONFIRM_CODE_VALIDATION_MESSAGE = "Confirm code length cannot be less than 6 characters."
export const PASSWORD_FORMAT_VALIDATION_MESSAGE = "Password did not confirm with policy:Password must have lowercase , uppercase & special characters."
export const MOBILE_VALIDATION_MESSAGE = "Mobile number length cannot be less than 10 characters.";
export const CNF_PASSWORD_VALIDATION_MESSAGE = "Password and Confirm Password didn't match. Please enter again.";
export const ISSUE_IN_HEART_RATE_COUNT  =   "Some error occured while fetching heartrate data";
export const ISSUE_IN_STEPS_COUNT  =   "Some error occured while fetching steps data";
export const OLD_PASSWORD_EMPTY =   "Old password can't be less than 8 characters";
export const OLD_PASSWORD_INVALID =   "Old password field can't be empty.";
export const NEW_PASSWORD_INVALID =   "New password can't be less than 8 characters.";
export const NEW_PASSWORD_EMPTY =   "New Password field can't be empty";
export const CONFIRM_EMPTY_FIELD = "Confirm password field can't be empty.";
export const NEW_NOT_EQUAL_TO_OLD_PASSWORD  =   "Old and New password needs to be different.";
export const ISSUE_IN_PROGRAMS_COUNT  =   "Some error occured while fetching programs data";
export const INTERNET_CONNECTION_ISSUE  =   "Connect to the network to Sign in.";
export const NO_INTERNET_MESSAGE    =   "Please check your internet connection";
export const NAME_VALIDATION_DIGIT = "Name field only characters are allowed. Please enter valid name.";
