import { createStore, combineReducers } from 'redux';
import indicationReducer from '../reducers/IndicationReducer';

const rootReducer = combineReducers({
    indicator: indicationReducer,

});

const configureStore = () => {
    return createStore(rootReducer);
}

export default configureStore;