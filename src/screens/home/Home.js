import React, { Component } from 'react';
import {
    Text,
    View,
    TouchableOpacity,
    ImageBackground,
    Dimensions,
    Image,
    ScrollView,
    KeyboardAvoidingView,
    Platform, Button,
    FlatList,
    Linking,
    Share,
    Modal,AsyncStorage
} from 'react-native';
import CardView from 'react-native-cardview';
import LinearGradient from 'react-native-linear-gradient';
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";

import Swiper from 'react-native-deck-swiper';
import { facebookPost } from '../../components/PostUtils'


let deviceWidth = Dimensions.get('window').width;
const paddingValue = Platform.isPad ? true : false;
import logo from '../../imgAssests/logo.png'
import OverlayLabel from '../../components/OverlayLabel';
import cross from '../../imgAssests/cross.png';
import rightArrow from '../../imgAssests/right-arrow-round-red.png';
import axios from 'axios'
import Hyperlink from 'react-native-hyperlink';
import Amplify, { Auth } from 'aws-amplify';
import {networkList,getContacts,allPostLists} from '../../store/actions/BuzzAction'
import DefaultUserPic from '../../imgAssests/DefaultUser.png'
import DemoPost from '../../imgAssests/DemoPost.jpg'


// json file sample

import RecentPostData from '../../constants/RecentPostData';
import UserProfile from '../../constants/UserProfile';
import NetworkData from '../../constants/NetworkData';




import SocialButton from '../../components/SocialButton';

const { width, height } = Dimensions.get('window')



const TAG = "Home => \n";

let TwitterParameters = '';




   
export default class Home extends Component {

    constructor() {
        super();
        this.state = {
            isLoading: false,
            showButton: false,
            email: "",
            password: "",
            recentPostsArray:[],
            membersArray: [],
            activeMembersArray:[],
            isModalVisiable: true,
            isModalShareVisiable: false,
            swiperVisiable: false,
            cards: [1],
            setTitle: null,
            swipeImg:null,
            postTiltle:'',
            postURL: 'https://www.facebook.com/LewisForMN/photos/a.10152982397618472/10157190807993472/?type=3&theater',
            postDescription: '',
            TwitterShareURL: 'https://www.facebook.com/LewisForMN/photos/a.10152982397618472/10157190807993472/?type=3&theater',
            TweetContent: 'Jason Lewis for Congress – US Senate – Campaign found in zip code 55044 & 55024',
        }
        this.getNetwokList()
        this.getContact();
        

    }

    
   

    async componentDidMount() {

      this.getNetwokList();
      this.postList();
      this.getContact();

        let lastOpen = new Date();
       // await AsyncStorage.setItem("lastOpenTime",lastOpen)
       // console.warn(new Date().getTime())
   }

   componentWillReceiveProps(props){
    this.getContact();

   }

   async postList (){

   // const userSession =  await Auth.currentSession();
   // let token = userSession.idToken.jwtToken;
   // console.log("token",token)
    //let postList = await allPostLists(token);
  //  console.log("posttttt",postList)
   // this.setState({recentPostsArray:postList})


   }

   // network lists api's calling

    async getNetwokList(){
        //const userSession =  await Auth.currentSession();
        // let token = userSession.idToken.jwtToken;
        // console.log("token",token)
        // let list = await networkList(token);
        // this.setState({membersArray:list})
    }

    // get contacts api's calling

    async getContact(){
        // const userSession =  await Auth.currentSession();
        // let token = userSession.idToken.jwtToken;
        // console.log("token",token)
        // let list = await getContacts(token);
        // console.log("contact response",list)
        // this.setState({activeMembersArray:list})

    }

    recentPostRowItem(index,title,postDes) {
        this.setState({ swiperVisiable: true,postTiltle:title,postDescription:postDes })
    }

    memberRowItem(item) {
        console.log("itemmsss",item)
        this.props.navigation.navigate("MemberDetails",{data:item})
    }

    activeContactRowItem(index,fName,lName) {
        //alert(" Active Contact " + index)
        this.props.navigation.navigate('UserDetails',{data:{fName,lName}})
    }









    postNotification() {
        this.props.navigation.navigate("PostNotification")
    }

    contactsInfo() {
        this.props.navigation.navigate("ContactsInfo",{data:'Home'});
    }



    renderItemRecentPost = ({ item, index }) => {

        console.warn(item)
       
        return (
          
            <View style={{ flex: 1, height:220, width:width*0.6, marginLeft: 10, marginRight: 2, marginTop: 15 }}>

                <CardView
                    cardElevation={2}
                    cardMaxElevation={2}
                    cornerRadius={20}
                    style={{
                        flex: 1,
                        alignSelf: 'center',
                        backgroundColor: '#FFFFFF',
                        marginLeft: 10,
                        marginRight: 10,
                        marginTop: 1,
                        marginBottom: 1,
                        
                    }}
                >
                    <TouchableOpacity onPress={() => this.recentPostRowItem(index,item.title,item.message)}>
                        <Image
                            style={{ borderTopLeftRadius: 20, borderTopRightRadius: 20, width:width*0.6, height: 130, paddingBottom: 20, marginLeft: 0, }}
                            source={DemoPost}
                        />
                    </TouchableOpacity>
                    <Text style={{padding:10,fontSize:12,fontWeight:'500',width:width*0.6}}>
                      {item.title}
                     </Text>
                </CardView>
            </View>
         
        )
    }
    renderItemMember = ({ item, index }) => {
        return (
            <TouchableOpacity style={{ flex: 1 }} onPress={() => this.memberRowItem(item)}>
                <View style={styles.memberCardView}>

                    <View style={{ flex: 1, marginTop:10, }}>
                        <Image
                            style={{ width:100, height:50, alignSelf: 'center',resizeMode:'contain'}}
                            //source={item.imgPath}
                            source={require('../../imgAssests/member4.png')}
                        />
                        <Text style={{fontSize:13,paddingHorizontal:5,fontWeight:'500'}}>{item.name.substring(0,25)}..</Text>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }

    renderItemActiveMember = ({ item, index }) => {
       // let dimension = 60;
        let dotDimension = 10;
       // console.log(TAG + "renderItemMemder => " + JSON.stringify(item));
        let fName = item.firstName || false;
        let lName = item.lastName || false;
        let thumbNailName = (fName ? fName.charAt(0) : '') + (lName ? lName.charAt(0) : '');
        let dimension = 60;
        return (
            <TouchableOpacity style={{ flex: 1 }} onPress={() => this.activeContactRowItem(index,fName,lName)}>

                <View style={{ flex: 1, marginLeft: 20 }}>

                    <View style={{ flex: 1 }}>


                        {/* <ImageBackground
                            style={{
                                height: 60, width: 60, borderRadius: 60 / 2,
                                justifyContent: 'flex-start', flexDirection: 'row-reverse'
                            }}
                            imageStyle={{ borderRadius: 75 / 2 }}
                            resizeMode="cover"
                            source={DefaultUserPic}>

                            <View style={{ height: dotDimension, width: dotDimension, borderRadius: dotDimension / 2, backgroundColor: 'green',top:10 }} />
                        </ImageBackground> */}
                         {/* first view */}
                      <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                        {item.hasThumbnail ? <Image
                            style={{ width: dimension, height: dimension, alignSelf: 'center', borderRadius: dimension / 2 }}
                            source={{ uri: item.thumbnailPath }}
                        /> : <View style={{ height: dimension, width: dimension, borderRadius: dimension / 2, backgroundColor: "#ccc", justifyContent: 'center', alignItems: 'center' }}><Text style={{ fontSize: 18, fontWeight: '800', color: '#fff' }}>{thumbNailName}</Text></View>}
                    </View>

                    </View>

                    <Text style={{
                        flex: 1,
                        alignSelf: 'center',
                        paddingHorizontal: 5,
                        paddingVertical: 10,
                        fontSize: 13,
                    }}>{item.firstName}</Text>
                </View>
            </TouchableOpacity>
        )
    }

    renderCard = () => {
       
        return (

            <View style={styles.cardContainer}>
                
                <View style={{ flex: 1, backgroundColor: 'white', alignItems: 'center', padding: 0, borderRadius: 25, }}>
                    <Image source={DemoPost}
                        style={{ height:height*0.25, width: width - 30, borderTopRightRadius: 20, borderTopLeftRadius: 20 }} />
                    <ScrollView style={{flex:1}} persistentScrollbar={true}>   
                    <Text style={{ color: '#2F3979', fontWeight: '800', fontSize:RFValue(18), textAlign: 'center', marginTop: 10, width: 300, }}>
                    {this.state.postTiltle} </Text>
                    
                    <Hyperlink linkDefault={ true } linkStyle={ {textDecorationLine:'underline' } }>
                    <Text style={{
                        color: 'grey', flexWrap: 'wrap', flex: 1,
                        fontSize:RFValue(14), textAlign: 'center', top:12, width: 300, 
                    }}>{this.state.postDescription}</Text>
                    </Hyperlink>
                    </ScrollView> 

                </View>
   

                <View style={{
                    backgroundColor: '#2F3979', borderBottomEndRadius: 10,
                    borderBottomStartRadius: 10, height: 80, alignItems: 'center', flexDirection: 'row', justifyContent: 'space-evenly'
                }}>

                    <TouchableOpacity style={{ bottom: 20 }} onPress={()=>this.swiper.swipeLeft()}>
                        <Image source={cross} style={styles.imgStyle} />
                    </TouchableOpacity>
                    <TouchableOpacity style={{ bottom: 20 }} onPress={()=>this.swiper.swipeRight()}>
                        <Image source={rightArrow} style={styles.imgStyle} />
                    </TouchableOpacity>

                </View>

            </View>



        )

    }

    onSwipeRight = () => {

        this.setState({ isModalShareVisiable: true, swiperVisiable: false },
            // this.refs.modal1.open()
        )
    }
   

   
    onSelect = (title) => {
        this.setState({ setTitle: title })


    }

    sharePost = () => {

        switch (this.state.setTitle) {
            case 'Facebook':
                this.postOnFacebook();
                break;
            case 'Instagram':
                this.onShare();
                break;
            case 'Twitter':
                this.postOnTwitter();
                break;
            case 'Snapchat':
                this.onShare();
                break;
            default: break;
        }
    }

    postOnFacebook = () => {

        let { postURL, postDescription } = this.state;

        let url = 'https://www.facebook.com/sharer/sharer.php' + facebookPost(postURL, postDescription);

        Linking.openURL(url).then((data) => {
            console.log('Facebook Opened');
        }).catch(() => {
            console.log('Something went wrong');
        });
    }

    postOnTwitter = () => {

        if (this.state.TwitterShareURL != undefined) {
            if (TwitterParameters.includes('?') == false) {
                TwitterParameters =
                    TwitterParameters + '?url=' + encodeURI(this.state.TwitterShareURL);
            } else {
                TwitterParameters =
                    TwitterParameters + '&url=' + encodeURI(this.state.TwitterShareURL);
            }
        }
        if (this.state.postDescription != undefined) {
            if (TwitterParameters.includes('?') == false) {
                TwitterParameters =
                    TwitterParameters + '?text=' + encodeURI(this.state.postDescription);
            } else {
                TwitterParameters =
                    TwitterParameters + '&text=' + encodeURI(this.state.postDescription);
            }
        }

        let url = 'https://twitter.com/intent/tweet' + TwitterParameters;
        Linking.openURL(url)
            .then(data => {
                console('Twitter Opened');
            })
            .catch(() => {
                console.log("error")
            });
    };

    onShare = () => {
       
        let shareOptions = {
            title: 'SwipeRed',
            message:this.state.postDescription,
            url:this.state.postURL,
        };

        Share.share(shareOptions);
    }

    render() {
        let { swiperVisiable } = this.state;

        return (
            <KeyboardAvoidingView
                style={{ flex: 1, justifyContent: 'center' }}
                behavior={Platform.OS === 'ios' ? 'padding' : null}>

                <ScrollView
                    style={{ flex: 1, backgroundColor: 'white' }} showsVerticalScrollIndicator={false} keyboardShouldPersistTaps={'handled'}>

                    <View style={{ flex: 1 }}>

                        <LinearGradient colors={['#085d87', '#27c7bb']} style={styles.mainContainer}>

                            <View style={styles.container}>
                                <View style={styles.swipeRed}>
                                    <Image
                                        style={{ height:75, width: 300, right: 75 }}
                                        resizeMode="contain"
                                        source={logo} />
                                </View>
                                <TouchableOpacity style={[{ paddingHorizontal: 1, justifyContent: 'center' }]} onPress={() => { this.postNotification() }}>
                                    <Image
                                        style={{ height: 25, width: 30, }}
                                        resizeMode="contain"
                                        source={require('../../imgAssests/notification.png')} />
                                </TouchableOpacity>
                            </View>

                            <View style={styles.recentPostView}>
                                <Text style={{ fontSize: 16, color: 'white', fontWeight: '400', right: 15 }}>Post Now</Text>
                            </View>
                        </LinearGradient>

                        <View style={{ flex: 1 }}>

                            <FlatList
                                style={styles.flatListRecentPost}
                                data={RecentPostData}
                                horizontal={true}
                                showsHorizontalScrollIndicator={false}
                                automaticallyAdjustContentInsets={false}
                                removeClippedSubviews={false}
                                enableEmptySections={false}
                                renderItem={this.renderItemRecentPost}
                            />

                            <View style={styles.activeContactsView}>
                                <Text style={styles.activeContactsText}>Member of</Text>
                            </View>

                            <FlatList
                                style={styles.flatListMember}
                                data={NetworkData}
                                horizontal={true}
                                showsHorizontalScrollIndicator={false}
                                automaticallyAdjustContentInsets={false}
                                removeClippedSubviews={false}
                                enableEmptySections={false}
                                renderItem={this.renderItemMember}
                            />

                            <View style={styles.activeContactsView}>
                                <Text style={styles.activeContactsText}>Active Contacts</Text>
                            </View>


                            <FlatList
                                style={styles.flatListActiveMember}
                                data={UserProfile}
                                horizontal={true}
                                showsHorizontalScrollIndicator={false}
                                automaticallyAdjustContentInsets={false}
                                removeClippedSubviews={false}
                                enableEmptySections={false}
                                renderItem={this.renderItemActiveMember}
                            />

                            <TouchableOpacity style={styles.contacts}
                                onPress={() => this.contactsInfo()}>
                                <View style={styles.contactsLink} >
                                    <Text
                                        underlineColorAndroid='black'
                                        style={styles.contactsText}>Connect your contacts</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>

                </ScrollView>

                {swiperVisiable &&
                    <Swiper
                        ref={swiper => {
                            this.swiper = swiper
                        }}
                        verticalSwipe={false}
                        cards={this.state.cards}
                        renderCard={this.renderCard}
                        stackSize={3}
                        stackSeparation={15}
                        animateOverlayLabelsOpacity
                        onSwipedRight={() => this.onSwipeRight()}
                        onSwipedLeft={() => this.setState({ swiperVisiable: false })}
                        animateCardOpacity
                        showSecondCard={false}
                        onTapCardDeadZone={1}
                        backgroundColor={'#00000070'}
                        overlayLabels={{
                            left: {
                                title: 'Close',
                                element: <OverlayLabel label="left" color="grey" direction={"left"} isClose={true} />,
                                style: {
                                    label: {
                                        backgroundColor: 'black',
                                        borderColor: 'black',
                                        color: 'white',
                                        borderWidth: 1
                                    },
                                    wrapper: {
                                        flexDirection: 'column',
                                        alignItems: 'center',
                                        justifyContent: 'center',
                                        marginTop: 30,
                                        marginLeft: -30
                                    }
                                }
                            },
                            right: {
                                title: 'Forward',
                                element: <OverlayLabel label="right" color="grey" direction={"right"} />,
                                style: {
                                    label: {
                                        backgroundColor: 'black',
                                        borderColor: 'black',
                                        color: 'white',
                                        borderWidth: 1
                                    },
                                    wrapper: {
                                        flexDirection: 'column',
                                        alignItems: 'center',
                                        justifyContent: 'center',
                                        marginTop: 30,
                                        marginLeft: 30
                                    }
                                }
                            },
                        }}
                        containerStyle={{
                            justifyContent: 'center', top: 0, backgroundColor: '#00000070'
                        }} />
                }

                {/* app guide modal open */}

                <Modal animationType={"slide"} transparent={true} visible={this.state.isModalVisiable}
                    onRequestClose={() => { console.log("Modal has been closed.") }}>

                    <ImageBackground source={require('../../imgAssests/Welcome.png')} style={styles.modalView}>

                        <TouchableOpacity style={styles.modalButtonView} onPress={() => this.setState({ isModalVisiable: false })}
                            underlayColor='#fff'>
                            <Text style={styles.modalButtonTitleStyle}>Got It</Text>
                        </TouchableOpacity>

                    </ImageBackground>


                </Modal>

                {/* modal view for share post */}


                <Modal visible={this.state.isModalShareVisiable} backdrop={false} transparent={true} style={{ flex: 1, backdropColor: '#999999', backdropOpacity: 0.5 }} >

                    <View style={styles.modalContainer}>

                        <View style={styles.mainViewContainer}>

                            <Text onPress={() => this.setState({ isModalShareVisiable: false,setTitle:null })} style={{ padding: 20, color: 'black' }}>X</Text>

                            <View style={{ alignItems: 'center', padding:0 }}>
                                <Text style={styles.infoTextStyle}>Where would you like to {'\n'} share this post ? </Text>
                                <View style={{ flex: 1, padding: 30, justifyContent: 'space-between' }}>

                                    <SocialButton title={'Facebook'} bgColor={'blue'} setTitle={this.state.setTitle}
                                        onpress={() => this.onSelect('Facebook')}
                                        isCheck={true} titleColor={'#FFF'} />
                                    <View style={{ height: 10 }} />

                                    <SocialButton title={'Instagram'} bgColor={'red'} onpress={() => this.onSelect('Instagram')}
                                        setTitle={this.state.setTitle}
                                        titleColor={'#FFF'} />
                                    <View style={{ height: 10 }} /> 
                                    <SocialButton title={'Snapchat'} bgColor={'yellow'} setTitle={this.state.setTitle}
                                        onpress={() => this.onSelect('Snapchat')} titleColor={'#000'} />
                                    <View style={{ height: 10 }} />
                                    <SocialButton title={'Twitter'} bgColor={'#0077be'} setTitle={this.state.setTitle}
                                        onpress={() => this.onSelect('Twitter')} titleColor={'#FFF'} />
                                    <View style={{ height: 20 }} />
                                    <TouchableOpacity
                                        disabled={this.state.setTitle !== null ? false : true}
                                        style={[styles.shareBtnStyle, { backgroundColor: this.state.setTitle !== null ? 'blue' : '#D3D3D3' }]}
                                        onPress={() => this.sharePost()} >
                                        <Text style={styles.shareBtnTextStyles}>Share</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>



                        </View>

                    </View>

                </Modal>





            </KeyboardAvoidingView>
        );
    }
}
const styles = {
    mainContainer: {
        backgroundColor: '#FF0000',
        paddingHorizontal: paddingValue ? 120 : 30,
        paddingTop: Platform.OS === 'android' ? 40 : 50,
        paddingBottom: Platform.OS === 'android' ? 10 : 50
    },
    container: {
        flex: 1, flexDirection: 'row',
        marginTop:10,
        justifyContent: 'space-between',
    },
    swipeRed: {
        flex: 1,
        alignItems: 'flex-start',
        justifyContent: 'flex-start',
        paddingVertical: 5,
    },
    swipeRedText: {
        fontSize: 28, color: 'white', fontWeight: '600',
    },
    flatListRecentPost: {
        flex: 1, marginTop: Platform.OS === 'android' ? -40 : -80, minHeight: 200, minWidth: 250, marginBottom: 5,
    },
    flatListMember: {
        flex: 1, minHeight: 90, minWidth: 250, backgroundColor: "#FFFFFF", marginTop: Platform.OS === 'android' ? 5 : 20,
    },
    flatListActiveMember: {
        flex: 1, minHeight: 50, minWidth: 30, 
        backgroundColor: "#FFFFFF",
         marginTop: Platform.OS === 'android' ? 10 : 20, marginLeft: 10
    },
    contacts: {
        paddingVertical: 5, marginLeft: Platform.OS === 'android' ? 20 : 0,
        marginBottom: Platform.OS === 'android' ? 20 : 10,
        alignItems: 'flex-start', justifyContent: 'flex-start'
    },
    contactsLink: {
        flex: 1,
        marginLeft: 15
    },
    contactsText: {
        fontSize: 17, color: 'grey', fontWeight: '600', textDecorationLine: 'underline',
    },

    activeContactsView: {
        paddingVertical: 5,
        marginLeft: 20,
        marginTop: 40,
        alignItems: 'flex-start', justifyContent: 'flex-start'
    },
    activeContactsText: {
        fontSize: 16, color: 'black', fontStyle: "normal", fontWeight: 'bold', fontWeight: '600'
    },
    recentPostView: {
        marginTop: 20,
        marginBottom: 20
    },
    modalView: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    modalButtonView: {
        width: deviceWidth - 100,
        height: 50,
        paddingTop: 15,
        paddingBottom: 5,
        backgroundColor: 'red',
        borderRadius: 5,
        top: 10
    },
    modalButtonTitleStyle: {
        color: '#fff', textAlign: 'center', paddingLeft: 10, paddingRight: 10

    },
    cardContainer: {
        width: width - 30,
        height:height>700 ? 550 :450,
        backgroundColor: '#FFF',
        alignSelf: 'center',
        shadowColor: '#000',
        borderRadius: 50,
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.5,
        shadowRadius: 2,
        elevation: 2,
        top:height*0.1
    },
    imgStyle: {
        width: 120,
        height: 120, resizeMode: 'contain'
    },
    modalContainer: {
        justifyContent: 'center', alignItems: 'center',
        height: height, width: width, borderRadius: 20,
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.5,
        shadowRadius: 2,
        elevation: 2,
        backgroundColor: '#00000070',
     },
    mainViewContainer: {
        backgroundColor: 'white',
        height:450 , width: width - 50, borderRadius: 20
    },
    infoTextStyle: {
        color: '#004772', fontWeight: '500',
        fontSize: 18, textAlign: 'center'
    },
    shareBtnStyle: {
        width: width - 70, height: 50, borderRadius: 10, justifyContent: 'center'
    },
    shareBtnTextStyles: {
        color: '#FFF',
        fontWeight: '500', fontSize: 14, textAlign: 'center'
    },
    memberCardView:{
        flex: 1, backgroundColor: '#F6F6F7', borderRadius:20,alignItems:'center',justifyContent:'center',
        height:100, width:200, marginLeft: 10

    }

}