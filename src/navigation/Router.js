import React from "react";
import { Image, Platform, AsyncStorage,View } from 'react-native';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import Login from '../screens/auth/Login';
import ResetPassword from '../screens/auth/ResetPassword';
import Forgot from '../screens/auth/Forgot';
import Signup from '../screens/auth/Signup';
import SignupStarted from '../screens/auth/SignupStarted';
import AddPhoto from '../screens/auth/AddPhoto';
import DynamicTab from '../screens/DynamicTab';
import Settings from '../screens/settings/Settings';
import Home from '../screens/home/Home';
import ContactsInfo from '../screens/contact/ContactsInfo';
import TermAndPrivacy from '../screens/settings/TermAndPrivacy';
import Messages from '../screens/message/Messages';
import MessageInfoList from '../screens/message/MessageInfoList';
import ContactTabs from '../screens/contact/ContactTabs';
import UserGuide from '../screens/UserGuide';
import MyAccount from '../screens/settings/MyAccount';
import InitialLoadingScreen from '../screens/InitialLoadingScreen';
import ReserLinkInfo from '../screens/auth/ResetLinkInfo';
import PostNotifications from "../screens/home/PostNotifications";
import MemberDetails from "../screens/home/MemberDetails";
import UserDetails from "../screens/home/UserDetails";
import Onboarding  from "../screens/Onboarding";






const paddingValue = Platform.isPad ? true : false;

//TODO Bottom tab
const TabNavigator = createBottomTabNavigator({
    Home: {
        screen: Home,
        navigationOptions: {
            tabBarLabel: "Home",
            tabBarIcon: ({ tintColor, focused }) => {
                return (<Image
                    style={{ width: 20, height: 20 }}
                    source={focused ? require('../imgAssests/home-red.png') : require('../imgAssests/home-grey.png')} />);
            }
        },
    },
    Message: {
        screen: Messages,
        navigationOptions: {
            tabBarLabel: 'Message',
            tabBarIcon: ({ tintColor, focused }) => {
                return (<Image
                    style={{ width: 25, height: 25 }}
                    source={focused ? require('../imgAssests/message-red.png') : require('../imgAssests/message-grey.png')} />);
            }
        }
    },
    Contacts: {
        screen: ContactTabs,
        navigationOptions: {
            tabBarLabel: "Contacts",
            tabBarIcon: ({ tintColor, focused }) => {
                return (<Image
                    style={{ width: 20, height: 20 }}
                    source={focused ? require('../imgAssests/contacts-red.png') : require('../imgAssests/contacts-grey.png')} />);
            }
        },
    },
    Setting: {
        screen: Settings, //Calling Screen2 Stack.
        navigationOptions: {
            tabBarLabel: 'Setting',
            tabBarIcon: ({ tintColor, focused }) => {
                return (
                  <Image
                    style={{ width: 20, height: 20 }}
                    source={focused ? require('../imgAssests/profile-red.png') : require('../imgAssests/profile-grey.png')} />
         );
            }
        }
    },

},
    {
        tabBarPosition: 'bottom',
        tabBarOptions: {
            inactiveTintColor: 'black',
            activeTintColor: 'white',
            swipeEnabled: false,
            showLabel: true,
            labelStyle: {
                marginTop: paddingValue ? 0 : 3,
                marginBottom: paddingValue ? 0 : -5
            },
            showIcon: true,
            style: {
                backgroundColor: '#2E86C1',
                paddingVertical: 10,
                height: 44,
            },

            indicatorStyle: {
                backgroundColor: 'white',
               
            },

        },
    });




export const AppStackNavigator = {

    Onboard:{
        screen: Onboarding,
        navigationOptions: {
            headerShown: false

        }
    },
   Initial: {
        screen: InitialLoadingScreen,
        navigationOptions: {
            headerShown: false

        }

    },

    Login: {
        screen: Login,
        navigationOptions: {
            headerShown: false,
            gesturesEnabled: false

        }
    },
    Forgot: {
        screen: Forgot,
        navigationOptions: {
            headerShown: false
        }
    },
    Signup: {
        screen: Signup,
        navigationOptions: {
            headerShown: false
        }
    },
    SignupStarted: {
        screen: SignupStarted,
        navigationOptions: {
            headerShown: false
        }
    },
    AddPhoto: {
        screen: AddPhoto,
        navigationOptions: {
            headerShown: false
        }
    },

    DynamicTab: {
        screen: DynamicTab,
        navigationOptions: {
            headerShown: false
        }
    },

    ContactsInfo: {
        screen: ContactsInfo,
        navigationOptions: {
            headerShown: false
        }
    },
    ResetPassword: {
        screen: ResetPassword,
        navigationOptions: {
            headerShown: false
        }
    },
    ResetLink: {
        screen: ReserLinkInfo,
        navigationOptions: {
            headerShown: false
        }
    },
    MemberDetails: {
        screen: MemberDetails,
        navigationOptions: {
            headerShown: false
        }

    },

    TermAndPrivacy: {


        screen: TermAndPrivacy,
        navigationOptions: {
            headerShown: false,
       }

        

    },

    MessageDetails: {
        screen: MessageInfoList,
        navigationOptions: {
            headerShown: false
        }

    },
    ContactTabs: {
        screen: ContactTabs,
        navigationOptions: {
            headerShown: false
        }
    },

    UserGuide: {
        screen: UserGuide,
        navigationOptions: {
            headerShown: false
        }
    },
    PostNotification: {
        screen: PostNotifications,
        navigationOptions: {
            headerShown: false
        }
    },
    MyAccount: {

        screen: MyAccount,


        navigationOptions: {

            headerShown: false,
       }

    },
    UserDetails: {
        screen: UserDetails,
        navigationOptions: {
            headerShown: false
        }

    },


    tabs: {
        screen: TabNavigator, //You can add the NavigationOption here with navigation as parameter using destructuring.
        navigationOptions: {
            headerShown: false,
            gesturesEnabled: false
        }
    },


};



export const appContainer = (isUserLogIn = false) => {
    return createAppContainer(createStackNavigator(
        AppStackNavigator,
        {
            headerMode: "screen",
            initialRouteName: isUserLogIn ? "tabs" : "tabs",

        }
    ))

}
