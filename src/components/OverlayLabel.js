import React from 'react'
import {Image } from 'react-native'
import Modal from "react-native-modal";
import rightArrow from '../imgAssests/right-arrow-round-red.png';
import leftArrow from '../imgAssests/left-arrow-round-grey.png';
import cross from '../imgAssests/cross.png'


const OverlayLabel = ({ label, color, direction, onpress,isClose }) => (

  <Modal isVisible={true} style={{ alignItems: 'center' }} animationIn={'fadeIn'}>
       {isClose ?
      <Image source={direction == "left" ? cross : rightArrow} style={styles.imgStyle} /> :
      <Image source={direction == "left" ? leftArrow : rightArrow} style={styles.imgStyle} />}
      
  </Modal>


)

const styles = {

  imgStyle: {
    height: 200, width: 200,
    resizeMode: 'contain'
  }
}

export default OverlayLabel