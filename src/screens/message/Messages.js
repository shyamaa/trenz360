import React, { Component } from 'react';
import { Text, View, TouchableOpacity, TextInput, Image, Platform, StyleSheet,Dimensions } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import ScrollableTabView, { ScrollableTabBar } from 'react-native-scrollable-tab-view';
import search from '../../imgAssests/search.png';
import notification from '../../imgAssests/notification.png'
import MessageCard from '../../components/MessageCard';
import userProfile from '../../constants/UserProfile';
import {networkList,networkContactList} from '../../store/actions/BuzzAction'
import axios from 'axios'
import Amplify, { Auth } from 'aws-amplify';
import Loader from '../../components/Loader';

const {width,height} = Dimensions.get('window')


const paddingValue = Platform.isPad ? true : false;






export default class Message extends Component {

    constructor() {
        super();
        this.state = {
            isLoading: false,     // true
            searchedText: "",
            users: userProfile,
            //users: [1,2],
            dupUser:userProfile,  //userProfile
            noDate: true,
            //membersArray:[],
            membersArray:[{'name':'Bharatiya Janata Party'},{'name':'Indian National Congress'},{'name':'Aam Aadmi Party'},{'name':'YSR Congress Party'}],

            //embersArray:[{'name':'MCCL MN'},{'name':'Jason Lewis'}]
        }
    }

    async componentDidMount() {
       // this.getNetwokList()
    }

    componentWillReceiveProps(props){
        console.log("props of contact will",props.navigation.state.params.data)
        props.navigation.state.params.data ? this.setState({users:userProfile,dupUser:userProfile}) : null
    }

    
   // network lists api's calling

   async getNetwokList(){
    const userSession =  await Auth.currentSession();
    let token = userSession.idToken.jwtToken;
    let list = await networkList(token);
    //this.setState({membersArray:list,isLoading:false})
   }

   


    searchClick() {
        alert("search Message");
    }

    navigateToMesssageDeatils() {
        this.props.navigation.navigate("MessageDetails",{membersArray:this.state.membersArray});
    }


    onSearchTextChange = (e) => {

        this.setState({searchedText:e})

    }

    renderNetworkCard=()=>{
        return(
        this.state.membersArray.map((data)=>{
            return(
                <MessageCard tabLabel={data.name} search={this.state.searchedText}
                userList={this.state.users} 
                 navigation={this.props.navigation} networkId={data.networkId} />
            )
        })
      ) 

       
    
            
      
    }






    render() {
        let {isLoading} = this.state;
        return (
            <View style={{ flex: 1,backgroundColor:'#FFF' }}>

                {/* header view */}

                <LinearGradient colors={['#085d87', '#27c7bb']} style={{ height:height * 0.25 }}>
                    <View style={styles.headerView}>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between',marginTop:20 }}>
                            <View style={styles.headerTitleView}>
                                <Text style={styles.headerTitleStyles}>Messages</Text>
                            </View>
                            <TouchableOpacity style={[{ paddingHorizontal: 1, justifyContent: 'center' }]} onPress={() => {
                                this.navigateToMesssageDeatils()
                            }}>
                                <Image
                                    style={{ height:20, width: 30,marginBottom:20 }}
                                    resizeMode="contain"
                                    source={notification}>
                                </Image>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.inputContainer}>

                            <TextInput
                                style={{ flex: 1, fontSize: 14, marginHorizontal: 15,fontWeight:'200' }}
                                autoCapitalize='none'
                                placeholder="Search Messages"
                                placeholderTextColor="gray"
                                underlineColorAndroid="transparent"
                                onChangeText={this.onSearchTextChange.bind(this)}
                            />
                            <TouchableOpacity style={styles.searchView} onPress={() => this.searchClick()}>
                                <Image source={search} style={styles.searchIconStyle} />
                            </TouchableOpacity>


                        </View>
                    </View>

                </LinearGradient>

                {/* main view container */}

                <ScrollableTabView
                    initialPage={0}
                    tabBarInactiveTextColor={'#5D5D5D'}
                    tabBarUnderlineStyle={{ backgroundColor: '#BB281C' }}
                    tabBarActiveTextColor={'#BB281C'}
                    renderTabBar={() => <ScrollableTabBar />}>

                  {this.renderNetworkCard()}
                    

                </ScrollableTabView>

                {isLoading && <Loader/>}


            </View>
        );
    }
}

const styles = StyleSheet.create({

    headerView: {
        paddingHorizontal: paddingValue ? 120 : 20, 
        paddingTop:height >700 ? 30 :10, 
        //paddingBottom: Platform.OS === 'android' ? 50 : 50
    },
    headerTitleView: {
        paddingVertical: 5, marginBottom: Platform.OS === 'android' ? 10 : 20
    },
    headerTitleStyles: {
        fontSize: 26, color: 'white', fontWeight: '600'
    },
    inputContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
        height: 50,
        borderRadius: 5,
    },
    searchView: {
        paddingLeft: 20, paddingTop: 5,
    },
    searchIconStyle: {
        margin: 15,
        height: 20,
        width: 20,
        bottom:2,
        resizeMode: 'stretch',
        alignItems: 'center'
    },

});
