import React, { Component } from 'react';
import {
    AppRegistry,
    Text,
    View,
    TouchableOpacity,
    ImageBackground,
    Dimensions,
    TextInput,
    Image,
    Alert,
    ActivityIndicator,
    ScrollView,
    KeyboardAvoidingView,
    Platform, Button,
    TouchableHighlight,
    StyleSheet,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import logo from '../../imgAssests/logo.png';
import loginarrow from '../../imgAssests/login-arrow.png'

import {
    APP_DISPLAY_NAME,
    EMAIL_EMPTY_MESSAGE,
    EMAIL_VALIDATION_MESSAGE, INTERNET_CONNECTION_ISSUE,
    LOGIN_ERROR,
    PASSWORD_EMPTY_MESSAGE, PASSWORD_VALIDATION_MESSAGE,
} from '../../actions/StringConstants';

import { showInfoAlert } from "../../utils/AlertPopUp";
import { validEmail } from "../../utils/Validations";


let deviceWidth = Dimensions.get('window').width;
let deviceHeight = Dimensions.get('window').height;



const TAG = "SignIn Screen => \n";

export default class ReserLinkInfo extends Component {

    constructor() {
        super();
        this.state = {
            isLoading: false,
            showButton: false,
            email: "",
            otp: "",
            password: "",
            confirmPassword: "",
        }
    }

    componentDidMount() {
    }

    async onOtpTextChange(text) {
        await this.setState({ otp: text });

        let userPassword = this.state.password;
        let userConfirmPassword = this.state.confirmPassword;
        let userOTP = this.state.otp;

        if (userOTP.length > 0
            && userPassword.length > 0 && userConfirmPassword.length > 0) {
            this.setState({ showButton: true });
        } else {
            this.setState({ showButton: false });
        }


    }

    async onPasswordTextChange(text) {
        await this.setState({ password: text });


        let userPassword = this.state.password;
        let userConfirmPassword = this.state.confirmPassword;
        let userOTP = this.state.otp;

        if (userOTP.length > 0
            && userPassword.length > 0 && userConfirmPassword.length > 0) {
            this.setState({ showButton: true });
        } else {
            this.setState({ showButton: false });
        }

    }

    async onConfirmPasswordTextChange(text) {
        await this.setState({ confirmPassword: text });

        let userPassword = this.state.password;
        let userConfirmPassword = this.state.confirmPassword;
        let userOTP = this.state.otp;

        if (userOTP.length > 0
            && userPassword.length > 0 && userConfirmPassword.length > 0) {
            this.setState({ showButton: true });
        } else {
            this.setState({ showButton: false });
        }

    }

    submitClicked() {


        let userEmail = this.state.email;
        let userPassword = this.state.password;

        if (userEmail.length <= 0) {
            showInfoAlert(EMAIL_EMPTY_MESSAGE)
            return;
        }

        if (validEmail(userEmail)) {
            showInfoAlert(EMAIL_VALIDATION_MESSAGE)
            return;
        }

        if (userPassword.length <= 0) {
            showInfoAlert(PASSWORD_EMPTY_MESSAGE)
            return;
        }


        if (userPassword.length < 8) {
            showInfoAlert(PASSWORD_VALIDATION_MESSAGE)
            return;
        }

        else {
            //this.setState({ isLoading: true });
            //   alert("Hit API")
            

            this.props.navigation.navigate("RecentPost");

        }

    }

    loginClicked() {
        this.props.navigation.navigate("Login");
    }

    render() {
        return (

           

                <LinearGradient colors={['#FF0000', '#A92217']} style={{ flex: 1,justifyContent:'space-between' }}>
                  
                   

                  
                        <View style={{ flex: 1, height: 'auto' }}>

                            {/*header view */}
                            <View style={{ marginTop:deviceHeight>700 ? 50 :20, alignItems: 'center' }}>
                                <Image
                                    style={{ height:75, width: 300,marginTop:30 }}
                                    resizeMode="contain"
                                    source={logo} />
                            </View>

                            {/*main view container */}
                            <View style={{ flex: 1, alignSelf: 'center', justifyContent: 'space-around', height: 'auto' }}>


                                <View style={{
                                    width: deviceWidth - 50, padding: 10, borderRadius: 10,
                                    backgroundColor: 'white', alignItems: 'center', marginTop: '15%'
                                }}>

                                    <Text style={{  color: '#3f50b5', fontSize: 25, fontWeight: '800', marginTop: 15 }}>Reset Link Sent!</Text>



                               

                                    {/*button view container*/}

                                    <View style={{
                                        marginTop:30, backgroundColor: '#fff', width: deviceWidth - 50,
                                        alignItems: 'center', borderRadius: 10}}>
                                        <Text style={{textAlign:'center',color: '#3f50b5',fontSize:15}}>
                                            Please click the link in your email and {'\n'} it will bring you back to the app to {'\n'} reset your password.
                                        </Text>       

                                       

                                        <View style={{ width: deviceWidth - 50, height:30, borderRadius: 30 }}></View>


                                    </View>




                                </View>

                                <TouchableOpacity style={{ marginTop: 100, marginBottom:10, alignSelf: 'center',flexDirection: 'row' }} onPress={() => this.loginClicked()}>
                                    <Text style={{ fontSize:15, color: '#fff', fontWeight: '800', }}>Login</Text>
                                    <Image source={loginarrow} style={{ height: 20, width: 20, resizeMode: 'contain', left: 10, top: 2 }} />

                                </TouchableOpacity>

                            </View>

                        </View>


        
                 


                </LinearGradient>

         
        )
    }
}


const styles = StyleSheet.create({

    textInput: {
        height: Platform.OS === 'ios' ? 40 : 45,
        marginTop: Platform.OS === 'android' ? 20 : 30,
        fontSize: 16,
        backgroundColor: 'rgba(255, 255, 255, 0.6)',
        //paddingHorizontal: 10,
        borderBottomWidth: 1,
        borderBottomColor: 'gray',
        width: deviceWidth - 90,
        color: 'gray'
    },
    buttonFacebook: {
        width: deviceWidth - 100,
        height: 50,
        paddingTop: 15,
        // paddingBottom: 5,
        backgroundColor: '#3f50b5',
        borderRadius: 5,
        borderWidth: 1,
        borderColor: '#fff',
    }

})