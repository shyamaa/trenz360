// API URLS
/*export const AUTH_BASE_URL = "http://35.180.128.167/NIBRS/";  // For authentication, login & logout
export const LOGIN_API = "api/Login";
export const LOGOUT_API = "api/Logoff";

export const APP_URL = "http://15.188.43.43/api/api/";
export const PREVIOUS_ALERTS_API = "Alert/GetPreviousAlerts?";
export const SAVE_COMMENT_API = "Alert/SaveAlertComments/";
export const GET_COMMENTS_API = "Alert/GetAlertComments?";
export const DELETE_COMMENT_API = "Alert/DiscardAlert";*/




// MESSAGES
export const EMPTY_EMAIL = "Please enter email";
export const VALID_EMAIL = "Please enter valid email";
export const EMPTY_PASSWORD = "Please enter password";
export const INTERNET_CONNECTION_ISSUE = "Connect to the network.";
export const SOME_ERROR_OCCURRED = "Some error occurred, please try again later.";
export const EMPTY_IP_ADDRESS = "Please enter IP address";
export const EMPTY_DESCRIPTION = "Please enter description";
export const EMPTY_ACTION_TAKEN = "Please enter action taken";
export const EMPTY_HOURS = "Please enter hours";
export const EMPTY_MINUTES = "Please enter minutes";
