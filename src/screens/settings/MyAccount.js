import React, { Component } from 'react';
import { Text, View, TouchableOpacity, TextInput, ScrollView, KeyboardAvoidingView, Platform, Image, Dimensions, AsyncStorage, Alert } from 'react-native';
import { getOr } from 'lodash/fp';
import backArrow from '../../imgAssests/backArrowGrey.png';
import axios from 'axios';
import Amplify, { Auth } from 'aws-amplify';
import Loader from '../../components/Loader';
import { getUserInfo } from '../../store/actions/BuzzAction';
import { showInfoAlert, showActionAlert } from "../../utils/AlertPopUp";




const TAG = "MyAccount Screen => \n";
const { width, height } = Dimensions.get('window')

export default class MyAccount extends Component {

    constructor(props) {
        super(props);
        console.log("propssss account", props.navigation.state.params)
        this.state = {
            isLoading: false,
            showButton: false,
            firstName: props.navigation.state.params.firstName,
            lastName: props.navigation.state.params.lastName,
            mobile: props.navigation.state.params.phone,
            email: props.navigation.state.params.email,
            facebookConnection: props.navigation.state.params.email,
            votingZipCode: props.navigation.state.params.postal,
            password: "******",
            editableFName: false,
            editableLName: false,
            editableMobile: false,
            editableVoting: false,
            editablePassword: false,
            userData: getOr([], 'userInfo', props.navigation.state.params)
        }
        this.getUserInfo();
    }



    firstNameEdit() {
        this.setState({
            editableFName: true,
            editableLName: false,
            editableMobile: false,
            editableVoting: false,
            editablePassword: false,
        });
        //this.updateUserInfo()
    }

    lastNameEdit() {
        this.setState({
            editableFName: false,
            editableLName: true,
            editableMobile: false,
            editableVoting: false,
            editablePassword: false,
        });
    }

    async mobileEdit() {
        await this.setState({
            editableFName: false,
            editableLName: false,
            editableMobile: true,
            editableVoting: false,
            editablePassword: false,
        });
    }

    async votingZipCodeEdit() {
        await this.setState({
            editableFName: false,
            editableLName: false,
            editableMobile: false,
            editableVoting: true,
            editablePassword: false,
        });
    }

    async passEdit() {
        await this.setState({
            editableFName: false,
            editableLName: false,
            editableMobile: false,
            editableVoting: false,
            editablePassword: true,
        });

    }

    deleteAction() {
        //this.props.navigation.navigate("Login");
        this.updateUserInfo();
    }

    async getUserInfo() {

        let self = this;

        const userSession = await Auth.currentSession();
        let token = userSession.idToken.jwtToken;
        let response = await getUserInfo(token);
        self.setState({
            userData: response, isLoading: false, editableFName: false,
            editableLName: false,
            editableMobile: false,
            editableVoting: false,
            editablePassword: false,
        })

    }


    async updateUserInfo() {

        let { firstName, lastName, votingZipCode, mobile } = this.state;
        this.setState({ isLoading: true })



        var token = await AsyncStorage.getItem('userToken');

        let url = "https://290e8otzd9.execute-api.us-west-2.amazonaws.com/dev/me"


        axios({
            method: 'put',
            url: url,
            headers: { 'Authorization': 'Bearer ' + token },
            data: {
                firstName: firstName, // This is the body part
                lastName: lastName,
                postal: votingZipCode,
                phone: mobile
            }
        }).then(res =>
            showInfoAlert("Profile update successfully."),
            this.getUserInfo()
        )
            .catch(e => console.log(e));

    }


    render() {

        let { isLoading } = this.state;

        return (
            <KeyboardAvoidingView
                style={{ flex: 1, justifyContent: 'center' }}
                behavior={Platform.OS === 'ios' ? 'padding' : null}>


                <ScrollView
                    style={{ flex: 1, backgroundColor: 'white', }} keyboardShouldPersistTaps={'handled'}>

                    {/* header view */}

                    <View style={{ flexDirection: 'row', padding: 20, marginTop: height > 700 ? 50 : 20 }}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('Setting')}>
                            <Image source={backArrow} style={{ height: 20, width: 20 }} />
                        </TouchableOpacity>

                        <Text style={{ paddingHorizontal: 20, fontWeight: '800', fontSize: 15, color: '#565656' }}>My Account</Text>
                    </View>
                    <View style={{ height: 0.2, width: width, backgroundColor: '#696969' }} />




                    <View style={{ flex: 1, marginTop: 10 }}>
                        <View style={styles.mainContainer}>
                            <View style={styles.container}>

                                <View style={{ flex: 1, flexDirection: 'row', marginTop: 10 }}>
                                    <Text style={styles.leftSide}>First Name</Text>

                                    <TouchableOpacity onPress={() => this.firstNameEdit()} underlayColor='#fff'>
                                        <Text style={styles.edit} >Edit</Text>
                                    </TouchableOpacity>
                                </View>
                                <TextInput
                                    style={styles.textInput}
                                    onChangeText={(text) => { this.setState({ firstName: text }) }}
                                    //defaultValue={this.state.firstName}
                                    defaultValue={this.state.userData.firstName}
                                    selectTextOnFocus={this.state.editableFName}
                                    editable={this.state.editableFName}
                                    underlineColorAndroid='transparent'
                                    autoCapitalize='none'
                                    placeholder="First Name"
                                    placeholderTextColor="gray"
                                />

                                <View style={{ flex: 1, flexDirection: 'row', marginTop: 10 }}>
                                    <Text style={styles.leftSide}>Last Name</Text>

                                    <TouchableOpacity onPress={() => this.lastNameEdit()} underlayColor='#fff'>
                                        <Text style={styles.edit} >Edit</Text>
                                    </TouchableOpacity>
                                </View>
                                <TextInput
                                    onChangeText={(text) => { this.setState({ lastName: text }) }}
                                    //defaultValue={this.state.lastName}
                                    defaultValue={this.state.userData.lastName}
                                    editable={this.state.editableLName}
                                    selectTextOnFocus={this.state.editableLName}
                                    underlineColorAndroid='transparent'
                                    autoCapitalize='none'
                                    placeholder="Last Name"
                                    placeholderTextColor="gray"
                                    style={styles.textInput}
                                />

                                <View style={{ flex: 1, flexDirection: 'row', marginTop: 10 }}>
                                    <Text style={styles.leftSide}>Change Number</Text>

                                    <TouchableOpacity onPress={() => this.mobileEdit()} underlayColor='#fff'>
                                        <Text style={styles.edit} >Edit</Text>
                                    </TouchableOpacity>
                                </View>

                                <TextInput
                                    onChangeText={(text) => { this.setState({ mobile: text }) }}
                                    //defaultValue={this.state.mobile}
                                    maxLength={12}
                                    defaultValue={this.state.userData.phone}
                                    editable={this.state.editableMobile}
                                    selectTextOnFocus={this.state.editableMobile}
                                    underlineColorAndroid='transparent'
                                    autoCapitalize='none'
                                    placeholder="Change Number"
                                    placeholderTextColor="gray"
                                    style={styles.textInput}
                                />
                                <View style={{ flex: 1, flexDirection: 'row', marginTop: 10 }}>
                                    <Text style={styles.leftSide}>Email Address</Text>

                                    <TouchableOpacity onPress={() => this.firstNameEdit()} underlayColor='#fff' disabled>
                                        <Text style={styles.edit} >Disconnect</Text>
                                    </TouchableOpacity>
                                </View>

                                <TextInput
                                    onChangeText={(text) => { this.setState({ email: text }) }}
                                    editable={false}
                                    defaultValue={this.state.email}
                                    value={this.state.userData.email}
                                    underlineColorAndroid='transparent'
                                    autoCapitalize='none'
                                    placeholder="Email Address"
                                    placeholderTextColor="gray"
                                    style={styles.textInput}
                                />


                                <View style={{ flex: 1, flexDirection: 'row', marginTop: 10 }}>
                                    <Text style={styles.leftSide}>Facebook Connection</Text>

                                    <TouchableOpacity onPress={() => this.firstNameEdit()} underlayColor='#fff' disabled>
                                        <Text style={styles.edit} >Disconnect</Text>
                                    </TouchableOpacity>
                                </View>
                                <TextInput
                                    onChangeText={(text) => { this.setState({ facebookConnection: text }) }}
                                    defaultValue={this.state.facebookConnection}
                                    value={this.state.userData.email}
                                    underlineColorAndroid='transparent'
                                    autoCapitalize='none'
                                    maxLength={12}
                                    keyboardType="numeric"
                                    placeholder="Facebook"
                                    placeholderTextColor="gray"
                                    style={styles.textInput}
                                    editable={false}
                                />


                                <View style={{ flex: 1, flexDirection: 'row', marginTop: 10 }}>
                                    <Text style={styles.leftSide}>Voting Zip Code</Text>

                                    <TouchableOpacity onPress={() => this.votingZipCodeEdit()} underlayColor='#fff'>
                                        <Text style={styles.edit} >Edit</Text>
                                    </TouchableOpacity>
                                </View>
                                <TextInput
                                    onChangeText={(text) => { this.setState({ votingZipCode: text }) }}
                                    //defaultValue={this.state.votingZipCode}
                                    defaultValue={this.state.userData.postal}
                                    editable={this.state.editableVoting}
                                    selectTextOnFocus={this.state.editableVoting}
                                    underlineColorAndroid='transparent'
                                    autoCapitalize='none'
                                    maxLength={6}
                                    placeholder="Voting Zip Code"
                                    placeholderTextColor="gray"
                                    style={styles.textInput}
                                />


                                <View style={{ flex: 1, flexDirection: 'row', marginTop: 10 }}>
                                    <Text style={styles.leftSide}>Password</Text>

                                    <TouchableOpacity onPress={() => this.passEdit()} underlayColor='#fff'>
                                        <Text style={styles.edit} >Disconnect</Text>
                                    </TouchableOpacity>
                                </View>
                                <TextInput
                                    onChangeText={(text) => { this.setState({ password: text }) }}
                                    defaultValue={this.state.password}
                                    editable={this.state.editablePassword}
                                    selectTextOnFocus={this.state.editablePassword}
                                    underlineColorAndroid='transparent'
                                    autoCapitalize='none'
                                    secureTextEntry={true}
                                    placeholder="Password"
                                    placeholderTextColor="gray"
                                    style={styles.textInput}
                                    editable={false}
                                />

                                <TouchableOpacity
                                    style={styles.deleteStyle}
                                    onPress={() => this.deleteAction()}
                                    underlayColor='#fff'>
                                    <Text style={styles.deleteText}>Save Account</Text>
                                </TouchableOpacity>


                            </View>
                        </View>
                    </View>

                </ScrollView>

                {isLoading && <Loader />}

            </KeyboardAvoidingView>
        );
    }
}


const styles = {
    mainContainer: {
        flex: 1,
        paddingHorizontal: 10,
        paddingVertical: 0
    },
    container: {
        flex: 1,
        alignItems: 'center',
        alignSelf: 'center',
        width: '100%',
        marginTop: 10,
        marginRight: 10,
    },
    leftSide: {
        flex: 1,
        color: '#5F5F5F',
        fontSize: 14,
        fontWeight: '700',
        textAlign: 'left',
        paddingLeft: 18,
    },
    edit: {
        flex: 1,
        color: '#5F5F5F',
        fontSize: 14,
        fontWeight: '700',
        textAlign: 'right',
        paddingRight: 10
    },
    textInput: {
        marginTop: 10,
        height: 45,
        textAlign: 'left',
        fontSize: 16,
        backgroundColor: 'rgba(255, 255, 255, 0.6)',
        borderBottomWidth: 0.5,
        borderBottomStartRadius: 10,
        borderBottomColor: 'gray',
        width: '95%',
        color: '#D3D3D3',
        paddingLeft: 10
    },
    deleteStyle: {
        marginTop: 30,
        alignItems: 'center',
        justifyContent: 'center',
        height: 50,
        width: '88%',
        borderRadius: 5,
        borderWidth: 1,
        borderColor: 'grey',
    },
    deleteText: {
        color: '#8e1f1c',
        fontSize: 15, fontWeight: 'bold', fontWeight: '600',

    }

}

