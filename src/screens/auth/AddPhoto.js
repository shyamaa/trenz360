import React, { Component } from 'react';
import { Text, View, Image, Alert, TouchableOpacity,ScrollView,AsyncStorage } from 'react-native';
import ImagePicker from 'react-native-image-picker';
import CustomButton from '../../components/CustomButton';
import { showInfoAlert } from '../../utils/AlertPopUp';
import {getOr} from 'lodash/fp'
import { updateProfilePhoto, finalUpload } from '../../store/actions/BuzzAction';
import Amplify, { Auth } from 'aws-amplify';
import config from '../../config/aws-exports'
Amplify.configure(config);





export default class AddPhoto extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            avatarSource:require('../../imgAssests/camera.png'),
            uploadAvatar:[]
        }
    }




    importFromFacebook() {

        alert("Hit importFromFacebook")
        this.props.navigation.navigate("tabs");
    }

    importFromPhoto = () => {

        //this.props.navigation.navigate("tabs");

        let options = {
          title: 'SwipeRed',
          storageOptions: {
              skipBackup: true,
              path: 'images',
          },
      };



      ImagePicker.showImagePicker(options, (response) => {
          console.log('Response = ', response);


          if (response.didCancel) {
              console.log('User cancelled image picker');

          } else if (response.error) {
              console.log('ImagePicker Error: ', response.error);
          } else if (response.customButton) {
              console.log('User tapped custom button: ', response.customButton);
          } else {
              const source = { uri: response.uri };
              this.setState({
                  avatarSource: source,
                  uploadAvatar: response,
                  isLoading: true
              }, () => this.updateUserInfo());
          }
      });


    }

    async updateUserInfo() {

      console.log("avatarSource.uri", this.state.avatarSource.uri)

      let { avatarSource } = this.state;
      var token = await AsyncStorage.getItem('userToken');
      let response = await updateProfilePhoto(token, avatarSource.uri);

      if (response.uploadUrl !== '') {
          let uploadRes = await finalUpload(response.uploadUrl, avatarSource);
          console.log(uploadRes, "uploadRes")
          this.setState({ isLoading: false })
          this.props.navigation.navigate("tabs");
          

      }
   }

    

    completeSignUp=()=>{

         this.props.navigation.navigate("tabs");

      }



    render() {


        return (
                  <View style={{ flex: 1, backgroundColor: '#FFFFFF' }}>
                    {/* header view conatiner */}

                    <View style={{  top: 70}}>
                        <TouchableOpacity style={{ marginTop: 20,paddingRight:20 }} onPress={()=>this.completeSignUp()}>
                          <Text style={{ textAlign: 'right' }}>Skip</Text>
                        </TouchableOpacity>

             
                       <View style={styles.container}>
                          <Image source={require('../../imgAssests/camera.png')} style={{ width: 80, height: 80, }} />
                          <Text style={{ fontSize: 30, color: '#2F3979', fontWeight: '800', top: 20 }}> Add a Photo</Text>

                          

                       </View>

                       

                </View>

                


                {/* picker button view container */}

                <View style={{ flex: 1, justifyContent: 'center', alignSelf: 'center' }}>

                    <CustomButton title={'Import from Photos'} onpress={() => this.importFromPhoto()} />

                    <View style={{ alignSelf: 'center', top: 10 }}>

                        <CustomButton title={'Import from Facebook'} onpress={() => this.importFromFacebook()} />

                    </View>


                </View>

            </View>

         );
    }
}
const styles = {

    container: {
        alignItems: 'center',
    },

}