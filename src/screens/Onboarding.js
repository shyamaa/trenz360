import React from 'react';
import { View, Text,TouchableOpacity } from 'react-native';
import { ProgressSteps, ProgressStep } from 'react-native-progress-steps';

// import SearchableDropdown from 'react-native-searchable-dropdown';
// import Login from '../screens/auth/Login'



var items = [
    {
      id: 1,
      name: 'JavaScript',
    },
    {
      id: 2,
      name: 'Java',
    },
    {
      id: 3,
      name: 'Ruby',
    },
    {
      id: 4,
      name: 'React Native',
    },
    {
      id: 5,
      name: 'PHP',
    },
    {
      id: 6,
      name: 'Python',
    },
    {
      id: 7,
      name: 'Go',
    },
    {
      id: 8,
      name: 'Swift',
    },
  ];


export class Onboarding extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isValid: false,
            errors: false,
            selectedItems: [ ]

        }
    }
    onNextStep = () => {

        alert("ppp")
    };

    componentDidCatch(error, info) {
      console.log(error,"errorrrrr")
      this.setState({ error: true });
    }
    render() {
        return (
            <View style={{ flex: 1, marginTop: 50,backgroundColor:'#FFF' }}>

                 <ProgressSteps
                    ref={ref => {
                        this.progress = ref;
                    }}
                    progressBarColor={'lightgrey'}
                    borderWidth={2}
                    activeLabelColor={'#ED6C39'}
                    activeStepNumColor={'#ED6C39'}
                    disabledStepNumColor={'black'}
                    completedProgressBarColor={'#ED6C39'}
                    completedCheckColor={'#FFF'}
                    disabledStepIconColor={"grey"}
                    completedStepIconColor={'#ED6C39'}
                    activeStepIconColor={'#FFF'}
                    previousBtnDisabled={true}
                    disableStepIconBorderColor={'#ED6C39'}
                    activeStepIconBorderColor={'#ED6C39'} >

                    <ProgressStep label="incident Information" onNext={this.onNextStep}  >
                        <View style={{ alignItems: 'center', justifyContent: 'center' }}>

                            <Text>This is the content within step 1!</Text>


                            <TouchableOpacity onPress={() =>this.progress.onNext()}
                                style={{ width: 350, height: 45, backgroundColor: '#ED6C39', alignItems: 'center', justifyContent: 'center', borderRadius: 5 }}>
                                <Text style={{ fontSize: 18, color: '#FFF' }}>Continue</Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => this.progress.onNext}
                                style={{ alignItems: 'center', justifyContent: 'center', marginTop: 5 }}>
                                <Text style={{ fontSize: 15, color: 'grey' }}>Cancel</Text>
                            </TouchableOpacity>


                        </View>


                    </ProgressStep>

                    <ProgressStep label="Contributing Factors" previousBtnDisabled={false} nextBtnStyle={{backgroundColor:'#FFF'}}
                        nextBtnTextStyle={{ color: '#FFF' }}>
                        <View style={{ alignItems: 'center' }}>
                            <Text>This is the content within step 2!</Text>
                            <Text>gg</Text>
                        </View>
                    </ProgressStep>

                    <ProgressStep label="Acknowledgement">
                        <View style={{ alignItems: 'center' }}>
                            <Text>This is the content within step 3!</Text>
                        </View>
                    </ProgressStep>

                </ProgressSteps> 



            </View>
        )
    }
}


// import React, { Component, useState } from 'react';
// import { View, Text } from 'react-native'
// import { Container, Header, Content, Tab, Tabs, TabHeading, Icon } from 'native-base';



// export default class Onboarding extends Component {
//   render() {
//     return (
//       <Container>
//         <Header hasTabs />
//         <Tabs tabBarUnderlineStyle={{ backgroundColor: 'transaprent' }}
//         >
//           <Tab heading={<TabHeading>
//             <CustomeHeader />
//           </TabHeading>}>
//             <View>
//               <Text>ggggg</Text>
//             </View>
//           </Tab>
//           <Tab heading="Tab2">
//             <View>
//               <Text>ggggg</Text>
//             </View>
//           </Tab>
//           <Tab heading="Tab3">
//             <View>
//               <Text>ggggg</Text>
//             </View>
//           </Tab>
//         </Tabs>
//       </Container>
//     );
//   }
// }

// const CustomeHeader = (focused, title, number) => {
//   console.log(focused, 'focused')
//   return (
//     <TabHeading>
//       <View style={{ width: 100, }}>
//         <Text>{title}</Text>
//         <Text>.</Text>
//       </View>
//     </TabHeading>
//   )
// }
// const CustomeHeader2 = (focused, title, number) => {
//   console.log(focused, 'focused')
//   return (
//     <TabHeading>
//       <View style={{ width: 100, }}>
//         <Text>title</Text>
//         <Text>.</Text>
//       </View>
//     </TabHeading>
//   )
// }

// class Onboarding extends React.Component {

//   constructor(props) {
//     super(props);
//     this.state = {
//       tabPage: 0
//     }
//   }

//   render() {
//     let { tabPage } = this.state
//     return (
//       <Container>
//         <Content>
//           <Tabs
//             page={tabPage}
           

//             onChangeTab={(page) => this.setState({ tabPage: page.i },()=>console.log("prashnat",this.state.tabPage))}>

//             <Tab heading={
//               <TabHeading>
//                 <View style={{ width: 100,alignItems:'center'  }}>
//                   <Text>title</Text>
//                   {tabPage == 0 ? <Text>.</Text>  : null}
//                 </View>
//               </TabHeading>
//               } >
//               <Text>tab 1</Text>
//             </Tab>
//             <Tab heading={ <TabHeading style={{ backgroundColor: "white" }}>
//               <View style={{ width: 100,alignItems:'center'  }}>
//                 <Text>title</Text>
//             {tabPage == 1 ? <Text>.</Text> : null }
//               </View>
//             </TabHeading>}>
//               <Text>tab 2</Text>
//             </Tab>

//             <Tab heading={ <TabHeading style={{ backgroundColor: "white" }}>
//               <View style={{ width: 100,alignItems:'center' }}>
//                 <Text>title</Text>
//             {tabPage == 2 ? <Text>.</Text> : null }
//               </View>
//             </TabHeading>}>
//               <Text>tab 3</Text>
//             </Tab>
//           </Tabs>
//         </Content>
//       </Container>
//     )
//   }
// }
export default Onboarding;